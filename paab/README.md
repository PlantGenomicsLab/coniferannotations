# Norway Spruce
## Data Location:
### Genome(s): 
#### Original Genome:
&nbsp;&nbsp;&nbsp;/isg/treegenes/treegenes_store/FTP/Genomes/Paab/v1.0b/genome/Paab.1_0b.fa 

### Transcriptome: 
 
&nbsp;&nbsp;&nbsp;/isg/treegenes/treegenes_store/FTP/Transcriptome/TSA/Paab/Paab_TSA.fasta = 209 sequences

### Quast:
&nbsp;&nbsp;&nbsp;/labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/genomes/norwaySpruce/quast_out

        Assembly                    Paab.1_0b  
        # contigs (>= 0 bp)         10253694   
        # contigs (>= 1000 bp)      1970461    
        # contigs (>= 5000 bp)      437348     
        # contigs (>= 10000 bp)     208699     
        # contigs (>= 25000 bp)     59334      
        # contigs (>= 50000 bp)     9864       
        Total length (>= 0 bp)      12301351591
        Total length (>= 1000 bp)   9455649713 
        Total length (>= 5000 bp)   6231162000 
        Total length (>= 10000 bp)  4608832595 
        Total length (>= 25000 bp)  2337265445 
        Total length (>= 50000 bp)  651113763  
        # contigs                   3183035    
        Largest contig              208095     
        Total length                10275523729
        GC (%)                      37.60      
        N50                         8217       
        N75                         2496       
        L50                         267058     
        L75                         867373     
        # N's per 100 kbp           2743.93    
        
                   
### Genome Busco:

&nbsp;&nbsp;&nbsp;/labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/genomes/norwaySpruce/run_Busco_paab_wholeGenome

       C:35.0%[S:30.1%,D:4.9%],F:10.4%,M:54.6%,n:1440

        505     Complete BUSCOs (C)
        434     Complete and single-copy BUSCOs (S)
        71      Complete and duplicated BUSCOs (D)
        150     Fragmented BUSCOs (F)
        785     Missing BUSCOs (M)
        1440    Total BUSCO groups searched


### Genome Annotation Busco:

&nbsp;&nbsp;&nbsp; /labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/genes/norwaySpruce/run_Busco_paab_wholeGenome/

        C:37.6%[S:30.2%,D:7.4%],F:16.5%,M:45.9%,n:1440

        542     Complete BUSCOs (C)
        435     Complete and single-copy BUSCOs (S)
        107     Complete and duplicated BUSCOs (D)
        237     Fragmented BUSCOs (F)
        661     Missing BUSCOs (M)
        1440    Total BUSCO groups searched

