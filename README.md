# ConiferAnnotations

[google spreadsheet: ](https://docs.google.com/spreadsheets/d/1IeVR1QSW0z5qfvnHFb4rGaWrBytfKDveHN7Oj5CHOYQ/edit#gid=0)  
OrthoFinder Results: /UCHC/LABS/Wegrzyn/ConiferGenomes/Pita/paper/orthofinder/comprehensive/Results_Aug02  
CAFE Results: /UCHC/LABS/Wegrzyn/ConiferGenomes/Pita/paper/cafe/correctSpeciesTree  
Transcript alignment rate, structural annotation stats, repetative content, and other metrics reported [here ](https://docs.google.com/spreadsheets/d/1NnMnZ4pvFB3WqOp3TG2aN-TgZcmQbcIBktX9fCOcwBY/edit#gid=922622915)  
Annotation Methods for gnetum, ginkgo, and norway spruce posted [here ](https://docs.google.com/document/d/1d7PGS9u-qSBznnviYKYYbBQDUvNDiBGW0laDE62e4gI/edit)

