Library: ERR2040823
```7018854 reads; of these:
  7018854 (100.00%) were paired; of these:
    1760026 (25.08%) aligned concordantly 0 times
    1442827 (20.56%) aligned concordantly exactly 1 time
    3816001 (54.37%) aligned concordantly >1 times
    ----
    1760026 pairs aligned concordantly 0 times; of these:
      13295 (0.76%) aligned discordantly 1 time
    ----
    1746731 pairs aligned 0 times concordantly or discordantly; of these:
      3493462 mates make up the pairs; of these:
        2807222 (80.36%) aligned 0 times
        219266 (6.28%) aligned exactly 1 time
        466974 (13.37%) aligned >1 times
80.00% overall alignment rate
```

Library: SRR10482197
```94979294 reads; of these:
  94979294 (100.00%) were paired; of these:
    51103439 (53.80%) aligned concordantly 0 times
    24747457 (26.06%) aligned concordantly exactly 1 time
    19128398 (20.14%) aligned concordantly >1 times
    ----
    51103439 pairs aligned concordantly 0 times; of these:
      7638694 (14.95%) aligned discordantly 1 time
    ----
    43464745 pairs aligned 0 times concordantly or discordantly; of these:
      86929490 mates make up the pairs; of these:
        22865205 (26.30%) aligned 0 times
        22183348 (25.52%) aligned exactly 1 time
        41880937 (48.18%) aligned >1 times
87.96% overall alignment rate
```

