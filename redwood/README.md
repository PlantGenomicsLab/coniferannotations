# Redwood
## Data Location:
### Genome(s): 
#### Original Genome:
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/redwood/v2.1/genome/SESE.2.1.fa 

#### Genome Filter for Scaffolds less than 3 kbps:
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/redwood/v2.1/genome/SESE.v2_1.3kps.fa

#### Soft masked genome (Only the RepeatModeler library was used to mask)
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/redwood/v2.1/genome/SESE.v2_1.3kps.fa.masked

#### Previous LTR + repeatmodeler libary here (used for annotation)
&nbsp;&nbsp;&nbsp;/core/labs/Wegrzyn/ConiferGenomes/redwood/analysis/repeats/     
&nbsp;&nbsp;&nbsp;Final libary used to softmask the genome: centroids_all_repeats.fasta

#### New Repeat Analysis - this is useless 
&nbsp;&nbsp;&nbsp;/core/projects/EBP/Wegrzyn/redwood/re-analyze/repeatmodeler      
&nbsp;&nbsp;&nbsp;New repeatmodeler run done with ltr analyisis...still needs to be softmasked. Which library is to be used?

### Transcriptome
&nbsp;&nbsp;&nbsp;/core/labs/Wegrzyn/ConiferGenomes/redwood/analysis/transcriptome_cluster/final_transcriptome

### Quast:
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/redwood/v2.1/quast/quast/report.txt

        Assembly                    SESE.2.1     SESE.2.1_broken
        # contigs (>= 0 bp)         393401       -              
        # contigs (>= 1000 bp)      393399       548886         
        # contigs (>= 5000 bp)      365663       518555         
        # contigs (>= 10000 bp)     303749       447927         
        # contigs (>= 25000 bp)     151235       261123         
        # contigs (>= 50000 bp)     60929        133533         
        Total length (>= 0 bp)      26549733188  -              
        Total length (>= 1000 bp)   26549731883  26454296067    
        Total length (>= 5000 bp)   26453641394  26348944100    
        Total length (>= 10000 bp)  25983491318  25811318927    
        Total length (>= 25000 bp)  23450717491  22686159848    
        Total length (>= 50000 bp)  20303958839  18187829983    
        # contigs                   393400       548908         
        Largest contig              1397834379   2400478        
        Total length                26549732779  26454311569    
        GC (%)                      35.33        35.33          
        N50                         61327544     97162          
        N75                         54575        39574          
        L50                         29           61058          
        L75                         53427        170736         
        # N's per 100 kbp           359.38       0.00    



### Genome Busco:
&nbsp;&nbsp;&nbsp; busco results for v2 of the genome...pending.  
&nbsp;&nbsp;&nbsp; results for genome v1.0 (BUSCO v.3.0.2)
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/redwood/v1.0/busco/run_run_busco_sese_wholeGenome/

        C:40.6%[S:13.5%,D:27.1%],F:8.8%,M:50.6%,n:1440

        584     Complete BUSCOs (C)
        194     Complete and single-copy BUSCOs (S)
        390     Complete and duplicated BUSCOs (D)
        126     Fragmented BUSCOs (F)
        730     Missing BUSCOs (M)
        1440    Total BUSCO groups searched

### Annotation
&nbsp;&nbsp;&nbsp;/isg/treegenes/treegenes_store/FTP/Genomes/Sese/v2.1/annotation/    

&nbsp;&nbsp;&nbsp;BUSCO results for embroyophyta lineage    

        C:65.5%[S:41.0%,D:24.5%],F:14.7%,M:19.8%,n:1614    
        1058    Complete BUSCOs (C)                        
        662     Complete and single-copy BUSCOs (S)        
        396     Complete and duplicated BUSCOs (D)         
        237     Fragmented BUSCOs (F)                      
        319     Missing BUSCOs (M)                         
        1614    Total BUSCO groups searched 


&nbsp;&nbsp;&nbsp;BUSCO results for viridiplantae lineage    
   
        C:68.3%[S:44.5%,D:23.8%],F:23.5%,M:8.2%,n:425      
        290     Complete BUSCOs (C)                        
        189     Complete and single-copy BUSCOs (S)        
        101     Complete and duplicated BUSCOs (D)         
        100     Fragmented BUSCOs (F)                      
        35      Missing BUSCOs (M)                         
        425     Total BUSCO groups searched  

### Orthofinder Analysis
&nbsp;&nbsp;&nbsp;/core/labs/Wegrzyn/WBP_DE/MET_REF/Orthofinder/OrthoFinder/Results_Sep07

Statistics Overlap per sp 
|	| cuch | megl |	segi |	sese |	tamu |	thpl |
| ------ | --- | --- |--- | --- | --- | --- |
| cuch | 24808 | | | | | |
| megl | 19049 | 25017 | | | | |
|segi |	18302 | 18919 |	24225 | | | |
| sese | 17769 |18711 |	18550 |	22819 | | |	
| tamu | 18594 | 18578 | 17965 | 17603 | 23609 |  |	
|thpl |	19985 |19811 |	18510 |	18008 |	18789 |	25976|
<br>

### Alignments
&nbsp;&nbsp;&nbsp;/core/labs/Wegrzyn/ConiferGenomes/redwood/analysis/hisat2/     
&nbsp;&nbsp;&nbsp;/core/labs/Wegrzyn/ConiferGenomes/redwood/analysis/gth


