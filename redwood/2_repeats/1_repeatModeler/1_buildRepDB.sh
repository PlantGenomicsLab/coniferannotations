#!/bin/bash
# Submission script for Xanadu
####SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --mem=50G
#SBATCH --job-name=buildbd
#SBATCH -o build-%j.output
#SBATCH -e build-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10
#SBATCH --partition=himem
#SBATCH --qos=himem

module load RepeatModeler/1.0.8
module load rmblastn/2.2.28

BuildDatabase -name redwood -engine ncbi /isg/shared/databases/alignerIndex/plant/redwood/v2.1/genome/SESE.2.1.fa


