#!/bin/bash
# Submission script for Xanadu
#SBATCH --mem=50G
#SBATCH --job-name=rm-redwood
#SBATCH -o repeatmodeler-%j.output
#SBATCH -e repeatmodeler-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=40
#SBATCH --partition=general
#SBATCH --qos=general

module load RepeatModeler/1.0.8
module load rmblastn/2.2.28

RepeatModeler -engine ncbi -pa 40 -database redwood


