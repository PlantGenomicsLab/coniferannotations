import linecache
import argparse
import os
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq

parser = argparse.ArgumentParser(
     prog='extractSeq.py',
     usage='''python extractSeq.py --input_ltr [output of ltrFinder to be used] --out [name of output file] --fasta [input fasta to be used]''',
     description='''this program extracts sequences from the output of ltr finder''',
     epilog='''It requires linecache, argparse, and os''')
parser.add_argument('--input_ltr', type=str, help='The name of the ltr file', required=True)
parser.add_argument('--out', type=str, help='name of output file', required=True)
parser.add_argument('--fasta', type=str, help='name of fasta file', required=True)

args=parser.parse_args()

fastafile=args.fasta
output=args.out
in_ltr=args.input_ltr

id_dict=SeqIO.to_dict(SeqIO.parse(fastafile, "fasta"))

#>Sequence: Scaffold_174185;HRSCAF=189069 Len:30285
#[1] Scaffold_174185;HRSCAF=189069 Len:30285
#Location : 11818 - 16529 Len: 4712 Strand:+

#>Sequence: Scaffold_226506;HRSCAF=243962 Len:25539
#No LTR Retrotransposons Found

repeats={}

lines=[line.rstrip('\n') for line in open(in_ltr)]
i=0
for row in lines:
    #print(row)
    if row.startswith(">") == True:
        if "No LTR " in lines[i+1]:
            print("no ltr found")
        elif "Len" in lines[i+2]:
            loc=lines[i+2]
            print(loc)
            cols=loc.split(" ")
            scaffold=row.split(" ")[1]
            start= cols[2]
            end= cols[4]
            strand=cols[7].split(":")[1]
            rep=(str(id_dict[scaffold].seq[int(start)-1:int(end)]))
            name=scaffold+";"+start+"-"+end
            if strand=="+":
                repeats[name]=rep
            elif strand=="-":
                rev=str((id_dict[scaffold].seq[int(start)-1:int(end)]).reverse_complement())
                repeats[name]=rev
            else:
                print('wtf?')
    i=i+1

with open(output,'w') as out:
    for seq in repeats.keys():
	  out.write("%s%s\n%s\n" %(">",seq,repeats.get(seq)))  
out.close()

