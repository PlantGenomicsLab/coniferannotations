#!/bin/bash
# Submission script for Xanadu
#SBATCH --mem=5G
#SBATCH --job-name=ltrf
#SBATCH -o ltrf-%j.output
#SBATCH -e ltrf-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --array=1-261%15

echo $hostname
echo $SLURM_ARRAY_TASK_ID
echo `hostname`

line=$(head -n "$SLURM_ARRAY_TASK_ID" outputs.txt | tail -n 1)
genome_part=$(head -2 ../ltr-output/$line | tail -1)
echo $line
echo $genome_part
python extractSeq.py --input_ltr /labs/Wegrzyn/ConiferGenomes/redwood/analysis/repeats/ltr/ltrfinder/ltr-output/$line --out $line.fa --fasta /labs/Wegrzyn/ConiferGenomes/redwood/analysis/busco/split_genome/SESE.v2_1.3kps.fa$genome_part.fa
