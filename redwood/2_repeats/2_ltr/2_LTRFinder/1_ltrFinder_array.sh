#!/bin/bash
# Submission script for Xanadu
#SBATCH --mem=10G
#SBATCH --job-name=ltrf
#SBATCH -o ltrf-%j.output
#SBATCH -e ltrf-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --array=1-262%15

echo $hostname
echo $SLURM_ARRAY_TASK_ID
module load LTR_Finder/1.07 
ltr_finder -D 50000 -L 5000 /labs/Wegrzyn/ConiferGenomes/redwood/analysis/busco/split_genome/SESE.v2_1.3kps.fa"$SLURM_ARRAY_TASK_ID".fa
