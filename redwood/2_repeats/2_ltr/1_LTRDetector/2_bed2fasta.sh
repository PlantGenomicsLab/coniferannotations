#!/bin/bash
# Submission script for Xanadu
#SBATCH --mem=5G
#SBATCH --job-name=fasta
#SBATCH -o fasta-%j.output
#SBATCH -e fasta-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --array=1-262%50

module load bedtools
echo `hostname`
echo "$SLURM_ARRAY_TASK_ID"
grep -v 'SeqID' /labs/Wegrzyn/ConiferGenomes/redwood/analysis/repeats/ltr/ltrdetector/detector/SESE.v2_1.3kps.fa"$SLURM_ARRAY_TASK_ID"Detector.bed | grep -v 'Start' - > ./$SLURM_ARRAY_TASK_ID.bed
bedtools getfasta -fo ./SESE.v2_1.3kps.fa$SLURM_ARRAY_TASK_ID.masked.fa -fi /labs/Wegrzyn/ConiferGenomes/redwood/analysis/busco/split_genome/SESE.v2_1.3kps.fa$SLURM_ARRAY_TASK_ID.fa -bed $SLURM_ARRAY_TASK_ID.bed 

