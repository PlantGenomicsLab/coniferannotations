#!/bin/bash
# Submission script for Xanadu
#SBATCH --mem=50G
#SBATCH --job-name=ltrd
#SBATCH -o ltrd-%j.output
#SBATCH -e ltrd-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --partition=general
#SBATCH --qos=general

module load LtrDetector/1.0 
LtrDetector -fasta /labs/Wegrzyn/ConiferGenomes/redwood/analysis/busco/split_genome/ -destDir ./detector -nThreads 16

