# Gnetum montanum 

## Data Location:
### Genome(s): 
#### Original Genome:
&nbsp;&nbsp;&nbsp;/labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/genomes/gnetum/Gnetum.final.fa 

### Transcriptome: 
&nbsp;&nbsp;&nbsp;/labs/Wegrzyn/1kp_transcriptomes/Gnmn/GTHK-SOAPdenovo-Trans-assembly.fa 

### Quast:
&nbsp;&nbsp;&nbsp;/labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/genomes/gnetum/quast/quast_out

        Assembly                    Gnetum.final
        # contigs (>= 0 bp)         16307       
        # contigs (>= 1000 bp)      16307       
        # contigs (>= 5000 bp)      7287        
        # contigs (>= 10000 bp)     5677        
        # contigs (>= 25000 bp)     3885        
        # contigs (>= 50000 bp)     3131        
        Total length (>= 0 bp)      1206557267  
        Total length (>= 1000 bp)   1206557267  
        Total length (>= 5000 bp)   1191269368  
        Total length (>= 10000 bp)  1179395663  
        Total length (>= 25000 bp)  1151459887  
        Total length (>= 50000 bp)  1124933044  
        # contigs                   16307       
        Largest contig              4209273     
        Total length                1206557267  
        GC (%)                      37.34       
        N50                         553794      
        N75                         253655      
        L50                         619         
        L75                         1426        
        # N's per 100 kbp           5138.37 

### Busco Genome:

&nbsp;&nbsp;&nbsp; /labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/genome/gnetum/run_Busco_gnmo_wholeGenome_Dec20
    
    C:21.8%[S:20.1%,D:1.7%],F:1.8%,M:76.4%,n:1440

        313     Complete BUSCOs (C)
        289     Complete and single-copy BUSCOs (S)
        24      Complete and duplicated BUSCOs (D)
        26      Fragmented BUSCOs (F)
        1101    Missing BUSCOs (M)
        1440    Total BUSCO groups searched

### Busco Transcriptome:
    
&nbsp;&nbsp;&nbsp; /labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/transcriptomes/gnetum/run_busco_gnetum_all_transcripts

    C:49.6%[S:45.9%,D:3.7%],F:10.6%,M:39.8%,n:1440

        714     Complete BUSCOs (C)
        661     Complete and single-copy BUSCOs (S)
        53      Complete and duplicated BUSCOs (D)
        152     Fragmented BUSCOs (F)
        574     Missing BUSCOs (M)
        1440    Total BUSCO groups searched

### Busco Genome Annotation:

&nbsp;&nbsp;&nbsp; /labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/genes/gnetum/run_Busco_gnmo_annotation

        C:72.8%[S:66.3%,D:6.5%],F:3.6%,M:23.6%,n:1440

        1049    Complete BUSCOs (C)
        955     Complete and single-copy BUSCOs (S)
        94      Complete and duplicated BUSCOs (D)
        52      Fragmented BUSCOs (F)
        339     Missing BUSCOs (M)
        1440    Total BUSCO groups searched
    