# Redwood
## Data Location: 
### Genome(s): 
#### Original Genome:
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/mystery_redwood/genome/redwood_p_utg_33fold.fa

### Quast:
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/mystery_redwood/quast/unitigs_33fold/report.txt

	Assembly                    redwood_p_utg_33fold
	# contigs (>= 0 bp)         43501               
	# contigs (>= 1000 bp)      43501               
	# contigs (>= 5000 bp)      43501               
	# contigs (>= 10000 bp)     43501               
	# contigs (>= 25000 bp)     43086               
	# contigs (>= 50000 bp)     37024               
	Total length (>= 0 bp)      50732528419         
	Total length (>= 1000 bp)   50732528419         
	Total length (>= 5000 bp)   50732528419         
	Total length (>= 10000 bp)  50732528419         
	Total length (>= 25000 bp)  50723172784         
	Total length (>= 50000 bp)  50494942941         
	# contigs                   43501               
	Largest contig              25744027            
	Total length                50732528419         
	GC (%)                      35.64               
	N50                         3031506             
	N75                         1533771             
	L50                         4990                
	L75                         10867               
	# N's per 100 kbp           0.00                




    
