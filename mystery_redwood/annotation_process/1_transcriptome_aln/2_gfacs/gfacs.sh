#!/bin/bash
#SBATCH --job-name=gfacs
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --mail-type=ALL
#SBATCH --mem=50G
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o stats_%j.o
#SBATCH -e stats_%j.e
#SBATCH --qos=general 

mkdir -p gfacs

module load perl/5.24.0
perl /labs/Wegrzyn/gFACs/gFACs.pl \
-f gmap_2017_03_17_gff3 \
-p mystery_redwood_illumina \
--statistics \
--unique-genes-only \
--min-CDS-size 300 \
--allowed-inframe-stop-codons 0 \
--min-exon-size 9 \
--min-intron-size 9 \
--fasta /isg/shared/databases/alignerIndex/plant/mystery_redwood/genome/redwood_p_utg_33fold.fa \
-O ./gfacs/ \
../1_gmap/illumina_genes.gff3
