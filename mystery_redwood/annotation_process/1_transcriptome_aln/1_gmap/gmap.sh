#!/bin/bash
#SBATCH --job-name=gmap
#SBATCH -n 20
#SBATCH --mem=80G
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o gmap_%j.out
#SBATCH -e gmap_%j.err
#SBATCH --qos=general
# Run the program 

module load gmap/2019-06-10 
#for CONIFERS ONLY

gmapl -K 1000000 -L 10000000 --cross-species -D /isg/shared/databases/alignerIndex/plant/mystery_redwood/index/gmap/gmap-2019-06-10/ -d redwood_hifi_GmapIndex_5000bps -f gff3_gene /labs/Wegrzyn/ConiferGenomes/redwood/illumina_transcriptome/s_sempervirens.fasta.transdecoder.cds --nthreads=20 --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 > illumina_genes.gff3
