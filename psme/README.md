# Douglas fir --> $PWD=/UCHC/LABS/Wegrzyn/ConiferGenomes/Psme/analysis/  
## Data Location:
### Genome(s): 
#### Original Genome:
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/Psme/genome/v1.0/Psme_v1.0.fasta 
&nbsp;&nbsp;&nbsp;[TreeGenes FTP](https://treegenesdb.org/FTP/Genomes/Psme/v1.0/genome/Psme.1_0.fa.gz) 
#### Softmasked Genome and scaffolds >= 5000bps:  
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/Psme/genome/v1.0.5000/Psme_v1.0.5000.masked.fasta 
&nbsp;&nbsp;&nbsp;[TreeGenes FTP](https://treegenesdb.org/FTP/Genomes/Psme/v1.0/genome/Psme.1_0.5kb.softmasked.fa.gz) 
### Raw RNA-seq Reads:
&nbsp;&nbsp;&nbsp;$PWD/raw_reads/  
&nbsp;&nbsp;&nbsp;[NCBI Bioproject](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA188506)  
&nbsp;&nbsp;&nbsp;[NCBI Download](https://www.ncbi.nlm.nih.gov/Traces/wgs/GFFY01?display=contigs&page=1)  
### Transcriptome: 
&nbsp;&nbsp;&nbsp;$PWD/Genes/Illumina/illumina_transcrips.fasta --> [NCBI](https://www.ncbi.nlm.nih.gov/Traces/wgs/GFFY01?display=contigs&page=1)  
&nbsp;&nbsp;&nbsp; Frame Selected Transcriptome: $PWD/Genes/Illumina/illumina_transcrips_framesel.fasta.fnn  
&nbsp;&nbsp;&nbsp; Frame Selected Transcriptome (Protein): $PWD/Genes/Illumina/illumina_transcrips_framesel.fasta.faa  
### Repeats: 
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/Psme/repeats/library_Psmewhol_finalclassifications.fasta  
&nbsp;&nbsp;&nbsp;[TreeGenes FTP](https://treegenesdb.org/FTP/Genomes/Psme/v1.0/repeats/library_Psmewhol_finalclassifications.fasta)
### Quast:
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/Psme/genome/v1.0.5000/SOAP/

        Assembly                    Psme_v1.0.5000.masked
        # contigs (>= 0 bp)         75986                
        # contigs (>= 1000 bp)      75986                
        # contigs (>= 5000 bp)      75471                
        # contigs (>= 10000 bp)     60847                
        # contigs (>= 25000 bp)     47768                
        # contigs (>= 50000 bp)     38906                
        Total length (>= 0 bp)      11469632196          
        Total length (>= 1000 bp)   11469632196          
        Total length (>= 5000 bp)   11467079007          
        Total length (>= 10000 bp)  11364167910          
        Total length (>= 25000 bp)  11154636306          
        Total length (>= 50000 bp)  10834232635          
        # contigs                   75986                
        Largest contig              3697365              
        Total length                11469632196          
        GC (%)                      37.23                
        N50                         394719               
        N75                         197140               
        L50                         8441                 
        L75                         18609                
        # N's per 100 kbp           2070.81 
                   
### Genome Busco:
&nbsp;&nbsp;&nbsp; /isg/shared/databases/alignerIndex/plant/Psme/genome/v1.0.5000/run_Busco_psme_wholeGenome

        C:33.6%[S:28.6%,D:5.0%],F:7.6%,M:58.8%,n:1440

        484     Complete BUSCOs (C)
        412     Complete and single-copy BUSCOs (S)
        72      Complete and duplicated BUSCOs (D)
        110     Fragmented BUSCOs (F)
        846     Missing BUSCOs (M)
        1440    Total BUSCO groups searched

### Transcriptome Busco:

&nbsp;&nbsp;&nbsp; /labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/transcriptomes/douglasfir/run_Busco_psme_illumina_transcripts/

        C:84.8%[S:59.4%,D:25.4%],F:3.3%,M:11.9%,n:1440

        1222    Complete BUSCOs (C)
        856     Complete and single-copy BUSCOs (S)
        366     Complete and duplicated BUSCOs (D)
        47      Fragmented BUSCOs (F)
        171     Missing BUSCOs (M)
        1440    Total BUSCO groups searched

### Genome Annotation Busco:

&nbsp;&nbsp;&nbsp; /labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/genes/douglasFir/run_Busco_psme_annotation/

        C:52.9%[S:34.7%,D:18.2%],F:6.3%,M:40.8%,n:1440

        762     Complete BUSCOs (C)
        500     Complete and single-copy BUSCOs (S)
        262     Complete and duplicated BUSCOs (D)
        91      Fragmented BUSCOs (F)
        587     Missing BUSCOs (M)
        1440    Total BUSCO groups searched

### Genome Annotation Structural (Done with Braker)

&nbsp;&nbsp;&nbsp; Detailed process is outlined in the annotationProcess.md  
&nbsp;&nbsp;&nbsp; [TreeGenes FTP Protein Sequence](https://treegenesdb.org/FTP/Genomes/Psme/v1.0/annotation/annotation2.0/Psme.1_0.peptides.fa)  
&nbsp;&nbsp;&nbsp; [TreeGenes FTP Nucleotide Sequence](https://treegenesdb.org/FTP/Genomes/sme/v1.0/annotation/annotation2.0/Psme.1_0.cds.fa)  
&nbsp;&nbsp;&nbsp; [TreeGenes FTP GTF](https://treegenesdb.org/FTP/Genomes/Psme/v1.0/annotation/annotation2.0/Psme.1_0.gtf)  

## Genome Annotation Structural (Done with Maker)
&nbsp;&nbsp;&nbsp; Initial Annotation: /labs/Wegrzyn/ConiferGenomes/Psme/PSME_1/Psme_v1.0_10kb-filter.all.genes.gff 
&nbsp;&nbsp;&nbsp; Published Annotation: /isg/treegenes/treegenes_store/FTP/Genomes/Psme/v1.0/annotation/annotation1.0/


### Genome Annotation Functional

&nbsp;&nbsp;&nbsp; [TreeGenes FTP](https://treegenesdb.org/FTP/Genomes/Psme/v1.0/annotation/annotation2.0/Psme.1_0.functionalAnnotation.tsv)
  






