# Location:
&nbsp;&nbsp;&nbsp; Output Location: /UCHC/LABS/Wegrzyn/ConiferGenomes/Psme/analysis/braker/Braker2/prot_alignments/douglas_protein_alignment.gff3  
&nbsp;&nbsp;&nbsp; Script Location: /UCHC/LABS/Wegrzyn/ConiferGenomes/Psme/analysis/braker/Braker2/prot_alignments/genomethreader.sh

# Input:
&nbsp;&nbsp;&nbsp; Query: /UCHC/LABS/Wegrzyn/ConiferGenomes/Psme/analysis/braker/Braker2/prots/comp_combined_protein_filtered.fasta150aa.faa  
&nbsp;&nbsp;&nbsp; The query fasta was created by combining all of the protein sequences from Douglas fir from various sources including Illumina sourced transcriptome  
&nbsp;&nbsp;&nbsp; + Maker gene models (read the README.md found in psme).  
&nbsp;&nbsp;&nbsp; Proteins found from PLAZA were also include (location:/UCHC/LABS/Wegrzyn/PLAZA/gymno/proteome.pme.csv)  
&nbsp;&nbsp;&nbsp; All these protein sequences were concatnated into a single file, short sequences (≤150 amino acids), clustered, and frame selected for complete genes. 

# Statistics:

STATS:
Number of genes:        70382  
Number of monoexonic genes:     32291  
Number of multiexonic genes:    38091  

Number of positive strand genes:        35283  
Monoexonic:     16177  
Multiexonic:    19106  

Number of negative strand genes:        35099  
Monoexonic:     16114  
Multiexonic:    18985  

Average overall gene size:      42730.199  
Median overall gene size:       1602  
Average overall CDS size:       1253.861  
Median overall CDS size:        955  
Average overall exon size:      482.136  
Median overall exon size:       226  

Average size of monoexonic genes:       14143.722  
Median size of monoexonic genes:        987  
Largest monoexonic gene:        2115182  
Smallest monoexonic gene:       403  

Average size of multiexonic genes:      66963.900  
Median size of multiexonic genes:       2842  
Largest multiexonic gene:       2322015  
Smallest multiexonic gene:      495  

Average size of multiexonic CDS:        1283.259  
Median size of multiexonic CDS: 986  
Largest multiexonic CDS:        14434  
Smallest multiexonic CDS:       373  

Average size of multiexonic exons:      324.256  
Median size of multiexonic exons:       172  
Average size of multiexonic introns:    14522.394  
Median size of multiexonic introns:     210  

Average number of exons per multiexonic gene:   3.958  
Median number of exons per multiexonic gene:    3  

# Command: 
``` #!/bin/bash
# Submission script for Xanadu
####SBATCH --time=10-01:00:00 # days-hh:mm:ss
####SBATCH --mem=350GB
#SBATCH --job-name=gth
#SBATCH -o gth-%j.output
#SBATCH -e gth-%j.error
#SBATCH --mail-user=sararat.tosakoon@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=general
#SBATCH --array=1-499%20

module load genomethreader/1.6.6
#echo $SLURM_ARRAY_TASK_ID
## blat format: blat database query [-ooc=11.ooc] output.psl
echo genome_scaffolds_blathits.fasta"$SLURM_ARRAY_TASK_ID".fa

gth -genomic /UCHC/LABS/Wegrzyn/ConiferGenomes/Psme/analysis/braker/Braker2/prots/splitfasta/splitfastaPsme_v1.0.5000.masked.fasta"$SLURM_ARRAY_TASK_ID".fa -protein /UCHC/LABS/Wegrzyn/ConiferGenomes/Psme/analysis/braker/Braker2/prots/comp_combined_protein_filtered.fasta150aa.faa -gff3out -startcodon -gcmincoverage 80 -finalstopcodon -introncutout -dpminexonlen 20 -skipalignmentout -o comp_combined_protein_filtered.fasta150aa.faa"$SLURM_ARRAY_TASK_ID".gth.gff3 -force -gcmaxgapwidth 1000000``` 
 