# Cycas debaoensis
## Data Location: $PWD=/labs/Wegrzyn/ConiferGenomes/coniferannotations/cycad/debao
### Genome(s): 
#### Original Genome:
&nbsp;&nbsp;&nbsp;/labs/Wegrzyn/cycad/Debao_sutie/00_GENOME/suite.genome.fasta

### Repeats: 

### Quast:
&nbsp;&nbsp;&nbsp;$PWD/debao/1_prelim_check/quast/quast_debao/quast.txt

        Assembly                    suite.genome
    # contigs (>= 0 bp)         4442
	# contigs (>= 1000 bp)      4442
	# contigs (>= 5000 bp)      4442
	# contigs (>= 10000 bp)     4442
	# contigs (>= 25000 bp)     4442
	# contigs (>= 50000 bp)     4426
	Total length (>= 0 bp)      10927072962
	Total length (>= 1000 bp)   10927072962
	Total length (>= 5000 bp)   10927072962
	Total length (>= 10000 bp)  10927072962
	Total length (>= 25000 bp)  10927072962
	Total length (>= 50000 bp)  10926394441
	# contigs                   4442
	Largest contig              65334826
	Total length                10927072962
	GC (%)                      35.80
	N50                         9984213
	N75                         3996796
	L50                         302
	L75                         725
	# N's per 100 kbp           0.00

### Genome Annotation BUSCO:

&nbsp;&nbsp;&nbsp;$PWD/1_prelim_check/busco/buscoV4.0_debao_genese/short_summary.specific.embryophyta_odb10.buscoV4.0_debao_genese.txt

	C:88.2%[S:80.6%,D:7.6%],F:3.2%,M:8.6%,n:1614       
        1424    Complete BUSCOs (C)                        
        1301    Complete and single-copy BUSCOs (S)        
        123     Complete and duplicated BUSCOs (D)         
        52      Fragmented BUSCOs (F)                      
        138     Missing BUSCOs (M)                         
        1614    Total BUSCO groups searched 

### Genome Annotation gFACs:
&nbsp;&nbsp;&nbsp;$PWD/debao/1_prelim_check/gfacs/gfacs/statistics.txt 

	STATS:
	Number of genes:	34348
	Number of monoexonic genes:	8847
	Number of multiexonic genes:	25501

	Number of positive strand genes:	17325
	Monoexonic:	4291
	Multiexonic:	13034

	Number of negative strand genes:	17023
	Monoexonic:	4556
	Multiexonic:	12467

	Average overall gene size:	134514.304
	Median overall gene size:	3776
	Average overall CDS size:	1160.177
	Median overall CDS size:	849
	Average overall exon size:	232.406
	Median overall exon size:	135

	Average size of monoexonic genes:	629.305
	Median size of monoexonic genes:	432
	Largest monoexonic gene:	4653
	Smallest monoexonic gene:	150

	Average size of multiexonic genes:	180962.701
	Median size of multiexonic genes:	76501.5
	Largest multiexonic gene:	2095666
	Smallest multiexonic gene:	192

	Average size of multiexonic CDS:	1344.351
	Median size of multiexonic CDS:	1056
	Largest multiexonic CDS:	16977
	Smallest multiexonic CDS:	150

	Average size of multiexonic exons:	210.814
	Median size of multiexonic exons:	128
	Average size of multiexonic introns:	33405.151
	Median size of multiexonic introns:	263

	Average number of exons per multiexonic gene:	6.377
	Median number of exons per multiexonic gene:	4
	Largest multiexonic exon:	14621
	Smallest multiexonic exon:	3
	Most exons in one gene:	78

	Average number of introns per multiexonic gene:	5.377
	Median number of introns per multiexonic gene:	3
	Largest intron:	1569820
	Smallest intron:	29

	The following columns do not involve codons:
	Number of complete models:	34348
	Number of 5' only incomplete models:	0
	Number of 3' only incomplete models:	0
	Number of 5' and 3' incomplete models:	0
