#!/bin/bash
#SBATCH --job-name=gfacs
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --mail-type=ALL
#SBATCH --mem=5G
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o cycad_stats_%j.o
#SBATCH -e cycad_stats_%j.e
#SBATCH --qos=general 

module load perl/5.24.0
perl /labs/Wegrzyn/gFACs/gFACs.pl  \
-f EVM_1.1.1_gff3 \
--statistics \
-O ./gfacs/ \
/labs/Wegrzyn/cycad/Debao_sutie/02.gene_prediction/sutie.gff3
