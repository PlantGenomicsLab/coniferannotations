#!/bin/bash
#SBATCH --job-name=quast
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL 
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --qos=general
#SBATCH --partition=general
#SBATCH -o quast.out
#SBATCH -e quast.err
#SBATCH --mem=12G

module load quast/4.6
module load anaconda

quast.py /labs/Wegrzyn/cycad/Debao_sutie/00_GENOME/suite.genome.fasta -o quast_debao

