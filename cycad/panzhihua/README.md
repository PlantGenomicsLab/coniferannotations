# Cycas panzhihuaensis
## Data Location: /labs/Wegrzyn/ConiferGenomes/coniferannotations/cycad/panzhihua
### Genome(s): 
#### Original Genome:
&nbsp;&nbsp;&nbsp;/labs/Wegrzyn/cycad/Panzhihua_sutie/00_GENOME/sutie2.genome.fasta

### Repeats: 

### Quast:
&nbsp;&nbsp;&nbsp;$PWD/panzhihua/1_prelim_check/quast/quast_panzhihuaquast.txt
       
    # Assembly                  sutie2.genome
	# contigs (>= 0 bp)         5310         
	# contigs (>= 1000 bp)      5310         
	# contigs (>= 5000 bp)      5310         
	# contigs (>= 10000 bp)     5310         
	# contigs (>= 25000 bp)     5310         
	# contigs (>= 50000 bp)     5216         
	Total length (>= 0 bp)      10764188900  
	Total length (>= 1000 bp)   10764188900  
	Total length (>= 5000 bp)   10764188900  
	Total length (>= 10000 bp)  10764188900  
	Total length (>= 25000 bp)  10764188900  
	Total length (>= 50000 bp)  10759983417  
	# contigs                   5310         
	Largest contig              69926807     
	Total length                10764188900  
	GC (%)                      36.02        
	N50                         12673008     
	N75                         5595242      
	L50                         249          
	L75                         559          
	# N's per 100 kbp           0.00

### Genome Annotation BUSCO:

&nbsp;&nbsp;&nbsp;$PWD/panzhihua/1_prelim_check/busco/buscoV4.0_panzhihua_genese/short_summary.specific.embryophyta_odb10.buscoV4.0_panzhihua_genese.txt

	C:89.4%[S:83.6%,D:5.8%],F:2.8%,M:7.8%,n:1614
	1442    Complete BUSCOs (C)
    1349    Complete and single-copy BUSCOs (S)
    93      Complete and duplicated BUSCOs (D)
	45      Fragmented BUSCOs (F)
    127     Missing BUSCOs (M)
	1614    Total BUSCO groups searched

### Genome Annotation gFACs:

&nbsp;&nbsp;&nbsp;$PWD/panzhihua/1_prelim_check/gfacs/gfacs/statistics.txt 

	STATS:
	Number of genes:	36027
	Number of monoexonic genes:	9874
	Number of multiexonic genes:	26153

	Number of positive strand genes:	17985
	Monoexonic:	4962
	Multiexonic:	13023

	Number of negative strand genes:	18042
	Monoexonic:	4912
	Multiexonic:	13130

	Average overall gene size:	112851.281
	Median overall gene size:	2770
	Average overall CDS size:	1113.151
	Median overall CDS size:	807
	Average overall exon size:	244.994
	Median overall exon size:	142

	Average size of monoexonic genes:	630.869
	Median size of monoexonic genes:	432
	Largest monoexonic gene:	5502
	Smallest monoexonic gene:	150

	Average size of multiexonic genes:	155219.817
	Median size of multiexonic genes:	52329.5
	Largest multiexonic gene:	1836548
	Smallest multiexonic gene:	196

	Average size of multiexonic CDS:	1295.235
	Median size of multiexonic CDS:	1020
	Largest multiexonic CDS:	17115
	Smallest multiexonic CDS:	153

	Average size of multiexonic exons:	220.223
	Median size of multiexonic exons:	134
	Average size of multiexonic introns:	31532.445
	Median size of multiexonic introns:	267

	Average number of exons per multiexonic gene:	5.881
	Median number of exons per multiexonic gene:	4
	Largest multiexonic exon:	14561
	Smallest multiexonic exon:	3
	Most exons in one gene:	80

	Average number of introns per multiexonic gene:	4.881
	Median number of introns per multiexonic gene:	3
	Largest intron:	1512715
	Smallest intron:	29

	The following columns do not involve codons:
	Number of complete models:	36027
	Number of 5' only incomplete models:	0
	Number of 3' only incomplete models:	0
	Number of 5' and 3' incomplete models:	0




