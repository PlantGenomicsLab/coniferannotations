# BUSCO version is: 4.0.2 
# The lineage dataset is: embryophyta_odb10 (Creation date: 2019-11-20, number of species: 50, number of BUSCOs: 1614)
# Summarized benchmarking in BUSCO notation for file /labs/Wegrzyn/cycad/Panzhihua_sutie/00_GENOME/sutie2.genome.fasta
# BUSCO was run in mode: genome

	***** Results: *****

	C:39.4%[S:36.6%,D:2.8%],F:11.0%,M:49.6%,n:1614	   
	636	Complete BUSCOs (C)			   
	591	Complete and single-copy BUSCOs (S)	   
	45	Complete and duplicated BUSCOs (D)	   
	178	Fragmented BUSCOs (F)			   
	800	Missing BUSCOs (M)			   
	1614	Total BUSCO groups searched		   
