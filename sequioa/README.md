# Sequioa 
## Data Location:
### Genome(s): 
#### Original Genome:
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/sequoia/Assembly/v2.0/SEGI.2.0.fa 

### Quast:
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/sequoia/Assembly/v2.0/quast_sequioa_noFilter/

    Assembly                    SEGI.2.0  
    # contigs (>= 0 bp)         8215      
    # contigs (>= 1000 bp)      8209      
    # contigs (>= 5000 bp)      7595      
    # contigs (>= 10000 bp)     5254      
    # contigs (>= 25000 bp)     1140      
    # contigs (>= 50000 bp)     222       
    Total length (>= 0 bp)      8125608169
    Total length (>= 1000 bp)   8125604663
    Total length (>= 5000 bp)   8123152985
    Total length (>= 10000 bp)  8105349072
    Total length (>= 25000 bp)  8041372983
    Total length (>= 50000 bp)  8010586579
    # contigs                   8213      
    Largest contig              985175838 
    Total length                8125607318
    GC (%)                      35.41     
    N50                         690549816 
    N75                         649867199 
    L50                         5         
    L75                         8         
    # N's per 100 kbp           276.50    

## Genome Busco:

&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/sequoia/Assembly/v2.0/run_Busco_sequioa_wholeGenome/ short_summary_Busco_sequioa_wholeGenome.txt 

        C:39.1%[S:36.0%,D:3.1%],F:9.4%,M:51.5%,n:1440

        564     Complete BUSCOs (C)
        519     Complete and single-copy BUSCOs (S)
        45      Complete and duplicated BUSCOs (D)
        136     Fragmented BUSCOs (F)
        740     Missing BUSCOs (M)
        1440    Total BUSCO groups searched
