# Ginkgo biloba

## Data Location:
### Genome(s): 
#### Original Genome:
&nbsp;&nbsp;&nbsp;/isg/treegenes/treegenes_store/FTP/Genomes/Gibi/v1.0/genome/Gibi.1_0.fa

### Transcriptome: 
&nbsp;&nbsp;&nbsp;/isg/treegenes/treegenes_store/FTP/Transcriptome/TSA/Gibi/Gibi_TSA.fasta

### Quast:

&nbsp;&nbsp;&nbsp;/labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/genomes/ginkgo/quast_out/

    Assembly                    Gibi.1_0   
    # contigs (>= 0 bp)         6459773    
    # contigs (>= 1000 bp)      67554      
    # contigs (>= 5000 bp)      22455      
    # contigs (>= 10000 bp)     18474      
    # contigs (>= 25000 bp)     13426      
    # contigs (>= 50000 bp)     11327      
    Total length (>= 0 bp)      10608657252
    Total length (>= 1000 bp)   9556997140 
    Total length (>= 5000 bp)   9482254114 
    Total length (>= 10000 bp)  9456163205 
    Total length (>= 25000 bp)  9368489825 
    Total length (>= 50000 bp)  9289780223 
    # contigs                   169974     
    Largest contig              13688121   
    Total length                9626492610 
    GC (%)                      34.92      
    N50                         1531366    
    N75                         750595     
    L50                         1808       
    L75                         4036       
    # N's per 100 kbp           5124.81  
        

### Busco Genome:

&nbsp;&nbsp;&nbsp;/labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/genome/ginkgo/run_Busco_GIBI_wholeGenome_Dec20/
    
    C:49.1%[S:44.2%,D:4.9%],F:6.4%,M:44.5%,n:1440

        707     Complete BUSCOs (C)
        637     Complete and single-copy BUSCOs (S)
        70      Complete and duplicated BUSCOs (D)
        92      Fragmented BUSCOs (F)
        641     Missing BUSCOs (M)
        1440    Total BUSCO groups searched

### Busco Transcriptome:   

&nbsp;&nbsp;&nbsp;/labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/transcriptomes/ginkgo/run_Busco_gibi_all_transcripts/
    
    C:88.4%[S:15.6%,D:72.8%],F:2.7%,M:8.9%,n:1440

        1272    Complete BUSCOs (C)
        224     Complete and single-copy BUSCOs (S)
        1048    Complete and duplicated BUSCOs (D)
        39      Fragmented BUSCOs (F)
        129     Missing BUSCOs (M)
        1440    Total BUSCO groups searched

### Busco Genome Annotation:

&nbsp;&nbsp;&nbsp;/labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/genes/ginkgo/run_Busco_gibi_annotation

        C:62.8%[S:52.9%,D:9.9%],F:7.7%,M:29.5%,n:1440

        905     Complete BUSCOs (C)
        762     Complete and single-copy BUSCOs (S)
        143     Complete and duplicated BUSCOs (D)
        111     Fragmented BUSCOs (F)
        424     Missing BUSCOs (M)
        1440    Total BUSCO groups searched
        
    