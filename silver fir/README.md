# Silver fir

## Data Location:

### Genome(s): 

#### Original Genome:
&nbsp;&nbsp;&nbsp; /labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/genomes/silverFir/AALBA1.1.fasta 

### Transcriptome: 
&nbsp;&nbsp;&nbsp; /labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/transcriptomes/silverFir/silverFirTranscriptome.fasta  
&nbsp;&nbsp;&nbsp; [Transcriptome Source](https://www.ncbi.nlm.nih.gov/nuccore?LinkName=bioproject_nuccore_transcript&from_uid=159263)

### Quast:

&nbsp;&nbsp;&nbsp; /labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/genome/busco/genomes/silverFir/quast_abal_1.1

        Assembly                    Abal.1_1   
        # contigs (>= 0 bp)         37192295   
        # contigs (>= 1000 bp)      1276678    
        # contigs (>= 5000 bp)      529013     
        # contigs (>= 10000 bp)     343016     
        # contigs (>= 25000 bp)     145508     
        # contigs (>= 50000 bp)     46234      
        Total length (>= 0 bp)      18167382048
        Total length (>= 1000 bp)   13017811908
        Total length (>= 5000 bp)   11361640463
        Total length (>= 10000 bp)  10034318481
        Total length (>= 25000 bp)  6872368770 
        Total length (>= 50000 bp)  3406852776 
        # contigs                   1887964    
        Largest contig              297427     
        Total length                13450974050
        GC (%)                      38.76      
        N50                         25814      
        N75                         9780       
        L50                         139726     
        L75                         348468     
        # N's per 100 kbp           1703.76  


### Genome Busco:
&nbsp;&nbsp;&nbsp;/labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/genome/busco/run_Busco_ABAL1.1_wholeGenome/short_summary_Busco_ABAL1.1_wholeGenome.txt  

        C:30.5%[S:26.7%,D:3.8%],F:13.0%,M:56.5%,n:1440

        440     Complete BUSCOs (C)
        385     Complete and single-copy BUSCOs (S)
        55      Complete and duplicated BUSCOs (D)
        187     Fragmented BUSCOs (F)
        813     Missing BUSCOs (M)
        1440    Total BUSCO groups searched


### Genome Annotation Busco:

&nbsp;&nbsp;&nbsp;/labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/genes/silverFir/run_Busco_alba_annotation/

        C:19.4%[S:14.5%,D:4.9%],F:14.3%,M:66.3%,n:1440

        279     Complete BUSCOs (C)
        209     Complete and single-copy BUSCOs (S)
        70      Complete and duplicated BUSCOs (D)
        206     Fragmented BUSCOs (F)
        955     Missing BUSCOs (M)
        1440    Total BUSCO groups searched

### Transcriptome Busco:

&nbsp;&nbsp;&nbsp;/labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/transcriptomes/silverFir/run_Busco_silverFir_transcripts

        C:6.9%[S:6.7%,D:0.2%],F:8.1%,M:85.0%,n:1440

        100     Complete BUSCOs (C)
        97      Complete and single-copy BUSCOs (S)
        3       Complete and duplicated BUSCOs (D)
        117     Fragmented BUSCOs (F)
        1223    Missing BUSCOs (M)
        1440    Total BUSCO groups searched