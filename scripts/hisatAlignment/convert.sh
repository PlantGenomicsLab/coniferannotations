#!/bin/bash
# Submission script for Xanadu 
####SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --job-name=SamToBam
#SBATCH -o convert-%j.output
#SBATCH -e convert-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1
#SBATCH --nodes=1 
#SBATCH --cpus-per-task=16
#SBATCH --partition=general

module load samtools/0.1.19

myarr=()
for f in ./*.sam
do 
	out=($(echo "$f" | awk -F'[/_]' '{ print $2 }' | awk -F'[.]' '{print $1}'))
	echo $out
	samtools view -Sb $f > $out.bam
done
