#!/bin/bash
#SBATCH --job-name=gmap 
#SBATCH -n 8
#SBATCH -N 1
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o gmap_%j.out
#SBATCH -e gmap_%j.err


module load gmap

:' READ ALL THE COMMENTS FIRST
The K and L parameter have to deal mainly with intron lengths, if you do not have a species w/ absurdly long (> 100,000 bps), do not include this flag
-a assumes which frame the transcriptome is in (get rid of this if your script is crashing)
-d the directory in which the all of the gmap index files are
-D path to -d
--fulllength assumes that your query (in this case Merged_PacBio.Cluster.450bps.fasta) is full length i.e. all query sequences have start and stop
-n1 will only report the best alignment
--cross-species more sensitive search for canonical splicing, particularly if species of your query do not match to your target but helpful in general
-T will truncate the alignment around start and stop codon (note: this does NOT guarantee that in frame stop codons will be absent, use gFACs for that).
gmap has a lot of parameters, make sure you are using them correctly to achieve the optimal alignment. Make sure all the sequences in the query are being processed. To check for this, scroll to the bottom of the error file, it will say x queries processed in y seconds, make sure x==number of sequences in your query'


gmapl -K 1000000 -L 10000000 -a 1 --cross-species -D /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/index/5000bps/gmap/pita2.01_5000bps_softmasked/ -d pita2.01_5000bps_softmasked -f gff3_gene ../Merged_PacBio.Cluster.450bps.fasta --fulllength --nthreads=8 --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 -T > pacbio_genes_pita2.01._95_95.gff3 2> query95_1_updated_95_95.error
