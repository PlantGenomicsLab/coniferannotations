#!/bin/bash
# Submission script for Xanadu 
#SBATCH --job-name=sickle
#SBATCH -o sickle-%j.output
#SBATCH -e sickle-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=4
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general


### This sickle script is designed for PAIRED end reads and is path dependent!!!
### variable myarr tries to capture all of the accession numbers (eg. SRR3723923_1.fastq & SRR3723923_2.fastq)
### variable uniq_file tries to capture all of the UNIQUE accession numbers (eg. SRR3723923)
### the unique accession numbers are then used to specify the forward (var f1) files and reverse (var f2) files
### the forward and reverse file names are used to run sickle 

module load sickle/1.33

#for all files in path, split the path/to/file on "/" and "_" and save the variable $6 into myarr
#based on the path I provided the accession number for $6, your could be different based on your path

myarr=()
for f in ../../raw_reads/hiseq/*.fastq
do 
	myarr+=($(echo "$f" | awk -F'[/_]' '{ print $6 }'))
done

#print element in myarr to ensure capture of accession numbers
#for i in ${myarr[@]}; do echo $i; done

uniq_file=($(echo "${myarr[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))

for i in ${uniq_file[@]}
do
	#echo $i
	f1=$i"_1.fastq"
	f2=$i"_2.fastq"
	echo $f1
	echo $f2
	sickle pe -f ../../raw_reads/hiseq/$f1 -r ../../raw_reads/hiseq/$f2 -t sanger -o trimmed_$f1 -p trimmed_$f2 -s single_trimmed_$i.fastq -q 30 -l 50
done
