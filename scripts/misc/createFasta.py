import linecache
import argparse
import os
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq

parser = argparse.ArgumentParser(
     prog='createFasta.py',
     usage='''python createFasta.py --fasta [fasta file given by braker] --path [Path of fasta file] --nameList [name of list file] --pathList [path of the list file] --out [name of output fasta file]''',
     description='''This program pulls out specific sequences from a fasta file, given the fasta file and a list of sequences saved in a text file''',
     epilog='''It requires numpy and biopython libraries''')
parser.add_argument('--fasta', type=str, help='The name of the fasta file', required=True)
parser.add_argument('--path', type=str, help='The path of the fasta file', required=False)
parser.add_argument('--nameList', type=str, help='name of the list file that contains name of sequences', required=True)
parser.add_argument('--pathList', type=str, help='path of list file', required=False)
parser.add_argument('--out', type=str, help='name of output fasta file', required=True)

args=parser.parse_args()
fastapath=args.path
fasta=args.fasta
listName=args.nameList
listPath=args.pathList
output=args.out

if fastapath==None:
    fastafile=fasta
else:
    fastafile=os.path.join(fastapath, fasta)

if listPath==None:
    listFile=listName
else:
    listFile=os.path.join(listPath, listName)

headers=[line.rstrip('\n') for line in open(listFile)]
print headers[:5]
id_dict=SeqIO.to_dict(SeqIO.parse(fastafile, "fasta"))
identifiers = [id_dict[seq_record].id for seq_record in id_dict]
print identifiers[:5]

with open(output,'w') as seqs:
    for record in id_dict:
        if id_dict[record].id in headers:
            seqs.write(">"+id_dict[record].id+"\n")
            seqs.write(str(id_dict[record].seq)+"\n")
            headers.remove(id_dict[record].id)

with open("scaffs_left_to_softmask.txt", 'w') as out:
    for fasta in headers:
        out.write(fasta+"\n")

        
