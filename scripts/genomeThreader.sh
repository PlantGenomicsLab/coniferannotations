#!/bin/bash
# Submission script for Xanadu
####SBATCH --time=10-01:00:00 # days-hh:mm:ss
####SBATCH --mem=350GB
#SBATCH --job-name=gth
#SBATCH -o gth-%j.output
#SBATCH -e gth-%j.error
#SBATCH --mail-user=sararat.tosakoon@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=general
#SBATCH --array=1-499%20

module load genomethreader/1.6.6
#echo $SLURM_ARRAY_TASK_ID
## blat format: blat database query [-ooc=11.ooc] output.psl
echo genome_scaffolds_blathits.fasta"$SLURM_ARRAY_TASK_ID".fa

gth -genomic /UCHC/LABS/Wegrzyn/ConiferGenomes/Psme/analysis/braker/Braker2/prots/comp_combined_protein_filtered.fasta150aa.faa"$SLURM_ARRAY_TASK_ID".fa -protein ../completeProts150aa_MakerGeneModels.fasta -gff3out -startcodon -gcmincoverage 80 -finalstopcodon -introncutout -dpminexonlen 20 -skipalignmentout -o comp_combined_protein_filtered.fasta150aa.faa"$SLURM_ARRAY_TASK_ID".gth.gff3 -force -gcmaxgapwidth 1000000
