#!/bin/bash
# Submission script for Xanadu
#SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --job-name=prep
#SBATCH -o prep-%j.output
#SBATCH -e prep-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --partition=himem1,himem2,himem3,himem4,himem5
#SBATCH --mem=500G

module load augustus
module load perl
module load bamtools/2.4.1

###dealing with RNA-seq evidence
# convert the bam alignment file (Pita_AllBamMerged.bam) to "hints" (gff file format)
/isg/shared/apps/augustus/3.2.3/config/../bin/bam2hints --maxintronlen=1000000 --maxgenelen=1000000 --intronsonly --in=/UCHC/LABS/Wegrzyn/ConiferGenomes/Pila/analysis/hisat2/mergeBam/sortReadNames/Pila_AllBamMerged.bam --out=./bam2hints.temp.gff

# append the bam2hint hints to a temporary file (hintsfile.temp.gff)
cat ./bam2hints.temp.gff >> ./hintsfile.temp.gff

# sort hints of type rnaseq (hintsfile.temp.gff --> hints.rnaseq.temp.sort.gff)
cat ./hintsfile.temp.gff | sort -n -k 4,4 | sort -s -n -k 5,5 | sort -s -n -k 3,3 | sort -s -k 1,1 > ./hints.rnaseq.temp.sort.gff

# join multiple hints (hints.rnaseq.temp.sort.gff --> hintsfile.temp.gff)
perl /isg/shared/apps/augustus/3.2.3/scripts/join_mult_hints.pl <./hints.rnaseq.temp.sort.gff > ./hintsfile.temp.gff

# filter introns, find strand and change score to 'mult' entry (hintsfile.temp.gff --> hintsfile.gff )
perl /home/CAM/szaman/2.0.5/filterIntronsFindStrand.pl /isg/shared/databases/alignerIndex/plant/Pila/genome/v1.5/Pila.v.1.5.masked.fasta ./hintsfile.temp.gff --score 1 > ./hintsfile.gff

###dealing with protein evidence

# convert protein alignments (all.multiexonic.pita.v2.0.1.NewMasked.5k.gth.gff3) to "hints" (prot_hintsfile.aln2hints.temp.gff) specify program used for alignment (gth=genomethreader)
perl /home/CAM/szaman/2.0.5/align2hints.pl --maxintronlen=1000000 --in=all.multiexonics.protAligned.pilav1.05.gth.gff3 --out=./prot_hintsfile.aln2hints.temp.gff --prg=gth

# concatenating protein hints from prot_hintsfile.aln2hints.temp.gff to prot_hintsfile.temp.gff
cat ./prot_hintsfile.aln2hints.temp.gff >> ./prot_hintsfile.temp.gff

# sort hints of type prot
cat ./prot_hintsfile.temp.gff | sort -n -k 4,4 | sort -s -n -k 5,5 | sort -s -n -k 3,3 | sort -s -k 1,1 > ./hints.prot.temp.sort.gff

# join multiple hints (hints.prot.temp.sort.gff --> prot_hintsfile.temp.gff)
perl /isg/shared/apps/augustus/3.2.3/scripts/join_mult_hints.pl <./hints.prot.temp.sort.gff >./prot_hintsfile.temp.gff

# moving prot_hintsfile.temp.gff to prot_hintsfile.gff
mv ./prot_hintsfile.temp.gff ./prot_hintsfile.gff

# Deleting prot_hintsfile.temp.gff
rm prot_hintsfile.temp.gff

# joining protein and RNA-Seq hints files -> appending prot_hintsfile.gff to hintsfile.gff
cat ./prot_hintsfile.gff >> ./hintsfile.gff

