# Pre-processing Steps:
## Genome 
1. Remove scaffolds shorter than 5 kbps.
2. Softmask the resulting genome (from step 1).
## Raw reads
1. Execute sickle (sickle.sh) to trim the raw RNA-seq reads 

# Alignment
1. Refer to the hisatAlignment directory for aligning the reads to the processed genome.
2. Align protein sequences (protein sequences are derived from taking the transcriptome of species of interest and translating it) to genome (genomethreader.sh)

# Braker
1. Execute brakerlog.sh as described in the script --> resulting output: hintsfile.gff (referred to as "processing alignment evidence")
2. Execute braker.sh as described in the script using the hintsfile.gff created previously.
