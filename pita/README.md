# Loblolly pine &rarr; $PWD=/core/labs/Wegrzyn/ConiferGenomes/Pita/analysis/
## Data Location:
### Genome(s): 
#### Original Genome:
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.fa
&nbsp;&nbsp;&nbsp;[TreeGenes FTP](https://treegenesdb.org/FTP/Genomes/Pita/v2.01/genome/Pita.2_01.fa.gz)      
#### Softmasked Genome (scaffolds ≥ 5000bps):
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa 
&nbsp;&nbsp;&nbsp;[TreeGenes FTP](https://treegenesdb.org/FTP/Genomes/Pita/v2.01/genome/Pita.2_01.5kb.softmasked.fa)
### Raw Reads:
&nbsp;&nbsp;&nbsp;Raw reads must be downloaded from NCBI 
&nbsp;&nbsp;&nbsp;[NCBI Download](https://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GEUZ01)
&nbsp;&nbsp;&nbsp;[NCBI BioProject](https://www.ncbi.nlm.nih.gov/bioproject/?term=PRJNA174450)
### Repeats: 
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/Pita/repeats/CPRD_Pier2.fa  
&nbsp;&nbsp;&nbsp;[TreeGenes FTP](https://treegenesdb.org/FTP/Genomes/Pita/v2.0/Repeats/)  
### Transcriptome: 
&nbsp;&nbsp;&nbsp;$PWD/Genes/pacbio/Merged_PacBio.Cluster.fasta  
&nbsp;&nbsp;&nbsp;$PWD/Genes/illumina/Pita.IU.TranscriptomeMainsV1_450bp.fasta    
&nbsp;&nbsp;&nbsp;[NCBI Download](https://www.ncbi.nlm.nih.gov/Traces/wgs/?display=contigs&page=1)  
&nbsp;&nbsp;&nbsp;[Transcriptome Alignment Statistics](https://docs.google.com/spreadsheets/d/1OX8U8fRJrAjCevhEUl-cwmY3uiEJV09LeU9TlBt7b88/edit#gid=0)
### Previous Maker Annotation:
&nbsp;&nbsp;&nbsp; $PWD/Genes/maker/pita.HQgenes.complete.proteins.fasta 
### Quast:
    
        Assembly                    Pita.2_01  
        # contigs (>= 0 bp)         1755249    
        # contigs (>= 1000 bp)      1019156    
        # contigs (>= 5000 bp)      445868     
        # contigs (>= 10000 bp)     296299     
        # contigs (>= 25000 bp)     163592     
        # contigs (>= 50000 bp)     108318     
        Total length (>= 0 bp)      22104357184
        Total length (>= 1000 bp)   21656360207
        Total length (>= 5000 bp)   20423850986
        Total length (>= 10000 bp)  19356868532
        Total length (>= 25000 bp)  17299575524
        Total length (>= 50000 bp)  15330332824
        # contigs                   1489469    
        Largest contig              2141636    
        Total length                22001038100
        GC (%)                      37.44      
        N50                         111354     
        N75                         34473      
        L50                         51129      
        L75                         136346     
        # N's per 100 kbp           7145.87       
                
### Busco:

    BUSCO result of genome (genome limited to scaffolds > 5kbps):
    
    location:  
    
    $PWD/analysis/paper/busco/genomes/loblolly/v2.01/run_Busco_PITA_wholeGenome_5000bps/
    
    C:44.2%[S:35.3%,D:8.9%],F:7.1%,M:48.7%,n:1440

    BUSCO result on transcriptome (105,000 sequences - no filter):

    location: $PWD/analysis/paper/busco/transcriptomes/loblolly/run_Busco_pita_transcripts/

    C:68.6%[S:38.0%,D:30.6%],F:4.6%,M:26.8%,n:1440


    BUSCO results on transcriptome (individually, no filter):

    Illumina    C:53.4%[S:47.1%,D:6.3%],F:2.2%,M:44.4%,n:1440
    pacbio      C:42.5%[S:22.2%,D:20.3%],F:7.3%,M:50.2%,n:1440

    BUSCO results on transcriptome alignment (individually, filtered for < 9bps introns and exons):

    


### Genome Annotation
&nbsp;&nbsp;&nbsp; location: [TreeGenes FTP](https://treegenesdb.org/FTP/Genomes/Pita/v2.01/annotation/)  
&nbsp;&nbsp;&nbsp; BUSCO Results: C:46.6%[S:36.9%,D:9.7%],F:13.2%,M:40.2%,n:1440  
&nbsp;&nbsp;&nbsp; gfacs statistics:

    Number of genes:	51751
    Number of monoexonic genes:	3513
    Number of multiexonic genes:	48238

    Number of positive strand genes:	25869
    Monoexonic:	1873
    Multiexonic:	23996

    Number of negative strand genes:	25882
    Monoexonic:	1640
    Multiexonic:	24242

    Average overall gene size:	18354.528
    Median overall gene size:	2252
    Average overall CDS size:	1131.184
    Median overall CDS size:	936
    Average overall exon size:	277.407
    Median overall exon size:	167

    Average size of monoexonic genes:	1101.754
    Median size of monoexonic genes:	883.5
    Largest monoexonic gene:	5598
    Smallest monoexonic gene:	300

    Average size of multiexonic genes:	19610.985
    Median size of multiexonic genes:	2497
    Largest multiexonic gene:	1375330
    Smallest multiexonic gene:	315

    Average size of multiexonic CDS:	1133.327
    Median size of multiexonic CDS:	939
    Largest multiexonic CDS:	15537
    Smallest multiexonic CDS:	300

    Average size of multiexonic exons:	263.452
    Median size of multiexonic exons:	164
    Average size of multiexonic introns:	5596.180
    Median size of multiexonic introns:	185

    Average number of exons per multiexonic gene:	4.302
    Median number of exons per multiexonic gene:	3
    Largest multiexonic exon:	7998
    Smallest multiexonic exon:	9
    Most exons in one gene:	51

    Average number of introns per multiexonic gene:	3.302
    Median number of introns per multiexonic gene:	2
    Largest intron:	758522
    Smallest intron:	9

    The following columns do not involve codons:
    Number of complete models:	51751
    Number of 5' only incomplete models:	0
    Number of 3' only incomplete models:	0
    Number of 5' and 3' incomplete models:	0
    
### Miscellaneous
&nbsp;&nbsp;&nbsp; [loblolly pine manuscript](https://docs.google.com/document/d/1Rwavu63ltoy8mkgfT0KohXv_dIg7GxRDJJIt9AWF7FU/edit)
&nbsp;&nbsp;&nbsp; [Spreadsheet for Loblolly Pine Transcriptomes ](https://docs.google.com/spreadsheets/d/1OX8U8fRJrAjCevhEUl-cwmY3uiEJV09LeU9TlBt7b88/edit#gid=0)
