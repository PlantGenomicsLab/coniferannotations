STATS:
Number of genes:	28291
Number of monoexonic genes:	2448
Number of multiexonic genes:	25843

Number of positive strand genes:	14270
Monoexonic:	1277
Multiexonic:	12993

Number of negative strand genes:	14021
Monoexonic:	1171
Multiexonic:	12850

Average overall gene size:	12490.577
Median overall gene size:	2327
Average overall CDS size:	1143.552
Median overall CDS size:	945
Average overall exon size:	288.759
Median overall exon size:	174

Average size of monoexonic genes:	1026.546
Median size of monoexonic genes:	783
Largest monoexonic gene:	5598
Smallest monoexonic gene:	300

Average size of multiexonic genes:	13576.518
Median size of multiexonic genes:	2676
Largest multiexonic gene:	371577
Smallest multiexonic gene:	234

Average size of multiexonic CDS:	1154.635
Median size of multiexonic CDS:	957
Largest multiexonic CDS:	15537
Smallest multiexonic CDS:	300

Average size of multiexonic exons:	272.278
Median size of multiexonic exons:	169
Average size of multiexonic introns:	3833.156
Median size of multiexonic introns:	182

Average number of exons per multiexonic gene:	4.241
Median number of exons per multiexonic gene:	3
Largest multiexonic exon:	7998
Smallest multiexonic exon:	9
Most exons in one gene:	51

Average number of introns per multiexonic gene:	3.241
Median number of introns per multiexonic gene:	2
Largest intron:	284950
Smallest intron:	9

The following columns do not involve codons:
Number of complete models:	28291
Number of 5' only incomplete models:	0
Number of 3' only incomplete models:	0
Number of 5' and 3' incomplete models:	0
