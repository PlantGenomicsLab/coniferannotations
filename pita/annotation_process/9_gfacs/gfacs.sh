#!/bin/bash
#SBATCH --job-name=gfacs
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --mail-type=ALL
#SBATCH --mem=10G
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o pita_stats_%j.o
#SBATCH -e pita_stats_%j.e
#SBATCH --qos=general 

module load perl/5.24.0
perl /labs/Wegrzyn/gFACs/gFACs.pl  \
-f gFACs_gtf \
--statistics \
--min-CDS-size 300 \
--unique-genes-only \
--allowed-inframe-stop-codons 0 \
--min-exon-size 9 \
--min-intron-size 9 \
--get-fasta-without-introns \
--get-protein-fasta \
--create-gtf \
--fasta /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa \
-O ./gfacs/ \
out.atacseqVerified.gtf
