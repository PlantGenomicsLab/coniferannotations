# BUSCO version is: 3.0.2 
# The lineage dataset is: embryophyta_odb9 (Creation date: 2016-02-13, number of species: 30, number of BUSCOs: 1440)
# To reproduce this run: python /isg/shared/apps/busco/3.0.2b/scripts/run_BUSCO.py -i genes_without_introns.fasta.faa -o Busco_PITA_annotation_himem -l /isg/shared/databases/busco_lineages/embryophyta_odb9/ -m proteins -c 8
#
# Summarized benchmarking in BUSCO notation for file genes_without_introns.fasta.faa
# BUSCO was run in mode: proteins

	C:28.9%[S:22.4%,D:6.5%],F:8.4%,M:62.7%,n:1440

	417	Complete BUSCOs (C)
	323	Complete and single-copy BUSCOs (S)
	94	Complete and duplicated BUSCOs (D)
	121	Fragmented BUSCOs (F)
	902	Missing BUSCOs (M)
	1440	Total BUSCO groups searched
