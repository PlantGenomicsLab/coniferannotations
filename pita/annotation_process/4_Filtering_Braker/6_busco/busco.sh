#!/bin/bash
#SBATCH --job-name=busco
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=8G
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err

module load busco/3.0.2b
module unload augustus
export PATH=/home/CAM/szaman/augustus-3.2.3/bin:/home/CAM/szaman/augustus-3.2.3/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus-3.2.3/config
###if your run crashes uncomment the following:
#module unload blast/2.7.1
#module load blast/2.2.29 

run_BUSCO.py -i ../5_removed_softmasked_genes/entap_gfacs_interpro_removeRetero_remMasked.faa -l /isg/shared/databases/busco_lineages/embryophyta_odb9/ -o Busco_PITA_annotation -m prot -c 4
