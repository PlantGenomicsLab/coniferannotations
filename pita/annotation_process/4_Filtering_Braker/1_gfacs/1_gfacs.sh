#!/bin/bash
#SBATCH --job-name=gfacs-m
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=5G
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o gfacs-m-%j.o
#SBATCH -e gfacs-m-%j.e

module load perl/5.24.0

options="--statistics \
--get-protein-fasta \ 
--statistics-at-every-step \
--rem-extra-introns \
--min-CDS-size 100 \
--rem-monoexonics \
--rem-all-incompletes \
--unique-genes-only \
--splice-rescue \ 
--min-exon-size 20 \
--min-intron-size 11"


out="./"
genome="/isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.fa"

perl /labs/Wegrzyn/gFACs/gFACs.pl \
-f braker_2.05_gtf  \
$options \
--fasta $genome \
-O $out \
/labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/3_Running_Braker/results/augustus.hints.gtf
