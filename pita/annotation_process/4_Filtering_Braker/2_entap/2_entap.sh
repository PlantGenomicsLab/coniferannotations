#!/bin/bash
#SBATCH --job-name=entap
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 12
#SBATCH --mem=30G
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general
#SBATCH --qos=general


module load anaconda/2.4.0
module load perl/5.24.0
module load diamond/0.9.19
module load eggnog-mapper/0.99.1
module load interproscan/5.25-64.0  

/labs/Wegrzyn/EnTAP/EnTAP --runP -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /labs/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein_v2_150_headers.dmnd -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd -i ../1_gfacs/genes_without_introns.fasta.faa --qcoverage 70 --tcoverage 70 --taxon pinus --threads 32 --ontology 0 --out-dir ./entap_test
