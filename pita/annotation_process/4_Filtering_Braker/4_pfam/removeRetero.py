import linecache
import argparse
import os

parser = argparse.ArgumentParser(
     prog='createGFF.py',
     usage='''python createGFF.py --gff [gff file given by braker] --path [Path of gff file] --nameList [name of list file] --reteroList [path of the list file] --out [name of output fasta file]''',
     description='''This program pulls out the gff of specific genes, given the gff file and a list of sequences saved in a text file''',
     epilog='''It requires numpy and biopython libraries''')
parser.add_argument('--gff', type=str, help='The name of the gff file', required=True)
parser.add_argument('--path', type=str, help='The path of the gff file', required=False)
parser.add_argument('--nameList', type=str, help='name of the list file that contains name of sequences', required=True)
parser.add_argument('--reteroList', type=str, help='path of list file', required=True)
parser.add_argument('--out', type=str, help='name of output fasta file', required=True)

args=parser.parse_args()
path=args.path
gff=args.gff
listName=args.nameList
reteroList=args.reteroList
output=args.out

if path==None:
    gffFile=gff
else:
    gffFile=os.path.join(path, gff)


listFile=listName
reteroFile=reteroList

mrna=[]
mrnalines=[]
genes=[]
i=1
for row in open(gffFile,'r'):
    if "\tmRNA\t" in row or "\tgene\t" in row:
        cols=row.split("\t")
        id=cols[8].strip("\n")
        mrna.append(id[3:])
        mrnalines.append(i)
        geneID=id
        gene=cols[0]+"\t"+cols[1]+"\t"+"gene"+"\t"+cols[3]+"\t"+cols[4]+"\t"+cols[5]+"\t"+cols[6]+"\t"+cols[7]+"\t"+geneID[0]
        genes.append(gene)
    i+=1    
mrnalines.append(i)
print mrna[:5]

keep=[line.rstrip('\n') for line in open(listFile)]
discard=[line.rstrip('\n') for line in open(reteroFile)]
print keep[:5]
print discard[:5]

with open(output,'w') as seqs:
   for name in mrna:
       if name in keep and name not in discard:
           k=mrna.index(name)
           entry1=mrnalines[k]
           entry2=mrnalines[k+1]
           geneGFF=""
           #seqs.write("%s\n" %(genes[k]))
           for m in range(entry1,entry2):
	       geneGFF+=linecache.getline(gffFile,m)
           seqs.write("%s" %(geneGFF))

        
