#!/bin/bash
#SBATCH --job-name=interpro
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=30G
#SBATCH -o interpro_%j.out
#SBATCH -e interpro_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load interproscan/5.35-74.0 
module unload anaconda2/4.4.0   
module unload perl/5.28.1
module load perl/5.24.0
module load diamond/0.9.19

interproscan.sh -i ../2_entap/entap/final_annotated_removeAsterik.faa  -o ./final_annotated_pfam.gff3 -f gff3 -appl pfam -goterms -iprlookup




