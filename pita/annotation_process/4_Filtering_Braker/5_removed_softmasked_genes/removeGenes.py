import os
import linecache
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
import argparse

parser = argparse.ArgumentParser(
     prog='removeGenes.py',
     usage='''python removeGenes.py --fasta [fasta file] --path [Path of fasta file] --threshold [repetitive cut-off] --out [name of output fasta file] --pathOut [directory where output file should go] --gff [input gff] --pathGFF [path to input gff]''',
     description='''This program pulls out specific sequences from a fasta file, given the fasta file and a list of sequences saved in a text file''',
     epilog='''It requires numpy and biopython libraries''')
parser.add_argument('--fasta', type=str, help='The name of the fasta file', required=True)
parser.add_argument('--path', type=str, help='The path of the fasta file', required=False)
parser.add_argument('--threshold', type=str, help='cut-off for masking ', required=True)
parser.add_argument('--out', type=str, help='name of output fasta file', required=True)
parser.add_argument('--pathOut', type=str, help='path of output fasta file', required=False)
parser.add_argument('--gff', type=str, help='name of input gff file', required=True)
parser.add_argument('--pathGFF', type=str, help='path of input gff file', required=False)

args=parser.parse_args()
fastapath=args.path
fasta=args.fasta
threshold=args.threshold
output=args.out
outputPath=args.pathOut
gff=args.gff
pathGFF=args.pathGFF

if fastapath==None:
    fastafile=fasta
else:
    fastafile=os.path.join(fastapath,fasta)

if outputPath==None:
    outputfile=output
else:
    outputfile=os.path.join(outputPath,output)

if pathGFF==None:
    gfffile = open(gff,"r")     
else:
    gfffile=open(os.path.join(pathGFF, gff), "r")

id_dict = SeqIO.to_dict(SeqIO.parse(fastafile, "fasta"))

discard=[]
gffline=gfffile.read()
blocks = gffline.split("###") 
keep_block=[]
print(len(blocks))

for b in blocks: 
    lines=b.split("\n")
    CDS=""
    strand=""
    gene=""
    for line in lines:
        if "\tgene\t" in line:
            cols=line.split("\t")
            strand=cols[6]
            gene=cols[8]
            print(gene)
        elif "\tCDS\t" in line:
            cols=line.split("\t")
            CDS=CDS+id_dict[cols[0]].seq[int(cols[3])-1:int(cols[4])]
    if strand=="-":
        seq=CDS.reverse_complement()
    else:
        seq=CDS
    masked_bps=sum(map(str.islower, seq))
    if masked_bps/float(len(seq)) >= 0.7:
        discard.append(gene)
    else:
        keep_block.append(b)
print(len(discard))
print(len(keep_block))

with open (outputfile, 'w') as out: 
    for gene in keep_block:
        out.write(gene)
