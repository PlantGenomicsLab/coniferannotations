STATS:
Number of genes:	44224
Number of monoexonic genes:	
Number of multiexonic genes:	44224

Number of positive strand genes:	21854
Monoexonic:	
Multiexonic:	21854

Number of negative strand genes:	22370
Monoexonic:	
Multiexonic:	22370

Average overall gene size:	15261.890
Median overall gene size:	2385
Average overall CDS size:	1113.228
Median overall CDS size:	927
Average overall exon size:	261.841
Median overall exon size:	164

Average size of monoexonic genes:	0.000
Median size of monoexonic genes:	
Largest monoexonic gene:	
Smallest monoexonic gene:	

Average size of multiexonic genes:	15261.890
Median size of multiexonic genes:	2385
Largest multiexonic gene:	489139
Smallest multiexonic gene:	221

Average size of multiexonic CDS:	1113.228
Median size of multiexonic CDS:	927
Largest multiexonic CDS:	14451
Smallest multiexonic CDS:	119

Average size of multiexonic exons:	261.841
Median size of multiexonic exons:	164
Average size of multiexonic introns:	4351.376
Median size of multiexonic introns:	182

Average number of exons per multiexonic gene:	4.252
Median number of exons per multiexonic gene:	3
Largest multiexonic exon:	8121
Smallest multiexonic exon:	20
Most exons in one gene:	51

Average number of introns per multiexonic gene:	3.252
Median number of introns per multiexonic gene:	2
Largest intron:	349537
Smallest intron:	53

The following columns do not involve codons:
Number of complete models:	44224
Number of 5' only incomplete models:	0
Number of 3' only incomplete models:	0
Number of 5' and 3' incomplete models:	0
