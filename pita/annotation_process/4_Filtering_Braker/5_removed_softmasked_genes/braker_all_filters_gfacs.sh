#!/bin/bash
#SBATCH --job-name=gfacs
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=5G
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o gfacs-m-%j.o
#SBATCH -e gfacs-m-%j.e

module load perl/5.24.0

options="--statistics"

out="./"

perl /labs/Wegrzyn/gFACs/gFACs.pl \
-f gFACs_gtf  \
$options \
--fasta $genome \
-O $out \
entap_gfacs_interpro_removeRetero_remMasked_mod.gtf 
