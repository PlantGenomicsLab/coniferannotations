import sys

lines = [line.rstrip('\n') for line in open(sys.argv[1])]
bed = [line.rstrip('\n') for line in open(sys.argv[2])]

with open (sys.argv[3], "w") as xyz:
    for b in bed:
        cols = b.split('\t')
        if cols[3] not in lines:
            xyz.write("%s\n" %(b))
            

