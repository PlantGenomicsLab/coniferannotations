import argparse
import os
import linecache
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
import re

def bed(inputGFF):
    lines=inputGFF.split("\n")
    bedOut=""
    for line in lines:
        cols=line.split("\t")
        if len(cols)==9:
            if (cols[2]=="mRNA" or cols[2]=="gene") and cols[1]=="gth":
                id=cols[8].split(";")
                l=cols[0]+"\t"+str(int(cols[3])-1)+"\t"+cols[4]+"\t"+id[2][7:]+"\t.\t"+cols[6]+"\n"
                bedOut=bedOut+l
                break
            elif (cols[2]=="mRNA" or cols[2]=="gene") and cols[1]!="gth":
                id=cols[8][3:]
                l=cols[0]+"\t"+cols[3]+"\t"+cols[4]+"\t"+id+"\t.\t"+cols[6]+"\n"
                bedOut=bedOut+l
                break
    return bedOut

def parse(inputGFF, outputGFF):
    gffline = inputGFF.read()
    blocks = gffline.split("###") 
    with open (outputGFF, 'w') as out:
        for b in blocks:
            string=bed(b)
            out.write(string)
            
def main():
    parser = argparse.ArgumentParser(
        prog='gff2bed.py',
        usage='''python gff2bed.py --gff [gtf] --pathGFF [path of the gtf file] --out [name of output file]''',
        description='''parse the GFF output from genomeThreader, write out only multiexonic genes''',
        epilog='''It requires numpy and biopython libraries''')
    parser.add_argument('--gff', type=str, help='The name of gff file', required=True)
    parser.add_argument('--pathGFF', type=str, help='The path of the gff file', required=False)
    parser.add_argument('--out', type=str, help='The name of output file', required=False)
    args = parser.parse_args()
    pathGFF=args.pathGFF
    gff=args.gff
    output=args.out
    
    if pathGFF==None:
        gfffile = open(gff,"r")
    else:
        gfffile=open(os.path.join(pathGFF, gff), "r")

    parse(inputGFF=gfffile,outputGFF=output)

if __name__ == "__main__":
    main()
