#!/bin/bash

module load bedtools
bedtools intersect -a gmap_maker_pita_out.bed -b ../illumina/gmap_illumina_pita_out.bed ../pacbio/gmap_pacbio_pita_out.bed ../braker/braker.bed -wa -wb > AnyOverlap.txt

bedtools intersect -f 1.0 -a gmap_maker_pita_out.bed -b ../illumina/gmap_illumina_pita_out.bed ../pacbio/gmap_pacbio_pita_out.bed ../braker/braker.bed -wa -wb > CompleteOverlap.txt

diff -U $(wc -l < AnyOverlap.txt) AnyOverlap.txt CompleteOverlap.txt | sed -n 's/^-//p' > PartialOverlap.txt
 ##remove the first line in PartialOverlap.txt before proceeding
sed -i '1d' PartialOverlap.txt
awk -F '\t' '(NR>=1)&&(($3-$2)<($10-$9))' CompleteOverlap.txt > LongerGeneModel.txt
awk -F '\t' '(NR>=1)&&(($3-$2)==($10-$9))' CompleteOverlap.txt > EqualGeneModel.txt
cut -f7,8,9,10,11 LongerGeneModel.txt > keepLongerGenes.bed
cut -f4 LongerGeneModel.txt | sort - | uniq > discardMaker

###remove Maker genes that are completely encapsulated by longer gene models
python ../scripts/filterBed.py discardMaker gmap_maker_pita_out.bed gmap_maker_pita_remInternal.bed


###################

