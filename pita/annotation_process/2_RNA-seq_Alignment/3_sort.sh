#!/bin/bash
# Submission script for Xanadu 
#SBATCH --job-name=sortBam
#SBATCH -o sortBam-%j.output
#SBATCH -e sortBam-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1
#SBATCH --nodes=1 
#SBATCH --cpus-per-task=10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=40G

module load samtools

for f in ./SRR*.bam
do 
        name=($(echo "$f" | awk -F'[/_]' '{ print $2 }' | awk -F'[.]' '{print $1}'))
        prefix="sorted_"
        out=$prefix$name
        echo $out
        samtools sort -n --threads 10 --output-fmt=BAM -o $out $f
done
