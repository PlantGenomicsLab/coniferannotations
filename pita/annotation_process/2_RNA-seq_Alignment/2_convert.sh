#!/bin/bash
# Submission script for Xanadu 
#SBATCH --job-name=SamToBam
#SBATCH -o convert-%j.output
#SBATCH -e convert-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1
#SBATCH --nodes=1 
#SBATCH --cpus-per-task=8
#SBATCH --mem=40G
#SBATCH --partition=general
#SBATCH --qos=general

module load samtools/0.1.19

myarr=()
for f in ./*.sam
do 
        out=($(echo "$f" | awk -F'[/_]' '{ print $2 }' | awk -F'[.]' '{print $1}'))
        echo $out
        samtools view -Sb $f > $out.bam
done

