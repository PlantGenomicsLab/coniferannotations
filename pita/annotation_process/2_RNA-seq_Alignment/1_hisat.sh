#!/bin/bash
# Submission script for Xanadu 
#SBATCH --job-name=hisat_Pita
#SBATCH -o hisat-%j.output
#SBATCH -e hisat-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=100G ###(256G)

module load hisat2/2.0.5

myarr=()

for f in ../../sickle/trimmed_raw_reads/trimmed_SRR*.fastq
do 
	myarr+=($(echo "$f" | awk -F'[/_]' '{ print $8 }'))
done

#for i in ${myarr[@]}; do echo $i; done
uniq_file=($(echo "${myarr[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))

for i in ${uniq_file[@]}
do
	echo $i
	f1="../../sickle/trimmed_raw_reads/trimmed_"$i"_1.fastq"
	f2="../../sickle/trimmed_raw_reads/trimmed_"$i"_2.fastq"
	echo $f1
	echo $f2
	hisat2 -q --max-intronlen 2000000 -x /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/index/5000bps/hisat2/Pita2.01_genome -1 $f1 -2 $f2 -S $i.sam
done
