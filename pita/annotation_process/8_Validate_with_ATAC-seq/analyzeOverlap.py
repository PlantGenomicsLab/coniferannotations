import argparse
import os
import linecache
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
import re

#query_scaff     query_start     query_end       query_id        query_frame     query_strand    subject_scaff   subject_start   subject_end     query_len       subject_len     coverage
#scaffold129847  0       141016  ID=g571204;COMP .       +       scaffold129847  22703   22841   141016  138     1.0
#scaffold129847  0       141016  ID=g571204;COMP .       +       scaffold129847  111979  112180  141016  201     1.0
#scaffold117256  0       59700   ID=g140869;COMP .       -       scaffold117256  42596   42777   59700   181     1.0
#scaffold52941   0       92591   ID=g372736;COMP .       -       scaffold52941   4792    5052    92591   260     1.0
#scaffold52941   0       92591   ID=g372736;COMP .       -       scaffold52941   15234   15366   92591   132     1.0
#scaffold52941   0       92591   ID=g372736;COMP .       -       scaffold52941   62453   62820   92591   367     1.0
#scaffold52941   0       92591   ID=g372736;COMP .       -       scaffold52941   69247   69422   92591   175     1.0
#scaffold52941   0       92591   ID=g372736;COMP .       -       scaffold52941   78763   78955   92591   192     1.0


def analyzeOverlap(overlap):
    genesToKeep=[]
    overlapFile = open(overlap,"r")
    overlapLines = overlapFile.read()
    lines=overlapLines.split("\n")
    dictionary={}
    avg_overlap={}
    for line in lines:
        cols=line.split("\t")
        if len(cols)==12:
            if cols[3] in dictionary.keys():
                overlap_list=dictionary[cols[3]]
                overlap_list.append(float(cols[11]))
                dictionary[cols[3]]=overlap_list
            else:                
                #print(cols[11])
                dictionary[cols[3]]=[float(cols[11])]
    
    for key,value in dictionary.items():
        ol=value
        avg=sum(ol)/float(len(ol))
        if avg >= 0.7:
            genesToKeep.append(key)
    
    return(genesToKeep)
def parse(inputGFF, outputGFF, keep):
    gffline = inputGFF.read()
    blocks = gffline.split("###") 
    with open (outputGFF, 'w') as out:
        for b in blocks:
            lines=b.split("\n")
            for line in lines:
                cols=line.split("\t")
                if len(cols)==9:
                    if (cols[2]=="gene"):
                        id=cols[8]
                        if id in keep:
                            out.write(b)
                        break
            
def main():
    parser = argparse.ArgumentParser(
        prog='gff2bed.py',
        usage='''python analyzeOverlap.py --gff [gtf] --pathGFF [path of the gtf file] --overlap [overlap report] --out [name of output file]''',
        description='''parse the GFF output from genomeThreader, write out only multiexonic genes''',
        epilog='''It requires numpy and biopython libraries''')
    parser.add_argument('--gff', type=str, help='The name of gff file', required=True)
    parser.add_argument('--pathGFF', type=str, help='The path of the gff file', required=False)
    parser.add_argument('--out', type=str, help='The name of output file', required=False)
    parser.add_argument('--overlap', type=str, help='how many bps upstream and downstream', required=False)
    args = parser.parse_args()
    pathGFF=args.pathGFF
    gff=args.gff
    output=args.out
    overlap=args.overlap
    if pathGFF==None:
        gfffile = open(gff,"r")
    else:
        gfffile=open(os.path.join(pathGFF, gff), "r")
    genesToKeep=analyzeOverlap(overlap)
    parse(inputGFF=gfffile,outputGFF=output,keep=genesToKeep)

if __name__ == "__main__":
    main()
