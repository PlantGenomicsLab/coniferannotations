import argparse
import os
import linecache
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
import re

def bed(inputGFF,expand):
    lines=inputGFF.split("\n")
    bedOut=""
    for line in lines:
        cols=line.split("\t")
        if len(cols)==9:
            if (cols[2]=="CDS") and cols[1]=="gth":
                id=cols[8]
                l=cols[0]+"\t"+str(int(cols[3])-1)+"\t"+cols[4]+"\t"+id[2][7:]+"\t.\t"+cols[6]+"\n"
                bedOut=bedOut+l
                break
            elif (cols[2]=="gene") and cols[1]!="gth":
                id=cols[8]
                start=int(cols[3])-expand
                end=int(cols[4])
                if start < 0:
                    start=0
                l=cols[0]+"\t"+str(start)+"\t"+str(end)+"\t"+id+"\t.\t"+cols[6]+"\n"
                bedOut=bedOut+l
                break
    return bedOut

def parse(inputGFF, outputGFF, expand):
    gffline = inputGFF.read()
    blocks = gffline.split("###") 
    with open (outputGFF, 'w') as out:
        for b in blocks:
            string=bed(b,expand)
            out.write(string)
            
def main():
    parser = argparse.ArgumentParser(
        prog='gff2bed.py',
        usage='''python gff2bed.py --gff [gtf] --pathGFF [path of the gtf file] --expand [upstream/downstream] --out [name of output file]''',
        description='''parse the GFF output from genomeThreader, write out only multiexonic genes''',
        epilog='''It requires numpy and biopython libraries''')
    parser.add_argument('--gff', type=str, help='The name of gff file', required=True)
    parser.add_argument('--pathGFF', type=str, help='The path of the gff file', required=False)
    parser.add_argument('--out', type=str, help='The name of output file', required=False)
    parser.add_argument('--expand', type=int, help='how many bps upstream and downstream', required=False)
    args = parser.parse_args()
    pathGFF=args.pathGFF
    gff=args.gff
    output=args.out
    expand=args.expand
    if pathGFF==None:
        gfffile = open(gff,"r")
    else:
        gfffile=open(os.path.join(pathGFF, gff), "r")

    parse(inputGFF=gfffile,outputGFF=output,expand=expand)

if __name__ == "__main__":
    main()
