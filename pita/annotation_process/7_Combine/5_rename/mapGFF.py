import os
import linecache
import argparse

fastanames=[]
fastalines=[]
lines = [line.rstrip('\n') for line in open('../3_filter_gfacs_with_EnTAP/genes_without_introns_annotated.fasta')]
i=1
for row in lines:
    if row.startswith(">") == True:
      fastanames.append(row[1:])
      fastalines.append(i)
    i+=1
fastalines.append(i)

print len(fastanames)
print len(fastalines)
print fastanames[0]

newnames=[]
newlines=[]
lines = [line.rstrip('\n') for line in open('./PITA.2_01.cds.fa')]
i=1
for row in lines:
    if row.startswith(">") == True:
      newnames.append(row[1:])
      newlines.append(i)
    i+=1
newlines.append(i)

print len(newnames)
print len(newlines)
print fastanames[0]

def map(block):
    global fastanames
    newID=""
    lines=block.split("\n")
    if len(lines)>0:
        for l in lines:
            cols=l.split("\t")
            if len(cols)==9:
                if cols[2]=="gene":
                    ids=cols[8].strip("\n")
                    if ids in fastanames:
                        newID=newnames[fastanames.index(ids)]
                        break
                    else:
                        print "id does not exist in fastanames ", ids[3:]
    newgff=""
    for l in lines:
        cols=l.split("\t")
        if len(cols)==9:
            cols[8]=newID
            newLines= '\t'.join(cols)+"\n"
            newgff=newgff+newLines

    return newgff

def parse(inputGFF, outputGFF):
    gffline = inputGFF.read()
    blocks = gffline.split("###") 
    with open (outputGFF, 'w') as out:
        for b in blocks:
            if len(b.split("\n")) > 0:
                writeOut=map(b)
                out.write(writeOut)
                out.write("###\n")

def main():
    
    parser = argparse.ArgumentParser(
        prog='mapGFF.py',
        usage='''python mapGFF.py --gff [gtf] --pathGFF [path of the gtf file] --out [name of output file]''',
        description='''parse the GFF output from genomeThreader, write out only multiexonic genes''',
        epilog='''It requires numpy and biopython libraries''')
    parser.add_argument('--gff', type=str, help='The name of gff file', required=True)
    parser.add_argument('--pathGFF', type=str, help='The path of the gff file', required=False)
    parser.add_argument('--out', type=str, help='The name of output file', required=False)
    args = parser.parse_args()
    pathGFF=args.pathGFF
    gff=args.gff
    output=args.out
    
    if pathGFF==None:
        gfffile = open(gff,"r")
    else:
        gfffile=open(os.path.join(pathGFF, gff), "r")

    parse(inputGFF=gfffile,outputGFF=output)

if __name__ == "__main__":
    main()
