#!/bin/bash
# Submission script for Xanadu 
####SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --job-name=rename
#SBATCH -o rename-%j.output
#SBATCH -e rename-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1
#SBATCH --nodes=1 
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --mem=10G
#SBATCH --qos=general


cp ../3_filter_gfacs_with_EnTAP/out_annotated.gtf ./
python header.py
python mapGFF.py --gff out_annotated.gtf --out PITA.2_01.gtf
