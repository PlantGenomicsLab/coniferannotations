#!/bin/bash
#SBATCH --job-name=entap
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 12
#SBATCH --mem=30G
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=himem
#SBATCH --qos=himem

module load anaconda/2.4.0
module load perl/5.24.0
module load diamond/0.9.19
module load interproscan/5.25-64.0  

/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP/EnTAP --runP -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /labs/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein_v2_150_headers.dmnd -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd -i ../PITA.2_01.peptides.fa --qcoverage 70 --tcoverage 70 --taxon pinus --threads 12 --ontology 0
