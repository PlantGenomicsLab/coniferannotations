Sun Feb  2 17:04:47 2020: Start - EnTAP
Sun Feb  2 17:04:47 2020: No inputted config file, using default: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_config.txt
Sun Feb  2 17:04:47 2020: Parsing configuration file...
Sun Feb  2 17:04:47 2020: Config file found at: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_config.txt
Sun Feb  2 17:04:47 2020: Success!
Sun Feb  2 17:04:47 2020: Assigning execution paths. Note they are not checked for validity yet...
Sun Feb  2 17:04:47 2020: Success! All exe paths set
Sun Feb  2 17:04:47 2020: ------------------------------------------------------
EnTAP Run Information - Execution
------------------------------------------------------
Current EnTAP Version: 0.9.0
Start time: Sun Feb  2 17:04:47 2020

Working directory has been set to: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles

Execution Paths/Commands:

RSEM Directory: /labs/Wegrzyn/EnTAP/libs/RSEM-1.3.0
GeneMarkS-T: perl /labs/Wegrzyn/EnTAP/libs/gmst_linux_64/gmst.pl
DIAMOND: diamond
InterPro: interproscan.sh
EggNOG SQL Database: /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP/databases/databases/eggnog.db
EggNOG DIAMOND Database: /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP/databases/bin/eggnog_proteins.dmnd
EnTAP Database (binary): /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP/databases/bin/entap_database.bin
EnTAP Database (SQL): /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//databases/entap_database.db
EnTAP Graphing Script: /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py

User Inputs:

contam: null
data-type: 0 
database: /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd /labs/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein_v2_150_headers.dmnd /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd 
e: 1.00e-05
fpkm: 0.50
input: ../1_gfacs/gfacs/genes_without_introns.fasta.faa
level: 0 3 4 
ontology: 0 
out-dir: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles
output-format: 1 4 3 
protein: pfam 
qcoverage: 70.00
runP: null
state: +
taxon: pinus
tcoverage: 70.00
threads: 12


Sun Feb  2 17:04:47 2020: User has input a database at: /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd
Sun Feb  2 17:04:47 2020: User has input a database at: /labs/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein_v2_150_headers.dmnd
Sun Feb  2 17:04:47 2020: User has input a database at: /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd
Sun Feb  2 17:04:47 2020: Verifying EnTAP database...
Sun Feb  2 17:04:47 2020: Reading serialized database from: /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP/databases/bin/entap_database.bin
 Of type: 2
Sun Feb  2 17:05:18 2020: Success!
Sun Feb  2 17:05:18 2020: Taxonomic species verified
Sun Feb  2 17:05:18 2020: Verifying software...
Sun Feb  2 17:05:18 2020: Executing command: 
diamond --version
Sun Feb  2 17:05:18 2020: 
Std Err:

Sun Feb  2 17:05:18 2020: Executing command: 
diamond --version
Sun Feb  2 17:05:19 2020: 
Std Err:

Sun Feb  2 17:05:19 2020: Success!
Sun Feb  2 17:05:19 2020: Success! Input verified
Sun Feb  2 17:05:19 2020: Killing Object - EntapDatabase
Sun Feb  2 17:05:32 2020: EnTAP Executing...
Sun Feb  2 17:05:32 2020: verifying state...
Sun Feb  2 17:05:32 2020: verifying state...
Sun Feb  2 17:05:32 2020: Success!
Sun Feb  2 17:05:32 2020: Processing transcriptome...
Sun Feb  2 17:05:32 2020: Transcriptome Lines - START
Sun Feb  2 17:05:32 2020: >c29537_f1p1_1403
Sun Feb  2 17:05:32 2020: NHMRASIEVKRNFPSSDVRLFLICELTPRVYSLIAAYGICILIFSSSVISFHQRHNPAIFFCYKQCRYCPNPQCVYTRIRKDCHHFRDPHPNPLRSTNAIPPQLYSPPTYPKDLNTLIFLHVVLLELSVFSALYLRATEFQSRRN*
Sun Feb  2 17:05:32 2020: >c2392_f2p0_1317
Sun Feb  2 17:05:32 2020: HYPKTTQRALAVGRSREAMVMATTVLLVCLSLFIHVLNTEQSLRLRLPLEDAKAENTLNSEYYRPLIGIVTHPGDGARGKLSTAPNASYIAASYVKFVESGGARVVPLIYNEPPEVLEQKFRAVNGLLFTGGSAKYGPYQQTVEKLFWRALEENDNGEYFPVYAVCLGFEILSMIVSKDLNILERFDAVNHPSTLSFESDAAKNSSIFKWFQPHILSKMTSEPLLFQNHMTYVSTMEAYNYPVTAVQWHPEKNTFEWEIPTIPHSADAIQVTQSVANFVISEARKSSHRPTFQQEEEYLIYNNFVAYSGKVGASFDQVYLFT*
Sun Feb  2 17:05:32 2020: >maker-jcf7180063005123-snap-gene-0.3-mRNA-1-transcript-offset:0-AED:0.10-eAED:0.26-QI:0|0|0|1|0|0|2|0|134
Sun Feb  2 17:05:32 2020: MIITTCQEIMLLLFNEFGRHHISIQFERCRCYYSITLNCMCKLKQVSKTIIRSDNFEFIEMFTQKLTNIKIPLPLVDEKKEVIEVVNKEICYIIKAKIAHLWKGKIVLPQQQLMSNYLKQFQQIFDPKIEAIKK*
Sun Feb  2 17:05:32 2020: >ID=g212594.3;COMP
Sun Feb  2 17:05:32 2020: MAGLISPSTLSSGDCFSFSPLCRQNPCDFSTVAGFFRIKNGADLHQKMKRAHFRVSGISANTDGSVFKNRIRGARPSKSVVSSEYSYSPGASEKLDFDIVIIGAGIIGLTIARQLLLNTDFSVAIVDARGSCAGATGAGQGYIWMGYRTPESDKWELEARSKQLWEEFVQEVEASGLEPLKLLGWKKTGSLLVGTTSEESAALQERVKLLTKAGIRAEFCSASSMHLMEPAVEVGKEGGAAFVPDDSQIDAKLAVSFIQEKNRAFSSQGRYEEFFEEPAVHFLRSNRKGHIEGVQTSKRVLYGNKAVIIAAGAWSCSLIEKMAEELSFPLHVPVKPRKMVARIEDISC*
Sun Feb  2 17:05:32 2020: >snap_masked-jcf7180062986231-processed-gene-0.1-mRNA-1-transcript-offset:0-AED:0.08-eAED:0.08-QI:0|0|0|0.66|1|1|3|0|248
Sun Feb  2 17:05:32 2020: Transcriptome Lines - END
Sun Feb  2 17:05:34 2020: Success!
Sun Feb  2 17:05:34 2020: Spawn Object - GraphingManager
Sun Feb  2 17:05:34 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -s -1 -g -1 -i /temp -t temp
Sun Feb  2 17:05:40 2020: 
Std Err:

Sun Feb  2 17:05:40 2020: Graphing is supported
Sun Feb  2 17:05:40 2020: Reading serialized database from: /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP/databases/bin/entap_database.bin
 Of type: 2
Sun Feb  2 17:06:10 2020: STATE - EXPRESSION FILTERING
Sun Feb  2 17:06:10 2020: No alignment file specified, skipping expression analysis
Sun Feb  2 17:06:10 2020: verifying state...
Sun Feb  2 17:06:10 2020: STATE - FRAME SELECTION
Sun Feb  2 17:06:10 2020: Protein sequences input, skipping frame selection
Sun Feb  2 17:06:10 2020: verifying state...
Sun Feb  2 17:06:10 2020: Beginning to copy final transcriptome to be used...
Sun Feb  2 17:06:10 2020: Success! Copied to: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/transcriptomes//genes_without_introns_final.fasta
Sun Feb  2 17:06:10 2020: verifying state...
Sun Feb  2 17:06:10 2020: STATE - SIMILARITY SEARCH
Sun Feb  2 17:06:10 2020: Spawn Object - SimilaritySearch
Sun Feb  2 17:06:10 2020: opendir: Path could not be read: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/figures/
Sun Feb  2 17:06:10 2020: opendir: Path could not be read: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed/
Sun Feb  2 17:06:10 2020: opendir: Path could not be read: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/overall_results
Sun Feb  2 17:06:10 2020: Spawn Object - ModDiamond
Sun Feb  2 17:06:10 2020: Verifying previous execution of database: /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd...
Sun Feb  2 17:06:10 2020: File for database uniprot_sprot does not exist.
/labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_uniprot_sprot.out
Sun Feb  2 17:06:10 2020: Verifying previous execution of database: /labs/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein_v2_150_headers.dmnd...
Sun Feb  2 17:06:10 2020: File for database conifer_geneSet_protein_v2_150_headers does not exist.
/labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_conifer_geneSet_protein_v2_150_headers.out
Sun Feb  2 17:06:10 2020: Verifying previous execution of database: /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd...
Sun Feb  2 17:06:10 2020: File for database plant does not exist.
/labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_plant.out
Sun Feb  2 17:06:10 2020: Success! Verified files for DIAMOND, continuing...
Sun Feb  2 17:06:10 2020: Executing DIAMOND for necessary files....
Sun Feb  2 17:06:10 2020: File not found, executing against database at: /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd
Sun Feb  2 17:06:10 2020: Executing command: 
diamond blastp -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd --query-cover 70.000000 --subject-cover 70.000000 --evalue 0.000010 --more-sensitive --top 3 -q /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/transcriptomes//genes_without_introns_final.fasta -o /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_uniprot_sprot.out -p 12 -f 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovhsp stitle
Sun Feb  2 17:12:16 2020: 
Std Err:

Sun Feb  2 17:12:16 2020: 
Printing to files:
Std Out: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_uniprot_sprot.out_std.out
Std Err: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_uniprot_sprot.out_std.err
Sun Feb  2 17:12:16 2020: Success! Results written to: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_uniprot_sprot.out
Sun Feb  2 17:12:16 2020: File not found, executing against database at: /labs/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein_v2_150_headers.dmnd
Sun Feb  2 17:12:17 2020: Executing command: 
diamond blastp -d /labs/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein_v2_150_headers.dmnd --query-cover 70.000000 --subject-cover 70.000000 --evalue 0.000010 --more-sensitive --top 3 -q /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/transcriptomes//genes_without_introns_final.fasta -o /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_conifer_geneSet_protein_v2_150_headers.out -p 12 -f 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovhsp stitle
Sun Feb  2 17:29:17 2020: 
Std Err:

Sun Feb  2 17:29:17 2020: 
Printing to files:
Std Out: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_conifer_geneSet_protein_v2_150_headers.out_std.out
Std Err: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_conifer_geneSet_protein_v2_150_headers.out_std.err
Sun Feb  2 17:29:17 2020: Success! Results written to: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_conifer_geneSet_protein_v2_150_headers.out
Sun Feb  2 17:29:17 2020: File not found, executing against database at: /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd
Sun Feb  2 17:29:17 2020: Executing command: 
diamond blastp -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd --query-cover 70.000000 --subject-cover 70.000000 --evalue 0.000010 --more-sensitive --top 3 -q /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/transcriptomes//genes_without_introns_final.fasta -o /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_plant.out -p 12 -f 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovhsp stitle
Sun Feb  2 20:07:17 2020: 
Std Err:

Sun Feb  2 20:07:17 2020: 
Printing to files:
Std Out: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_plant.out_std.out
Std Err: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_plant.out_std.err
Sun Feb  2 20:07:17 2020: Success! Results written to: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_plant.out
Sun Feb  2 20:07:17 2020: Beginning to filter individual DIAMOND files...
Sun Feb  2 20:07:17 2020: DIAMOND file located at /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_uniprot_sprot.out being parsed
Sun Feb  2 20:07:17 2020: Database file at /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_uniprot_sprot.out
Determined to be UniProt
Sun Feb  2 20:07:23 2020: File parsed, calculating statistics and writing output...
Sun Feb  2 20:07:26 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//uniprot_sprot/figures//_species_bar.png -t uniprot_sprot_Top_10_Species_Distribution -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//uniprot_sprot/figures//_species_bar.txt 
Sun Feb  2 20:07:32 2020: 
Std Err:

Sun Feb  2 20:07:32 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 2 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//uniprot_sprot/figures//_summary_bar.png -t uniprot_sprot_Summary -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//uniprot_sprot/figures//_summary_bar.txt 
Sun Feb  2 20:07:34 2020: 
Std Err:

Sun Feb  2 20:07:34 2020: Success!
Sun Feb  2 20:07:34 2020: DIAMOND file located at /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_conifer_geneSet_protein_v2_150_headers.out being parsed
Sun Feb  2 20:07:58 2020: File parsed, calculating statistics and writing output...
Sun Feb  2 20:08:03 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//conifer_geneSet_protein_v2_150_headers/figures//_species_bar.png -t conifer_geneSet_protein_v2_150_headers_Top_10_Species_Distribution -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//conifer_geneSet_protein_v2_150_headers/figures//_species_bar.txt 
Sun Feb  2 20:08:05 2020: 
Std Err:
Traceback (most recent call last):
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 282, in <module>
    main()
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 279, in main
    create_graphs(_software_flag)
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 139, in create_graphs
    sim_search_graphs()
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 112, in sim_search_graphs
    InputValues = parse_input(_stats_path)
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 236, in parse_input
    UserVals.values.append(int(values[1]))
IndexError: list index out of range

Sun Feb  2 20:08:05 2020: 
Error generating graph from:
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//conifer_geneSet_protein_v2_150_headers/figures//_species_bar.png -t conifer_geneSet_protein_v2_150_headers_Top_10_Species_Distribution -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//conifer_geneSet_protein_v2_150_headers/figures//_species_bar.txt 
Sun Feb  2 20:08:05 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 2 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//conifer_geneSet_protein_v2_150_headers/figures//_summary_bar.png -t conifer_geneSet_protein_v2_150_headers_Summary -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//conifer_geneSet_protein_v2_150_headers/figures//_summary_bar.txt 
Sun Feb  2 20:08:06 2020: 
Std Err:

Sun Feb  2 20:08:06 2020: Success!
Sun Feb  2 20:08:06 2020: DIAMOND file located at /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_plant.out being parsed
Sun Feb  2 20:49:17 2020: File parsed, calculating statistics and writing output...
Sun Feb  2 20:49:33 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//plant/figures//_species_bar.png -t plant_Top_10_Species_Distribution -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//plant/figures//_species_bar.txt 
Sun Feb  2 20:49:36 2020: 
Std Err:

Sun Feb  2 20:49:36 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 2 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//plant/figures//_summary_bar.png -t plant_Summary -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//plant/figures//_summary_bar.txt 
Sun Feb  2 20:49:38 2020: 
Std Err:

Sun Feb  2 20:49:38 2020: Success!
Sun Feb  2 20:49:38 2020: Calculating overall Similarity Searching statistics...
Sun Feb  2 20:49:41 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/overall_results/figures//_species_bar.png -t _Top_10_Species_Distribution -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/overall_results/figures//_species_bar.txt 
Sun Feb  2 20:49:42 2020: 
Std Err:
Traceback (most recent call last):
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 282, in <module>
    main()
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 279, in main
    create_graphs(_software_flag)
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 139, in create_graphs
    sim_search_graphs()
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 112, in sim_search_graphs
    InputValues = parse_input(_stats_path)
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 236, in parse_input
    UserVals.values.append(int(values[1]))
IndexError: list index out of range

Sun Feb  2 20:49:42 2020: 
Error generating graph from:
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/overall_results/figures//_species_bar.png -t _Top_10_Species_Distribution -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/overall_results/figures//_species_bar.txt 
Sun Feb  2 20:49:42 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 2 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/overall_results/figures//_summary_bar.png -t _Summary -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/overall_results/figures//_summary_bar.txt 
Sun Feb  2 20:49:44 2020: 
Std Err:

Sun Feb  2 20:49:44 2020: Success!
Sun Feb  2 20:49:44 2020: verifying state...
Sun Feb  2 20:49:44 2020: STATE - GENE ONTOLOGY
Sun Feb  2 20:49:44 2020: Spawn Object - Ontology
Sun Feb  2 20:49:44 2020: opendir: Path could not be read: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures/
Sun Feb  2 20:49:44 2020: opendir: Path could not be read: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/processed/
Sun Feb  2 20:49:44 2020: Spawn Object - ModEggnogDMND
Sun Feb  2 20:49:44 2020: Overwrite was unselected, verifying output files...
Sun Feb  2 20:49:44 2020: 
File Status at: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/blastp_genes_without_introns_eggnog_proteins.out
    Error: File empty
    Error: File could not be found
    Error: File could not be opened
Sun Feb  2 20:49:44 2020: Errors in opening file, continuing with execution...
Sun Feb  2 20:49:44 2020: Running EggNOG against Diamond database...
Sun Feb  2 20:49:44 2020: Executing command: 
diamond blastp -d /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP/databases/bin/eggnog_proteins.dmnd --top 1 --more-sensitive -q /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/transcriptomes//genes_without_introns_final.fasta -o /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/blastp_genes_without_introns_eggnog_proteins.out -p 12 -f 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovhsp stitle
Sun Feb  2 22:24:14 2020: 
Std Err:

Sun Feb  2 22:24:14 2020: 
Printing to files:
Std Out: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/blastp_genes_without_introns_eggnog_proteins__std.out
Std Err: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/blastp_genes_without_introns_eggnog_proteins__std.err
Sun Feb  2 22:24:14 2020: Parsing EggNOG DMND file located at: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/blastp_genes_without_introns_eggnog_proteins.out
Sun Feb  2 22:24:14 2020: Beginning to parse EggNOG results...
Sun Feb  2 22:24:14 2020: Alignments parsed: 5000
Sun Feb  2 22:24:15 2020: Alignments parsed: 10000
Sun Feb  2 22:24:15 2020: Alignments parsed: 15000
Sun Feb  2 22:24:17 2020: Alignments parsed: 20000
Sun Feb  2 22:24:17 2020: Alignments parsed: 25000
Sun Feb  2 22:24:18 2020: Alignments parsed: 30000
Sun Feb  2 22:24:18 2020: Alignments parsed: 35000
Sun Feb  2 22:24:19 2020: Alignments parsed: 40000
Sun Feb  2 22:24:19 2020: Alignments parsed: 45000
Sun Feb  2 22:24:19 2020: Alignments parsed: 50000
Sun Feb  2 22:24:20 2020: Alignments parsed: 55000
Sun Feb  2 22:24:20 2020: Alignments parsed: 60000
Sun Feb  2 22:24:21 2020: Alignments parsed: 65000
Sun Feb  2 22:24:21 2020: Alignments parsed: 70000
Sun Feb  2 22:24:22 2020: Alignments parsed: 75000
Sun Feb  2 22:24:22 2020: Alignments parsed: 80000
Sun Feb  2 22:24:22 2020: Alignments parsed: 85000
Sun Feb  2 22:24:23 2020: Alignments parsed: 90000
Sun Feb  2 22:24:23 2020: Alignments parsed: 95000
Sun Feb  2 22:24:25 2020: Alignments parsed: 100000
Sun Feb  2 22:24:25 2020: Alignments parsed: 105000
Sun Feb  2 22:24:25 2020: Alignments parsed: 110000
Sun Feb  2 22:24:26 2020: Alignments parsed: 115000
Sun Feb  2 22:24:26 2020: Success!
Sun Feb  2 22:24:26 2020: Success! Calculating statistics and accessing database...
Sun Feb  2 22:24:26 2020: Opening SQL database...
Sun Feb  2 22:24:26 2020: Opening SQL database at: /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP/databases/databases/eggnog.db
Sun Feb  2 22:24:26 2020: Success!
Sun Feb  2 22:24:26 2020: Getting EggNOG database SQL version...
Sun Feb  2 22:24:26 2020: Executing sql cmd: SELECT version FROM version
Sun Feb  2 22:24:26 2020: WARNING: couldn't get SQL version.Setting to earlier version by default
Mon Feb  3 00:16:06 2020: Killing object - EggNOG Database
Mon Feb  3 00:16:06 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//eggnog_tax_scope.png -t Top_Tax_Levels -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//eggnog_tax_scope.txt 
Mon Feb  3 00:16:09 2020: 
Std Err:

Mon Feb  3 00:16:09 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//cellular_component0_go_bar_graph.png -t Top_GO_Cellular_Terms_Level:_0 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//cellular_component0_go_bar_graph.txt 
Mon Feb  3 00:16:11 2020: 
Std Err:

Mon Feb  3 00:16:11 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//molecular_function0_go_bar_graph.png -t Top_GO_Molecular_Terms_Level:_0 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//molecular_function0_go_bar_graph.txt 
Mon Feb  3 00:16:13 2020: 
Std Err:

Mon Feb  3 00:16:14 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//overall0_go_bar_graph.png -t Top_GO_Terms_Level:_0 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//overall0_go_bar_graph.txt 
Mon Feb  3 00:16:16 2020: 
Std Err:

Mon Feb  3 00:16:16 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//biological_process0_go_bar_graph.png -t Top_GO_Biological_Terms_Level:_0 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//biological_process0_go_bar_graph.txt 
Mon Feb  3 00:16:18 2020: 
Std Err:

Mon Feb  3 00:16:18 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//cellular_component3_go_bar_graph.png -t Top_GO_Cellular_Terms_Level:_3 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//cellular_component3_go_bar_graph.txt 
Mon Feb  3 00:16:20 2020: 
Std Err:

Mon Feb  3 00:16:20 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//molecular_function3_go_bar_graph.png -t Top_GO_Molecular_Terms_Level:_3 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//molecular_function3_go_bar_graph.txt 
Mon Feb  3 00:16:23 2020: 
Std Err:

Mon Feb  3 00:16:23 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//overall3_go_bar_graph.png -t Top_GO_Terms_Level:_3 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//overall3_go_bar_graph.txt 
Mon Feb  3 00:16:25 2020: 
Std Err:

Mon Feb  3 00:16:25 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//biological_process3_go_bar_graph.png -t Top_GO_Biological_Terms_Level:_3 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//biological_process3_go_bar_graph.txt 
Mon Feb  3 00:16:27 2020: 
Std Err:

Mon Feb  3 00:16:27 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//cellular_component4_go_bar_graph.png -t Top_GO_Cellular_Terms_Level:_4 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//cellular_component4_go_bar_graph.txt 
Mon Feb  3 00:16:30 2020: 
Std Err:

Mon Feb  3 00:16:30 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//molecular_function4_go_bar_graph.png -t Top_GO_Molecular_Terms_Level:_4 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//molecular_function4_go_bar_graph.txt 
Mon Feb  3 00:16:32 2020: 
Std Err:

Mon Feb  3 00:16:32 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//overall4_go_bar_graph.png -t Top_GO_Terms_Level:_4 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//overall4_go_bar_graph.txt 
Mon Feb  3 00:16:34 2020: 
Std Err:

Mon Feb  3 00:16:34 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//biological_process4_go_bar_graph.png -t Top_GO_Biological_Terms_Level:_4 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//biological_process4_go_bar_graph.txt 
Mon Feb  3 00:16:36 2020: 
Std Err:

Mon Feb  3 00:16:36 2020: Success! EggNOG results parsed
Mon Feb  3 00:16:37 2020: Killing Object - ModEggnogDMND
Mon Feb  3 00:16:37 2020: Beginning to print final results...
Mon Feb  3 00:17:10 2020: Success!
Mon Feb  3 00:17:10 2020: verifying state...
Mon Feb  3 00:17:10 2020: Pipeline finished! Calculating final statistics...
Mon Feb  3 00:17:10 2020: Killing Object - QueryData
Mon Feb  3 00:17:18 2020: QuerySequence data freed
Mon Feb  3 00:17:18 2020: Killing Object - EntapDatabase
Mon Feb  3 00:17:30 2020: End - EnTAP
Mon Feb  3 00:17:30 2020: Killing object - FileSystem
Mon Feb  3 00:17:30 2020: Killing object - UserInput
