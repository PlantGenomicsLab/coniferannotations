gFACs LOG

Version: 1.1.2 (03/19/2020)
Time of run: Sat Mar 21 00:12:43 2020

Command:/labs/Wegrzyn/gFACs/gFACs.pl -f gFACs_gtf --statistics --unique-genes-only --min-CDS-size 300 --allowed-inframe-stop-codons 0 --min-exon-size 9 --min-intron-size 9 --get-fasta --get-protein-fasta --create-gtf --fasta /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa -O ./gfacs/ all_gene_models.gff

You have specfied a format:	GFACS all version in out.gtf format

You have specified an output directory: ./gfacs/

Format command:	perl /labs/Wegrzyn/gFACs/format_scripts/gFACs_gtf.pl all_gene_models.gff ./gfacs/  


Flags:

--no-processing has NOT been activated. I will look for overlapping gene space and see if it is the result of multiple transcripts mapping to the same location. Splice variants will be labeled as [gene].1, [gene].2, etc... and added to the end of gene_table.txt.
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/overlapping_exons.pl ./gfacs///gene_table.txt ./gfacs/  >> ./gfacs///gFACs_log.txt
Results:
Number of genes with overlapping exons:	3
Number of genes without overlapping exons:	56627
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/splice_variants.pl ./gfacs///gene_table.txt ./gfacs/  >> ./gfacs///gFACs_log.txt
Results: 3 genes has become 3 transcripts.

Format Check!
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/gene_table_fixer.pl ./gfacs/ 

Incompleteness Check! Genes will be labeled as 5' or 3' incompletes.
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/gene_editor.pl ./gfacs/ 

--min-exon-size has been activated. Genes with an exon that is less than 9 will be removed from gene_table.txt
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/minimum_exon.pl ./gfacs///gene_table.txt ./gfacs/ 9   >> ./gfacs///gFACs_log.txt
Results:
Number of genes retained:	56630
Number of genes lost:	0

--min-intron-size has been activated. Genes with an intron that is less than 9 will be removed from gene_table.txt
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/minimum_intron.pl ./gfacs///gene_table.txt ./gfacs/ 9  >> ./gfacs///gFACs_log.txt
Results:
Number of genes retained:	56630
Number of genes lost:	0

--min-CDS-size has been activated. Genes with a CDS that is less than 300 will be removed from gene_table.txt
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/CDS_size.pl ./gfacs///gene_table.txt ./gfacs/ 300  >> ./gfacs///gFACs_log.txt
Results:
Number of genes retained:	55499
Number of genes lost:	1131

You have specified a fasta! Indexing it...
Creating index file from /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa

--allowed-inframe-stop-codons has been activated. Genes with more than 0 inframe stop codons will be removed from gene_table.txt
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/inframe_stop.pl /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa ./gfacs/ 0  >> ./gfacs///gFACs_log.txt
Results:
Number of genes retained:	55280
Number of genes lost:	219

--unique-genes-only has been activated. The longest transcript is chosen if present. Otherwise, the first transcipt is chosen.
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/unique_genes.pl ./gfacs/  >> ./gfacs///gFACs_log.txt
Results:
Number of genes in: 55280
	0 were transcripts.
	122 were non-transcript duplicates.
Number of unique genes retained:	52958
Number of genes lost:	2322

--get-fasta has been activated. Nucleotide fasta will be created called genes.fasta.
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/get_fasta_without_introns.pl /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa ./gfacs/ 

--get-protein-fasta has been activated. Protein fasta will be created called genes.fasta.faa.
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/get_protein_fasta.pl /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa ./gfacs/ 

--create-gtf has been activated. A gtf file called out.gtf will be created.
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/add_start_stop_to_gene_table.pl /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa ./gfacs/ 
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/gtf_creator.pl ./gfacs///start_and_stop_gene_table.txt ./gfacs/ all_gene_models.gff 

--statistics has been activated. Statistics will be printed to statistics.txt
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/classic_stats.pl ./gfacs///gene_table.txt > ./gfacs///statistics.txt

Completed! Have a great day!

