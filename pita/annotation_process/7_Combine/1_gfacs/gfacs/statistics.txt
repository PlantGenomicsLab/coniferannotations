STATS:
Number of genes:	52958
Number of monoexonic genes:	4329
Number of multiexonic genes:	48629

Number of positive strand genes:	26481
Monoexonic:	2280
Multiexonic:	24201

Number of negative strand genes:	26477
Monoexonic:	2049
Multiexonic:	24428

Average overall gene size:	18062.540
Median overall gene size:	2197
Average overall CDS size:	1118.952
Median overall CDS size:	921
Average overall exon size:	278.349
Median overall exon size:	168

Average size of monoexonic genes:	994.432
Median size of monoexonic genes:	762
Largest monoexonic gene:	5598
Smallest monoexonic gene:	300

Average size of multiexonic genes:	19581.959
Median size of multiexonic genes:	2489
Largest multiexonic gene:	1375330
Smallest multiexonic gene:	234

Average size of multiexonic CDS:	1130.036
Median size of multiexonic CDS:	936
Largest multiexonic CDS:	15537
Smallest multiexonic CDS:	300

Average size of multiexonic exons:	263.486
Median size of multiexonic exons:	164
Average size of multiexonic introns:	5610.543
Median size of multiexonic introns:	185

Average number of exons per multiexonic gene:	4.289
Median number of exons per multiexonic gene:	3
Largest multiexonic exon:	7998
Smallest multiexonic exon:	9
Most exons in one gene:	51

Average number of introns per multiexonic gene:	3.289
Median number of introns per multiexonic gene:	2
Largest intron:	758522
Smallest intron:	9

The following columns do not involve codons:
Number of complete models:	52958
Number of 5' only incomplete models:	0
Number of 3' only incomplete models:	0
Number of 5' and 3' incomplete models:	0
