#!/bin/bash
#SBATCH --job-name=gstats_pita
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --mail-type=ALL
#SBATCH --mem=10G
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o pita_stats_%j.o
#SBATCH -e pita_stats_%j.e
#SBATCH --qos=general 

module load perl/5.24.0
perl /labs/Wegrzyn/gFACs/gFACs.pl  \
-f gmap_2017_03_17_gff3 \
-p gmap_maker_pita_me_mi_ifs \
--statistics \
--unique-genes-only \
--min-CDS-size 300 \
--allowed-inframe-stop-codons 0 \
--min-exon-size 9 \
--min-intron-size 9 \
--get-fasta-without-introns \
--get-protein-fasta \
--create-gtf \
--fasta /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa \
-O ./gfacs/ \
../1_gmap/maker_v1.0_pita2.01.gff3
