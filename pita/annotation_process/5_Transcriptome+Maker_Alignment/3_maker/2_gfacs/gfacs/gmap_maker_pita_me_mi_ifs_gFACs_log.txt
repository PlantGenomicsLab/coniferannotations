gFACs LOG

Version: 1.1.1 (11/12/2019)
Time of run: Sat Feb  1 00:29:54 2020

Command:/labs/Wegrzyn/gFACs/gFACs.pl -f gmap_2017_03_17_gff3 -p gmap_maker_pita_me_mi_ifs --statistics --unique-genes-only --min-CDS-size 300 --allowed-inframe-stop-codons 0 --min-exon-size 9 --min-intron-size 9 --get-fasta-without-introns --get-protein-fasta --create-gtf --fasta /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa -O ./gfacs/ ../1_gmap/maker_v1.0_pita2.01.gff3

You have specified a prefix:	gmap_maker_pita_me_mi_ifs

You have specfied a format:	GMAP version 2017/03/17 in gff3 format

You have specified an output directory: ./gfacs/

Format command:	perl /labs/Wegrzyn/gFACs/format_scripts/gmap_2017_03_17_gff3.pl ../1_gmap/maker_v1.0_pita2.01.gff3 ./gfacs/ gmap_maker_pita_me_mi_ifs_ 


Flags:

--no-processing has NOT been activated. I will look for overlapping gene space and see if it is the result of multiple transcripts mapping to the same location. Splice variants will be labeled as [gene].1, [gene].2, etc... and added to the end of gene_table.txt.
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/overlapping_exons.pl ./gfacs///gmap_maker_pita_me_mi_ifs_gene_table.txt ./gfacs/ gmap_maker_pita_me_mi_ifs_ >> ./gfacs///gmap_maker_pita_me_mi_ifs_gFACs_log.txt
Results:
Number of genes with overlapping exons:	0
Number of genes without overlapping exons:	14879
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/splice_variants.pl ./gfacs///gmap_maker_pita_me_mi_ifs_gene_table.txt ./gfacs/ gmap_maker_pita_me_mi_ifs_ >> ./gfacs///gmap_maker_pita_me_mi_ifs_gFACs_log.txt
Results: 0 genes has become 0 transcripts.

Format Check!
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/gene_table_fixer.pl ./gfacs/ gmap_maker_pita_me_mi_ifs_

Incompleteness Check! Genes will be labeled as 5' or 3' incompletes.
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/gene_editor.pl ./gfacs/ gmap_maker_pita_me_mi_ifs_

--min-exon-size has been activated. Genes with an exon that is less than 9 will be removed from gene_table.txt
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/minimum_exon.pl ./gfacs///gmap_maker_pita_me_mi_ifs_gene_table.txt ./gfacs/ 9  gmap_maker_pita_me_mi_ifs_ >> ./gfacs///gmap_maker_pita_me_mi_ifs_gFACs_log.txt
Results:
Number of genes retained:	14847
Number of genes lost:	32

--min-intron-size has been activated. Genes with an intron that is less than 9 will be removed from gene_table.txt
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/minimum_intron.pl ./gfacs///gmap_maker_pita_me_mi_ifs_gene_table.txt ./gfacs/ 9 gmap_maker_pita_me_mi_ifs_ >> ./gfacs///gmap_maker_pita_me_mi_ifs_gFACs_log.txt
Results:
Number of genes retained:	14844
Number of genes lost:	3

--min-CDS-size has been activated. Genes with a CDS that is less than 300 will be removed from gene_table.txt
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/CDS_size.pl ./gfacs///gmap_maker_pita_me_mi_ifs_gene_table.txt ./gfacs/ 300 gmap_maker_pita_me_mi_ifs_ >> ./gfacs///gmap_maker_pita_me_mi_ifs_gFACs_log.txt
Results:
Number of genes retained:	14316
Number of genes lost:	528

You have specified a fasta! Indexing it...
Creating index file from /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa

--allowed-inframe-stop-codons has been activated. Genes with more than 0 inframe stop codons will be removed from gene_table.txt
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/inframe_stop.pl /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa ./gfacs/ 0 gmap_maker_pita_me_mi_ifs_ >> ./gfacs///gmap_maker_pita_me_mi_ifs_gFACs_log.txt
Results:
Number of genes retained:	5660
Number of genes lost:	8656

--unique-genes-only has been activated. The longest transcript is chosen if present. Otherwise, the first transcipt is chosen.
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/unique_genes.pl ./gfacs/ gmap_maker_pita_me_mi_ifs_ >> ./gfacs///gmap_maker_pita_me_mi_ifs_gFACs_log.txt
Results:
Number of genes in: 5660
	0 were transcripts.
	0 were non-transcript duplicates.
Number of unique genes retained:	5646
Number of genes lost:	14

--get-fasta-without-introns has been activated. Nucleotide fasta will be created called genes_without_introns.fasta.
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/get_fasta_without_introns.pl /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa ./gfacs/ gmap_maker_pita_me_mi_ifs_

--get-protein-fasta has been activated. Protein fasta will be created called genes_without_introns.fasta.faa.
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/get_protein_fasta.pl /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa ./gfacs/ gmap_maker_pita_me_mi_ifs_

--create-gtf has been activated. A gtf file called out.gtf will be created.
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/add_start_stop_to_gene_table.pl /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa ./gfacs/ gmap_maker_pita_me_mi_ifs_
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/gtf_creator.pl ./gfacs///gmap_maker_pita_me_mi_ifs_start_and_stop_gene_table.txt ./gfacs/ ../1_gmap/maker_v1.0_pita2.01.gff3 gmap_maker_pita_me_mi_ifs_

--statistics has been activated. Statistics will be printed to statistics.txt
	Command: perl /labs/Wegrzyn/gFACs/task_scripts/classic_stats.pl ./gfacs///gmap_maker_pita_me_mi_ifs_gene_table.txt > ./gfacs///gmap_maker_pita_me_mi_ifs_statistics.txt

Completed! Have a great day!

