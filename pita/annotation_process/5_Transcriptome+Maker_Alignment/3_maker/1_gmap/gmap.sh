#!/bin/bash
#SBATCH --job-name=gmap
#SBATCH -n 8
#SBATCH --mem=20G
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o gmap.out
#SBATCH -e gmap.err
#SBTACH --qos=general
# Run the program 

module load gmap/2017-03-17

gmapl -K 1000000 -L 10000000 -a 1 --cross-species -D /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/index/5000bps/gmap/pita2.01_5000bps_softmasked/ -d pita2.01_5000bps_softmasked -f gff3_gene pita.transcripts.filtered.high.met_starts.v1.0.fasta --fulllength --nthreads=8 --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 > maker_v1.0_pita2.01.gff3



