#!/bin/bash
#SBATCH --job-name=gmap
#SBATCH -n 12
#SBATCH --mem=50G
#SBATCH -N 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o gmap_%j.out
#SBATCH -e gmap_%j.err
#SBTACH --qos=general
# Run the program 

module load gmap/2017-03-17
#for CONIFERS ONLY
gmapl -K 1000000 -L 10000000 -a 1 --cross-species -D /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/index/5000bps/gmap/pita2.01_5000bps_softmasked/ -d pita2.01_5000bps_softmasked -f gff3_gene ../../../../Genes/illumina/Pita.IU.TranscriptomeMainsV1_450bp.fasta --fulllength --nthreads=12 --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 -T > illumina_genes_pita2.01._95_95.gff3 2> query95_1_updated_95_95.error

gmapl -K 1000000 -L 10000000 -a 1 --cross-species -D /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/index/5000bps/gmap/pita2.01_5000bps_softmasked/ -d pita2.01_5000bps_softmasked -f gff3_gene ../../../../Genes/illumina/Pita.IU.TranscriptomeMainsV1_450bp.fasta --fulllength --nthreads=12 --min-trimmed-coverage=0.95 --min-identity=0.92 -n1 -T > illumina_genes_pita2.01._95_92.gff3 2> query95_1_updated_95_92.error





