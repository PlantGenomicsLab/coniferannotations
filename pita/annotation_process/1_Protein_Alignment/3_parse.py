import argparse
import os
import linecache
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
import re

parser = argparse.ArgumentParser(
     prog='parse.py',
     usage='''python parse.py --gff [gtf] --pathGFF [path of the gtf file]''',
     description='''parse the GFF output from genomeThreader''',
     epilog='''It requires numpy and biopython libraries''')
parser.add_argument('--gff', type=str, help='The name of gff file', required=True)
parser.add_argument('--pathGFF', type=str, help='The path of the gff file', required=False)
args = parser.parse_args()
pathGFF=args.pathGFF
gff=args.gff

if pathGFF==None:
    gfffile = open(gff,"r")     
else:
    gfffile=open(os.path.join(pathGFF, gff), "r")

gffline = gfffile.read()
#print gffline
blocks = gffline.split("###") 
keep=[]
for b in blocks:
    if "three_prime_cis_splice_site" in b:
	keep.append(b)

for k in keep:
    print k
    print "###"	
