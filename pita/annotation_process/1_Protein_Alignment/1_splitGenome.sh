#!/bin/bash
# Submission script for Xanadu 
#SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --job-name=pieces
#SBATCH -o piece-%j.output
#SBATCH -e piece-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=40G

module load anaconda

python /UCHC/LABS/Wegrzyn/ConiferGenomes/GenomeAnnotations/splitfasta.py --fasta pita.2_01.5000bps.softmasked.fa --path /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/softmasking/ --pieces 500 --pathOut ./split

