#!/bin/bash
# Submission script for Xanadu
#SBATCH --mem=10GB
#SBATCH --job-name=gth
#SBATCH -o gth-%j.output
#SBATCH -e gth-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --partition=general
#SBATCH --array=1-499%30
#SBATCH --qos=general

#module load genomethreader/1.6.6
#echo $SLURM_ARRAY_TASK_ID
## blat format: blat database query [-ooc=11.ooc] output.psl

module load genomethreader/1.7.1 
#module load genomethreader/1.6.6 
echo genome_scaffolds_blathits.fasta"$SLURM_ARRAY_TASK_ID".fa
gth -genomic splitpita.2_01.5000bps.softmasked.fa"$SLURM_ARRAY_TASK_ID".fa -protein ../complete_partial_illumina_pacbop_maker_prots_clean.fasta -gff3out -startcodon -finalstopcodon -introncutout -dpminexonlen 20 -dpminintronlen 9 -skipalignmentout -o pita.2_01.5000bps.softmasked.fa"$SLURM_ARRAY_TASK_ID".gth.gff3 -force -gcmaxgapwidth 1000000 -gcmincoverage 80

#gth -genomic splitpita.2_01.5000bps.softmasked.fa"$SLURM_ARRAY_TASK_ID".fa -protein ../complete_partial_illumina_pacbop_maker_prots_clean_stops.fasta -gff3out -startcodon -finalstopcodon -introncutout -dpminexonlen 20 -dpminintronlen 9 -skipalignmentout -o pita.2_01.5000bps.softmasked.fa"$SLURM_ARRAY_TASK_ID".gth.gff3 -force -gcmaxgapwidth 1000000 -gcmincoverage 80


rm splitpita.2_01.5000bps.softmasked.fa"$SLURM_ARRAY_TASK_ID".fa.dna.*
