#!/bin/bash
# Submission script for Xanadu 
#SBATCH --job-name=brakerPita
#SBATCH -o brakerPita-%j.output
#SBATCH -e brakerPita-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=100G

module load BRAKER/2.0.5

module unload augustus
module unload bamtools/2.5.1
module unload GeneMark-ET/4.38
module rm  perl/5.28.0-clean
module unload perl/5.28.1

module load bamtools/2.4.1
export BAMTOOLS_PATH=/isg/shared/apps/bamtools/2.4.1/bin/
export GENEMARK_PATH=/labs/Wegrzyn/local_software/gm_et_linux_64/gmes_petap/
export AUGUSTUS_CONFIG_PATH=$HOME/augustus-3.2.3/config

export PATH=/home/CAM/szaman/augustus-3.2.3/bin:/home/CAM/szaman/augustus-3.2.3/scripts:$PATH 

module load perl/5.24.0
export PERL5LIB=/labs/Wegrzyn/perl5/lib/perl5/
export PERLINC=/labs/Wegrzyn/perl5/lib/perl5/

cp ~/local_gm_key_64 ~/.gm_key

~/2.0.5/braker.pl --cores 16 --genome=/isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa --species=Pita2.01_Braker2.05_RNAseqProteins_3 --hints=hintsfile.gff --prot_aln=all.pita.multiexonic.2_01.5000bps.softmasked.gff3 --GENEMARK_PATH=/labs/Wegrzyn/local_software/gm_et_linux_64/gmes_petap/ --BAMTOOLS_PATH=/isg/shared/apps/bamtools/2.4.1/bin -prg=gth --gth2traingenes --softmasking 1 --gff3
