#       Annotation Pipeline: 

Annotation Process for Loblolly Pine.  
$PWD = /core/labs/Wegrzyn/ConiferGenomes/Pita/analysis/

## Data Location:
&nbsp;&nbsp;&nbsp;1. Pacbio --> $PWD/Genes/Pacbio/pacbio_pre_frame_selection.fasta   
&nbsp;&nbsp;&nbsp;2. Maker --> $PWD/Genes/Maker/maker_transcripts.fasta  
&nbsp;&nbsp;&nbsp;3. Illumina --> $PWD/Genes/illumina/Pita.IU.TranscriptomeMainsV1_450bp.fasta  

### 1. Protein Alignment 
&nbsp;&nbsp;&nbsp;1. Concatenate the protein fasta files from Pacbio, Illumina, Maker (no filters)
&nbsp;&nbsp;&nbsp;2. Clean up the fasta file by removing the '.' character 
&nbsp;&nbsp;&nbsp;3. Run GenomeThreader 

### 2. RNA-seq Alignment
&nbsp;&nbsp;&nbsp;1. Download the raw reads from NCBI
&nbsp;&nbsp;&nbsp;2. Trim the raw reads using sickle. 
&nbsp;&nbsp;&nbsp;3. Align the trimmed reads to the genome using HISAT

### 3. Running Braker

### 4. Filtering Braker Gene Models to Obtain Multiexonic Ab initio Gene Models

&nbsp;&nbsp;&nbsp;1. Filter with gFACs, filters include: min-CDS-size 100, rem-monoexonics, rem-all-incompletes, unique-genes-only, splice-rescue, min-exon-size 20, min-intron-size 11  
&nbsp;&nbsp;&nbsp;2. Run EnTAP  
&nbsp;&nbsp;&nbsp;3. Remove genes that are not fucntionally annotated with gFACs  
&nbsp;&nbsp;&nbsp;4. Run Interpro and removes genes that annotate as reterodomains  
&nbsp;&nbsp;&nbsp;5. Remove genes that are more than 70% soft-masked  

### 5. Overlap the Braker Gene Models with Pacbio, Illumina, and Maker Gene Models
&nbsp;&nbsp;&nbsp;1. Intersect each source (braker, pacbio, illumina, and maker) with all the other sources (using bedtools) to remove nested gene models.  
&nbsp;&nbsp;&nbsp;2. After removing nested gene models from each source, remove partially over-lapping genes models by choosing the longer gene model.

Hisat2 Alignment:  
$PWD/Hisat2/hisat.sh 

Protein Alignment using GenomeThreader:  

Concatenate protein sequences from maker gene models, illumina, and pacbio data. Retain complete and partial gene models and align them to the genome via genomethreader.  

Braker Run:  
More details on how to execute braker is located in the scripts folder.  

Post-processing braker: 
The resulting genes from braker were processed by gFACS for complete, multiexonic genes with exons at least 20 bps long  
and introns at least 9 bps long. The resulting genes were also processed by EnTAP for having functional protein domains.  
The remaining genes were processed by InterPro scan and removed if they had a similarity hit to reterotransposon elements.  

Overlap:  
To resolve gene fragmentation, we look for nested or overlapping gene models between various methods i.e ab initio (maker and braker) and de novo assembled transcriptome (Pacbio & Illumina)  

Further Filtering:  
After removing over-lapping and nested gene models, all genes models less than 450 bps (150 aa) were removed.  
Duplicate gene models (i.e. genes on same scaffold with same start and stop site) were also removed.  

####    BUSCO:
Assessing completeness of the annotation (using busco):
/labs/Wegrzyn/ConiferGenomes/Pita/analysis/Genes/pita2.01_annotation2.0/busco/run_Busco_PITA2.01_annot2.0  
``` C:21.6%[S:17.4%,D:4.2%],F:8.3%,M:70.1%,n:1440

        311     Complete BUSCOs (C)
        250     Complete and single-copy BUSCOs (S)
        61      Complete and duplicated BUSCOs (D)
        120     Fragmented BUSCOs (F)
        1009    Missing BUSCOs (M)
        1440    Total BUSCO groups searched```
