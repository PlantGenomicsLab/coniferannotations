Library: SRR1200298
```19794272 reads; of these:
  19794272 (100.00%) were paired; of these:
    1269592 (6.41%) aligned concordantly 0 times
    13914101 (70.29%) aligned concordantly exactly 1 time
    4610579 (23.29%) aligned concordantly >1 times
    ----
    1269592 pairs aligned concordantly 0 times; of these:
      69115 (5.44%) aligned discordantly 1 time
    ----
    1200477 pairs aligned 0 times concordantly or discordantly; of these:
      2400954 mates make up the pairs; of these:
        1823666 (75.96%) aligned 0 times
        352192 (14.67%) aligned exactly 1 time
        225096 (9.38%) aligned >1 times
95.39% overall alignment rate 
```

Library: SRR1200343  
```29344793 reads; of these:
  29344793 (100.00%) were paired; of these:
    3552052 (12.10%) aligned concordantly 0 times
    17726701 (60.41%) aligned concordantly exactly 1 time
    8066040 (27.49%) aligned concordantly >1 times
    ----
    3552052 pairs aligned concordantly 0 times; of these:
      223482 (6.29%) aligned discordantly 1 time
    ----
    3328570 pairs aligned 0 times concordantly or discordantly; of these:
      6657140 mates make up the pairs; of these:
        5011195 (75.28%) aligned 0 times
        964288 (14.49%) aligned exactly 1 time
        681657 (10.24%) aligned >1 times
91.46% overall alignment rate 
```

Library: SRR1200365 
```39655127 reads; of these:
  39655127 (100.00%) were paired; of these:
    4430772 (11.17%) aligned concordantly 0 times
    24621397 (62.09%) aligned concordantly exactly 1 time
    10602958 (26.74%) aligned concordantly >1 times
    ----
    4430772 pairs aligned concordantly 0 times; of these:
      281945 (6.36%) aligned discordantly 1 time
    ----
    4148827 pairs aligned 0 times concordantly or discordantly; of these:
      8297654 mates make up the pairs; of these:
        6194632 (74.66%) aligned 0 times
        1233681 (14.87%) aligned exactly 1 time
        869341 (10.48%) aligned >1 times
92.19% overall alignment rate
```

Library: SRR1200366  
```17578810 reads; of these:
  17578810 (100.00%) were paired; of these:
    1425192 (8.11%) aligned concordantly 0 times
    11791257 (67.08%) aligned concordantly exactly 1 time
    4362361 (24.82%) aligned concordantly >1 times
    ----
    1425192 pairs aligned concordantly 0 times; of these:
      60343 (4.23%) aligned discordantly 1 time
    ----
    1364849 pairs aligned 0 times concordantly or discordantly; of these:
      2729698 mates make up the pairs; of these:
        2094059 (76.71%) aligned 0 times
        378657 (13.87%) aligned exactly 1 time
        256982 (9.41%) aligned >1 times
94.04% overall alignment rate
``` 

Library: SRR1200403
```10846952 reads; of these:
  10846952 (100.00%) were paired; of these:
    1409110 (12.99%) aligned concordantly 0 times
    6642227 (61.24%) aligned concordantly exactly 1 time
    2795615 (25.77%) aligned concordantly >1 times
    ----
    1409110 pairs aligned concordantly 0 times; of these:
      70411 (5.00%) aligned discordantly 1 time
    ----
    1338699 pairs aligned 0 times concordantly or discordantly; of these:
      2677398 mates make up the pairs; of these:
        2021453 (75.50%) aligned 0 times
        350749 (13.10%) aligned exactly 1 time
        305196 (11.40%) aligned >1 times
90.68% overall alignment rate
```

Library: SRR1200404     
```18762947 reads; of these:
  18762947 (100.00%) were paired; of these:
    2229011 (11.88%) aligned concordantly 0 times
    11706980 (62.39%) aligned concordantly exactly 1 time
    4826956 (25.73%) aligned concordantly >1 times
    ----
    2229011 pairs aligned concordantly 0 times; of these:
      118866 (5.33%) aligned discordantly 1 time
    ----
    2110145 pairs aligned 0 times concordantly or discordantly; of these:
      4220290 mates make up the pairs; of these:
        3187491 (75.53%) aligned 0 times
        555599 (13.16%) aligned exactly 1 time
        477200 (11.31%) aligned >1 times
91.51% overall alignment rate
```

Library: SRR1200409 
```36296017 reads; of these:
  36296017 (100.00%) were paired; of these:
    4185912 (11.53%) aligned concordantly 0 times
    22619135 (62.32%) aligned concordantly exactly 1 time
    9490970 (26.15%) aligned concordantly >1 times
    ----
    4185912 pairs aligned concordantly 0 times; of these:
      238244 (5.69%) aligned discordantly 1 time
    ----
    3947668 pairs aligned 0 times concordantly or discordantly; of these:
      7895336 mates make up the pairs; of these:
        5938155 (75.21%) aligned 0 times
        1067417 (13.52%) aligned exactly 1 time
        889764 (11.27%) aligned >1 times
91.82% overall alignment rate
```

Library: SRR1200410
```9200199 reads; of these:
  9200199 (100.00%) were paired; of these:
    1069071 (11.62%) aligned concordantly 0 times
    5718503 (62.16%) aligned concordantly exactly 1 time
    2412625 (26.22%) aligned concordantly >1 times
    ----
    1069071 pairs aligned concordantly 0 times; of these:
      38330 (3.59%) aligned discordantly 1 time
    ----
    1030741 pairs aligned 0 times concordantly or discordantly; of these:
      2061482 mates make up the pairs; of these:
        1660406 (80.54%) aligned 0 times
        226815 (11.00%) aligned exactly 1 time
        174261 (8.45%) aligned >1 times
90.98% overall alignment rate
```   

Library: SRR1200412

```9527313 reads; of these:
  9527313 (100.00%) were paired; of these:
    1042549 (10.94%) aligned concordantly 0 times
    6008471 (63.07%) aligned concordantly exactly 1 time
    2476293 (25.99%) aligned concordantly >1 times
    ----
    1042549 pairs aligned concordantly 0 times; of these:
      38502 (3.69%) aligned discordantly 1 time
    ----
    1004047 pairs aligned 0 times concordantly or discordantly; of these:
      2008094 mates make up the pairs; of these:
        1595023 (79.43%) aligned 0 times
        234070 (11.66%) aligned exactly 1 time
        179001 (8.91%) aligned >1 times
91.63% overall alignment rate
```   

Library: SRR1200413
```23685394 reads; of these:
  23685394 (100.00%) were paired; of these:
    2250979 (9.50%) aligned concordantly 0 times
    14782246 (62.41%) aligned concordantly exactly 1 time
    6652169 (28.09%) aligned concordantly >1 times
    ----
    2250979 pairs aligned concordantly 0 times; of these:
      92995 (4.13%) aligned discordantly 1 time
    ----
    2157984 pairs aligned 0 times concordantly or discordantly; of these:
      4315968 mates make up the pairs; of these:
        3322314 (76.98%) aligned 0 times
        558671 (12.94%) aligned exactly 1 time
        434983 (10.08%) aligned >1 times
92.99% overall alignment rate
```   

Library: SRR1200414
```36681033 reads; of these:
  36681033 (100.00%) were paired; of these:
    3871755 (10.56%) aligned concordantly 0 times
    23040210 (62.81%) aligned concordantly exactly 1 time
    9769068 (26.63%) aligned concordantly >1 times
    ----
    3871755 pairs aligned concordantly 0 times; of these:
      135562 (3.50%) aligned discordantly 1 time
    ----
    3736193 pairs aligned 0 times concordantly or discordantly; of these:
      7472386 mates make up the pairs; of these:
        5915609 (79.17%) aligned 0 times
        883048 (11.82%) aligned exactly 1 time
        673729 (9.02%) aligned >1 times
91.94% overall alignment rate
```

Library: SRR1200420
```23880785 reads; of these:
  23880785 (100.00%) were paired; of these:
    3019705 (12.64%) aligned concordantly 0 times
    14684649 (61.49%) aligned concordantly exactly 1 time
    6176431 (25.86%) aligned concordantly >1 times
    ----
    3019705 pairs aligned concordantly 0 times; of these:
      404725 (13.40%) aligned discordantly 1 time
    ----
    2614980 pairs aligned 0 times concordantly or discordantly; of these:
      5229960 mates make up the pairs; of these:
        4101443 (78.42%) aligned 0 times
        557197 (10.65%) aligned exactly 1 time
        571320 (10.92%) aligned >1 times
91.41% overall alignment rate
```
Library: SRR1200421
```24170848 reads; of these:
  24170848 (100.00%) were paired; of these:
    3104585 (12.84%) aligned concordantly 0 times
    14838197 (61.39%) aligned concordantly exactly 1 time
    6228066 (25.77%) aligned concordantly >1 times
    ----
    3104585 pairs aligned concordantly 0 times; of these:
      380091 (12.24%) aligned discordantly 1 time
    ----
    2724494 pairs aligned 0 times concordantly or discordantly; of these:
      5448988 mates make up the pairs; of these:
        4299962 (78.91%) aligned 0 times
        575925 (10.57%) aligned exactly 1 time
        573101 (10.52%) aligned >1 times
91.11% overall alignment rate
```
Library: SRR1200422
```11334506 reads; of these:
  11334506 (100.00%) were paired; of these:
    1319274 (11.64%) aligned concordantly 0 times
    7166519 (63.23%) aligned concordantly exactly 1 time
    2848713 (25.13%) aligned concordantly >1 times
    ----
    1319274 pairs aligned concordantly 0 times; of these:
      201275 (15.26%) aligned discordantly 1 time
    ----
    1117999 pairs aligned 0 times concordantly or discordantly; of these:
      2235998 mates make up the pairs; of these:
        1720773 (76.96%) aligned 0 times
        260828 (11.66%) aligned exactly 1 time
        254397 (11.38%) aligned >1 times
92.41% overall alignment rate
```
Library: SRR1200423
```30775379 reads; of these:
  30775379 (100.00%) were paired; of these:
    3749472 (12.18%) aligned concordantly 0 times
    19253352 (62.56%) aligned concordantly exactly 1 time
    7772555 (25.26%) aligned concordantly >1 times
    ----
    3749472 pairs aligned concordantly 0 times; of these:
      471017 (12.56%) aligned discordantly 1 time
    ----
    3278455 pairs aligned 0 times concordantly or discordantly; of these:
      6556910 mates make up the pairs; of these:
        5206905 (79.41%) aligned 0 times
        696419 (10.62%) aligned exactly 1 time
        653586 (9.97%) aligned >1 times
91.54% overall alignment rate
```
Library: SRR1200424
```16035037 reads; of these:
  16035037 (100.00%) were paired; of these:
    2086686 (13.01%) aligned concordantly 0 times
    9899696 (61.74%) aligned concordantly exactly 1 time
    4048655 (25.25%) aligned concordantly >1 times
    ----
    2086686 pairs aligned concordantly 0 times; of these:
      329373 (15.78%) aligned discordantly 1 time
    ----
    1757313 pairs aligned 0 times concordantly or discordantly; of these:
      3514626 mates make up the pairs; of these:
        2749118 (78.22%) aligned 0 times
        372139 (10.59%) aligned exactly 1 time
        393369 (11.19%) aligned >1 times
91.43% overall alignment rate
```

Library: SRR1200425
```18329229 reads; of these:
  18329229 (100.00%) were paired; of these:
    3295144 (17.98%) aligned concordantly 0 times
    9674251 (52.78%) aligned concordantly exactly 1 time
    5359834 (29.24%) aligned concordantly >1 times
    ----
    3295144 pairs aligned concordantly 0 times; of these:
      246388 (7.48%) aligned discordantly 1 time
    ----
    3048756 pairs aligned 0 times concordantly or discordantly; of these:
      6097512 mates make up the pairs; of these:
        5264419 (86.34%) aligned 0 times
        383064 (6.28%) aligned exactly 1 time
        450029 (7.38%) aligned >1 times
85.64% overall alignment rate
```

Library: SRR1200426
```29046280 reads; of these:
  29046280 (100.00%) were paired; of these:
    3210012 (11.05%) aligned concordantly 0 times
    18244471 (62.81%) aligned concordantly exactly 1 time
    7591797 (26.14%) aligned concordantly >1 times
    ----
    3210012 pairs aligned concordantly 0 times; of these:
      247913 (7.72%) aligned discordantly 1 time
    ----
    2962099 pairs aligned 0 times concordantly or discordantly; of these:
      5924198 mates make up the pairs; of these:
        4419381 (74.60%) aligned 0 times
        891137 (15.04%) aligned exactly 1 time
        613680 (10.36%) aligned >1 times
92.39% overall alignment rate
```

Library: SRR1200427
```15737655 reads; of these:
  15737655 (100.00%) were paired; of these:
    1755236 (11.15%) aligned concordantly 0 times
    10054124 (63.89%) aligned concordantly exactly 1 time
    3928295 (24.96%) aligned concordantly >1 times
    ----
    1755236 pairs aligned concordantly 0 times; of these:
      133451 (7.60%) aligned discordantly 1 time
    ----
    1621785 pairs aligned 0 times concordantly or discordantly; of these:
      3243570 mates make up the pairs; of these:
        2423749 (74.72%) aligned 0 times
        476590 (14.69%) aligned exactly 1 time
        343231 (10.58%) aligned >1 times
92.30% overall alignment rate
```

Library: SRR1200428
```15578040 reads; of these:
  15578040 (100.00%) were paired; of these:
    1574065 (10.10%) aligned concordantly 0 times
    10070592 (64.65%) aligned concordantly exactly 1 time
    3933383 (25.25%) aligned concordantly >1 times
    ----
    1574065 pairs aligned concordantly 0 times; of these:
      136008 (8.64%) aligned discordantly 1 time
    ----
    1438057 pairs aligned 0 times concordantly or discordantly; of these:
      2876114 mates make up the pairs; of these:
        2113219 (73.47%) aligned 0 times
        447203 (15.55%) aligned exactly 1 time
        315692 (10.98%) aligned >1 times
93.22% overall alignment rate
```

Library: SRR1200429
```29796367 reads; of these:
  29796367 (100.00%) were paired; of these:
    3615282 (12.13%) aligned concordantly 0 times
    18444695 (61.90%) aligned concordantly exactly 1 time
    7736390 (25.96%) aligned concordantly >1 times
    ----
    3615282 pairs aligned concordantly 0 times; of these:
      286609 (7.93%) aligned discordantly 1 time
    ----
    3328673 pairs aligned 0 times concordantly or discordantly; of these:
      6657346 mates make up the pairs; of these:
        5078659 (76.29%) aligned 0 times
        927888 (13.94%) aligned exactly 1 time
        650799 (9.78%) aligned >1 times
91.48% overall alignment rate
```

Library: SRR1200430
```33321691 reads; of these:
  33321691 (100.00%) were paired; of these:
    4211567 (12.64%) aligned concordantly 0 times
    20533835 (61.62%) aligned concordantly exactly 1 time
    8576289 (25.74%) aligned concordantly >1 times
    ----
    4211567 pairs aligned concordantly 0 times; of these:
      390197 (9.26%) aligned discordantly 1 time
    ----
    3821370 pairs aligned 0 times concordantly or discordantly; of these:
      7642740 mates make up the pairs; of these:
        5881683 (76.96%) aligned 0 times
        1007208 (13.18%) aligned exactly 1 time
        753849 (9.86%) aligned >1 times
91.17% overall alignment rate
```

Library: SRR1200431
```11133511 reads; of these:
  11133511 (100.00%) were paired; of these:
    1303656 (11.71%) aligned concordantly 0 times
    6908336 (62.05%) aligned concordantly exactly 1 time
    2921519 (26.24%) aligned concordantly >1 times
    ----
    1303656 pairs aligned concordantly 0 times; of these:
      102582 (7.87%) aligned discordantly 1 time
    ----
    1201074 pairs aligned 0 times concordantly or discordantly; of these:
      2402148 mates make up the pairs; of these:
        1804316 (75.11%) aligned 0 times
        348574 (14.51%) aligned exactly 1 time
        249258 (10.38%) aligned >1 times
91.90% overall alignment rate
```

Library: SRR1200432
```25241080 reads; of these:
  25241080 (100.00%) were paired; of these:
    2678011 (10.61%) aligned concordantly 0 times
    15891942 (62.96%) aligned concordantly exactly 1 time
    6671127 (26.43%) aligned concordantly >1 times
    ----
    2678011 pairs aligned concordantly 0 times; of these:
      289028 (10.79%) aligned discordantly 1 time
    ----
    2388983 pairs aligned 0 times concordantly or discordantly; of these:
      4777966 mates make up the pairs; of these:
        3612796 (75.61%) aligned 0 times
        648948 (13.58%) aligned exactly 1 time
        516222 (10.80%) aligned >1 times
92.84% overall alignment rate
```

Library: SRR1200433
```34625980 reads; of these:
  34625980 (100.00%) were paired; of these:
    3976477 (11.48%) aligned concordantly 0 times
    21304209 (61.53%) aligned concordantly exactly 1 time
    9345294 (26.99%) aligned concordantly >1 times
    ----
    3976477 pairs aligned concordantly 0 times; of these:
      283891 (7.14%) aligned discordantly 1 time
    ----
    3692586 pairs aligned 0 times concordantly or discordantly; of these:
      7385172 mates make up the pairs; of these:
        5686301 (77.00%) aligned 0 times
        960202 (13.00%) aligned exactly 1 time
        738669 (10.00%) aligned >1 times
91.79% overall alignment rate
```

Library: SRR1200435
```35364726 reads; of these:
  35364726 (100.00%) were paired; of these:
    4430462 (12.53%) aligned concordantly 0 times
    21774628 (61.57%) aligned concordantly exactly 1 time
    9159636 (25.90%) aligned concordantly >1 times
    ----
    4430462 pairs aligned concordantly 0 times; of these:
      406672 (9.18%) aligned discordantly 1 time
    ----
    4023790 pairs aligned 0 times concordantly or discordantly; of these:
      8047580 mates make up the pairs; of these:
        6212392 (77.20%) aligned 0 times
        1032849 (12.83%) aligned exactly 1 time
        802339 (9.97%) aligned >1 times
91.22% overall alignment rate
```

Library: SRR1200436
```20484153 reads; of these:
  20484153 (100.00%) were paired; of these:
    2352967 (11.49%) aligned concordantly 0 times
    12832895 (62.65%) aligned concordantly exactly 1 time
    5298291 (25.87%) aligned concordantly >1 times
    ----
    2352967 pairs aligned concordantly 0 times; of these:
      258218 (10.97%) aligned discordantly 1 time
    ----
    2094749 pairs aligned 0 times concordantly or discordantly; of these:
      4189498 mates make up the pairs; of these:
        3138225 (74.91%) aligned 0 times
        605156 (14.44%) aligned exactly 1 time
        446117 (10.65%) aligned >1 times
92.34% overall alignment rate
```
