# BUSCO version is: 4.0.2 
# The lineage dataset is: embryophyta_odb10 (Creation date: 2019-11-20, number of species: 50, number of BUSCOs: 1614)
# Summarized benchmarking in BUSCO notation for file /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/transcriptome_comparison/3_gmap_genomeV1.01/pacbio/gfacs/gmap_pacbio_pita_me_mi_ifs_genes.fasta.faa
# BUSCO was run in mode: proteins

	***** Results: *****

	C:24.1%[S:17.4%,D:6.7%],F:7.1%,M:68.8%,n:1614	   
	389	Complete BUSCOs (C)			   
	281	Complete and single-copy BUSCOs (S)	   
	108	Complete and duplicated BUSCOs (D)	   
	114	Fragmented BUSCOs (F)			   
	1111	Missing BUSCOs (M)			   
	1614	Total BUSCO groups searched		   
