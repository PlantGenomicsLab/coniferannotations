# BUSCO version is: 4.0.2 
# The lineage dataset is: embryophyta_odb10 (Creation date: 2019-11-20, number of species: 50, number of BUSCOs: 1614)
# Summarized benchmarking in BUSCO notation for file /labs/Wegrzyn/ConiferGenomes/coniferannotations/pita/transcriptome_comparison/3_gmap_genomeV1.01/illumina/gfacs/gmap_illumina_pita_me_mi_ifs_genes.fasta.faa
# BUSCO was run in mode: proteins

	***** Results: *****

	C:32.0%[S:29.1%,D:2.9%],F:2.0%,M:66.0%,n:1614	   
	517	Complete BUSCOs (C)			   
	470	Complete and single-copy BUSCOs (S)	   
	47	Complete and duplicated BUSCOs (D)	   
	33	Fragmented BUSCOs (F)			   
	1064	Missing BUSCOs (M)			   
	1614	Total BUSCO groups searched		   
