#!/bin/bash
#SBATCH --job-name=busco
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err

module load busco/4.0.2
module unload augustus
export PATH=/home/FCAM/szaman/augustus-3.2.3/bin:/home/FCAM/szaman/augustus-3.2.3/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus-3.2.3/config

busco -i Pita.IU.TranscriptomeMainsV1_450bp.fasta -f -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o buscoV4.0_illumina_embroy -m tran -c 8

busco -i Pita.IU.TranscriptomeMainsV1_450bp.fasta -f -l /isg/shared/databases/BUSCO/odb10/viridiplantae_odb10 -o buscoV4.0_illumina_virid -m tran -c 8
