# BUSCO version is: 4.0.2 
# The lineage dataset is: viridiplantae_odb10 (Creation date: 2019-11-20, number of species: 57, number of BUSCOs: 425)
# Summarized benchmarking in BUSCO notation for file Merged_PacBio.Cluster.450bps.fasta
# BUSCO was run in mode: transcriptome

	***** Results: *****

	C:49.4%[S:29.6%,D:19.8%],F:13.2%,M:37.4%,n:425	   
	210	Complete BUSCOs (C)			   
	126	Complete and single-copy BUSCOs (S)	   
	84	Complete and duplicated BUSCOs (D)	   
	56	Fragmented BUSCOs (F)			   
	159	Missing BUSCOs (M)			   
	425	Total BUSCO groups searched		   
