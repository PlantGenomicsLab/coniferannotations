#!/bin/bash
#SBATCH --job-name=busco
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err

module load busco/4.0.2
module unload augustus
export PATH=/home/CAM/szaman/augustus-3.2.3/bin:/home/CAM/szaman/augustus-3.2.3/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus-3.2.3/config

busco -i /labs/Wegrzyn/ConiferGenomes/Pita/analysis/Genes/illumina/Pita.IU.TranscriptomeMainsV1_450bp.fasta -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o buscoV4.0_illumina_trans -m tran -c 8
