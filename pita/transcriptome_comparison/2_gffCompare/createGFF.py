import linecache
import argparse
import os

parser = argparse.ArgumentParser(
     prog='createGFF.py',
     usage='''python createGFF.py --gff [gff file given by braker] --path [Path of gff file] --nameList [name of list file] --pathList [path of the list file] --out [name of output fasta file]''',
     description='''This program pulls out the gff of specific genes, given the gff file and a list of sequences saved in a text file''',
     epilog='''It requires numpy and biopython libraries''')
parser.add_argument('--gff', type=str, help='The name of the gff file', required=True)
parser.add_argument('--path', type=str, help='The path of the gff file', required=False)
parser.add_argument('--nameList', type=str, help='name of the list file that contains name of sequences', required=False)
parser.add_argument('--pathList', type=str, help='path of list file', required=False)
parser.add_argument('--out', type=str, help='name of output fasta file', required=True)

args=parser.parse_args()
path=args.path
gff=args.gff
listName=args.nameList
listPath=args.pathList
output=args.out

if path==None:
    gffFile=gff
else:
    gffFile=os.path.join(path, gff)

if listPath==None:
    listFile=listName
else:
    listFile=os.path.join(listPath, listName)

with open(output,'w') as seqs:
    for row in open(gffFile,'r'):
        if "\tgene\t" in row:
            cols=row.split("\t")
            id=""
            if "ID=" not in cols[8]:
                id="ID="+cols[8]
            else:
                id=cols[8]
            gene=cols[0]+"\t"+cols[1]+"\t"+cols[2]+"\t"+cols[3]+"\t"+cols[4]+"\t"+cols[5]+"\t"+cols[6]+"\t"+cols[7]+"\t"+id
            seqs.write("%s" %(gene))
        else:
            seqs.write("%s" %(row))

        
