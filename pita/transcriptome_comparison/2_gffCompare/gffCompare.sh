#!/bin/bash
#SBATCH --job-name=gffCompare
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=30G
#SBATCH -o gffCompare_%j.out
#SBATCH -e gffCompare_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load gffcompare/0.10.4

# python createGFF.py --gff gmap_pacbio_pita_me_mi_ifs_out.gtf --out pacbio.gtf
# python createGFF.py --gff gmap_illumina_pita_me_mi_ifs_out.gtf --out illumina.gtf
# mkdir ref_pacbio
# gffcompare -r pacbio.gtf illumina.gtf -s /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa -T
# mv gffcmp* ref_pacbio

# mkdir ref_illumina
# gffcompare -r illumina.gtf pacbio.gtf -s /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa -T
# mv gffcmp* ref_illumina


mkdir brakerRef_illumina
gffcompare -r entap_gfacs_interpro_removeRetero_remMasked_mod.gtf illumina.gtf -s /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa -T  
mv gffcmp* brakerRef_illumina
mkdir brakerRef_pacbio
gffcompare -r entap_gfacs_interpro_removeRetero_remMasked_mod.gtf pacbio.gtf -s /isg/shared/databases/alignerIndex/plant/Pita/Pita_2.01/genome/Pita.2_01.5000bps.softmasked.fa -T
mv gffcmp* brakerRef_pacbio

