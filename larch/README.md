# Larix sibirica ??? --> $PWD=
## Data Location:
### Genome(s): 
#### Original Genome:
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/siberian_larch/ncbi-genomes-2019-05-30/q

### Quast
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/siberian_larch/ncbi-genomes-2019-05-30/quast_out/report.txt

        Assembly                    GCA_004151065.1_LarixSibirica0.1_genomic
        # contigs (>= 0 bp)         11325800                                
        # contigs (>= 1000 bp)      1495392                                 
        # contigs (>= 5000 bp)      443578                                  
        # contigs (>= 10000 bp)     202256                                  
        # contigs (>= 25000 bp)     57847                                   
        # contigs (>= 50000 bp)     20031                                   
        Total length (>= 0 bp)      12342093815                             
        Total length (>= 1000 bp)   8900886205                              
        Total length (>= 5000 bp)   6702015431                              
        Total length (>= 10000 bp)  4929179903                              
        Total length (>= 25000 bp)  2830676945                              
        Total length (>= 50000 bp)  1514179697                              
        # contigs                   3009254                                 
        Largest contig              354326                                  
        Total length                9922363287                              
        GC (%)                      36.86                                   
        N50                         9935                                    
        N75                         3044                                    
        L50                         205467                                  
        L75                         641585                                  
        # N's per 100 kbp           43517.06

#### Busco Genome:  

        C:30.6%[S:21.6%,D:9.0%],F:12.2%,M:57.2%,n:1440

        441     Complete BUSCOs (C)
        311     Complete and single-copy BUSCOs (S)
        130     Complete and duplicated BUSCOs (D)
        175     Fragmented BUSCOs (F)
        824     Missing BUSCOs (M)
        1440    Total BUSCO groups searched
