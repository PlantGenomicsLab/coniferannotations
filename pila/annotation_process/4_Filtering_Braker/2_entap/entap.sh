#!/bin/bash
#SBATCH --job-name=entap
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
###SBATCH -c 32
#SBATCH --mem=120G
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general

module load anaconda/2.4.0
module load perl/5.24.0
module load diamond/0.9.19
module load eggnog-mapper/0.99.1
module load interproscan/5.25-64.0  

/UCHC/LABS/Wegrzyn/EnTAP/EnTAP --runP -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /UCHC/LABS/Wegrzyn/ConiferGenomes/ConiferDB/ConiferGeneSet_099_protein150aa_RefSeqHeader_diamond.v0.9.19.dmnd -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd  --i ../gstats/gstats_output_multi/multi_pila_genes_without_introns.fasta.faa --qcoverage 70 --tcoverage 70 --taxon pinus --threads 32 --ontology 0


