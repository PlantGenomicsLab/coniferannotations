#!/bin/bash
#SBATCH --job-name=gff
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 12
#SBATCH --mem=25G
#SBATCH -o gff_%j.out
#SBATCH -e gff_%j.err
#SBATCH --partition=general

module load anaconda/2.4.0
module load perl/5.24.0
module load diamond/0.9.19
module load interproscan/5.25-64.0  

interproscan.sh -i ../3_remove_functionally_unannotated_genes/final_annotated.faa -o ./final_annotated_pfam.gff3 -f gff3 -appl pfam -goterms -iprlookup

