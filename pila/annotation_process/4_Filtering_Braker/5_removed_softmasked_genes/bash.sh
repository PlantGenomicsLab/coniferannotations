#!/bin/bash
# Submission script for Xanadu 
#SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --job-name=fasta
#SBATCH -o remGenes-%j.output
#SBATCH -e remGenes-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=25G


python removeGenes.py --fasta Pila.v.1.5.masked.fasta --path /isg/shared/databases/alignerIndex/plant/Pila/genome/v1.5/ --threshold 0.7 --out entap_gfacs_interpro_removeRetero_remMasked.gtf --gff final_annotated_remRet_corrected.gff3
