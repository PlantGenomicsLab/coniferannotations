#!/bin/bash
#SBATCH --job-name=gstats_psme
#SBATCH -n 16
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --mail-type=ALL
#SBATCH --mem=100G
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o psme_stats_%j.o
#SBATCH -e psme_stats_%j.e


module load perl
cd /UCHC/LABS/Wegrzyn/gstats/
perl gstats.pl \
-f braker_2.05_gtf \
-p multi_pila \
--statistics \
--splice-rescue \
--rem-extra-introns \
--rem-monoexonics \
--min-exon-size 20 \
--min-intron-size 9 \
--rem-extra-introns \
--rem-genes-without-stop-codon \
--rem-genes-without-start-codon \
--get-fasta-with-introns \
--get-fasta-without-introns \
--get-protein-fasta \
--create-gtf \
--fasta /isg/shared/databases/alignerIndex/plant/Pila/genome/v1.5/Pila.v.1.5.masked.fasta \
-O /UCHC/LABS/Wegrzyn/ConiferGenomes/Pila/analysis/braker2.05/braker/Pila1.5_Braker2.05_gth2train/analysis/gstats_output_multi \
/UCHC/LABS/Wegrzyn/ConiferGenomes/Pila/analysis/braker2.05/braker/Pila1.5_Braker2.05_gth2train/augustus.hints.gtf
