STATS:
Number of genes:	76390
Number of monoexonic genes:	
Number of multiexonic genes:	76390

Number of positive strand genes:	38244
Monoexonic:	
Multiexonic:	38244

Number of negative strand genes:	38146
Monoexonic:	
Multiexonic:	38146

Average overall gene size:	25375.912
Median overall gene size:	4086
Average overall CDS size:	863.750
Median overall CDS size:	684

Average size of monoexonic genes:	0.000
Median size of monoexonic genes:	
Largest monoexonic gene:	
Smallest monoexonic gene:	

Average size of multiexonic genes:	25375.912
Median size of multiexonic genes:	4086
Largest multiexonic gene:	493177
Smallest multiexonic gene:	251

Average size of multiexonic CDS:	863.750
Median size of multiexonic CDS:	684
Largest multiexonic CDS:	12744
Smallest multiexonic CDS:	201

Average size of multiexonic exons:	234.497
Median size of multiexonic exons:	159
Average size of multiexonic introns:	9134.693
Median size of multiexonic introns:	300

Average number of exons per multiexonic gene:	3.683
Median number of exons per multiexonic gene:	2
Largest multiexonic exon:	8004
Smallest multiexonic exon:	20
Most exons in one gene:	112

Average number of introns per multiexonic gene:	2.683
Median number of introns per multiexonic gene:	1
Largest intron:	471941
Smallest intron:	25

