#!/bin/bash
# Submission script for Xanadu 
#SBATCH --job-name=filter
#SBATCH -o filter-%j.output
#SBATCH -e filter-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1
#SBATCH --nodes=1 
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --mem=5G
#SBATCH --qos=general


python createFasta.py --fasta genes_without_introns.fasta --path ../1_gfacs/gfacs/ --nameList annotated_genes.txt --out genes_without_introns_annotated.fasta

python createFasta.py --fasta genes_without_introns.fasta.faa --path ../1_gfacs/gfacs/ --nameList annotated_genes.txt --out genes_without_introns_annotated.fasta.faa

python createGFF.py --gff out.gtf --path ../1_gfacs/gfacs/ --nameList annotated_genes.txt --out out_annotated.gtf
