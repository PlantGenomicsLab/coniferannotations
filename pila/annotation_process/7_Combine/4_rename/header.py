import os
import linecache

lines = [line.rstrip('\n') for line in open('../3_filter_gfacs_with_EnTAP/genes_without_introns_annotated.fasta')]
fastalines=[]
fastanames = []
new=[]
i=1
j=1
for row in lines:
    if row.startswith(">") == True:
      fastanames.append(row[1:])
      string = "PILA_"+str(j).zfill(5)
      j+=1
      new.append(string)
      fastalines.append(i)
    i+=1
fastalines.append(i)

filename9="../3_filter_gfacs_with_EnTAP/"+ "genes_without_introns_annotated.fasta"
with open("PITA.2_01.cds.fa", "w") as xyz:
  for g in fastanames:
      k=fastanames.index(g)
      newname=new[k]
      entry1=fastalines[k]
      entry2=fastalines[k+1]
      xyz.write("%s%s\n" %(">",newname))
      for m in range(entry1+1,entry2): ###take off write after the > line and ends right before the next entry
          xyz.write("%s" %(linecache.getline(filename9,m)))                                                      

lines = [line.rstrip('\n') for line in open('../3_filter_gfacs_with_EnTAP/genes_without_introns_annotated.fasta.faa')]
fastalines=[]
fastanames = []
new=[]
i=1
j=1
for row in lines:
   if row.startswith(">") == True:
     fastanames.append(row[1:])
     string = "PILA_"+str(j).zfill(5)
     j+=1
     new.append(string)
     fastalines.append(i)
   i+=1
fastalines.append(i)

filename9="../3_filter_gfacs_with_EnTAP/"+ "genes_without_introns_annotated.fasta.faa"
with open("PITA.2_01.peptides.fa", "w") as xyz:
 for g in fastanames:
     k=fastanames.index(g)
     newname=new[k]
     entry1=fastalines[k]
     entry2=fastalines[k+1]
     xyz.write("%s%s\n" %(">",newname))
     for m in range(entry1+1,entry2): ###take off write after the > line and ends right before the next entry
         xyz.write("%s" %(linecache.getline(filename9,m)))
