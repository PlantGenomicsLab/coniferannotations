#!/bin/bash
#SBATCH --job-name=busco
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=8
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err

#module load busco/3.0.2b

module load busco/4.0.2
module unload augustus
export PATH=/home/CAM/szaman/augustus-3.2.3/bin:/home/CAM/szaman/augustus-3.2.3/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus-3.2.3/config
###if your run crashes uncomment the following:
#module unload blast/2.7.1
#module load blast/2.2.29 

#run_BUSCO.py -i ../PILA.1_5.peptides.fa -l /isg/shared/databases/busco_lineages/embryophyta_odb9/ -o Busco_PILA_annotation -m prot -c 8

busco -i ../PILA.1_5.peptides.fa -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o buscoV4.0_PILA_annotation -m prot -c 8  
