#!/bin/bash
#SBATCH --job-name=gstats_pila
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --mail-type=ALL
#SBATCH --mem=10G
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o pila_stats_%j.o
#SBATCH -e pila_stats_%j.e
#SBATCH --qos=general 

module load perl/5.24.0
perl /labs/Wegrzyn/gFACs/gFACs.pl  \
-f gFACs_gtf \
-p final_pila1.5 \
--statistics \
--unique-genes-only \
--min-CDS-size 300 \
--allowed-inframe-stop-codons 0 \
--min-exon-size 9 \
--min-intron-size 9 \
--fasta /isg/shared/databases/alignerIndex/plant/Pila/genome/v1.5/Pila.v.1.5.masked.fasta \
-O ./ \
../PILA.1_5.gtf
