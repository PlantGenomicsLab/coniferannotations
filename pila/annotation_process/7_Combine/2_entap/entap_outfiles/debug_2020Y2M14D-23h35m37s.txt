Fri Feb 14 23:35:37 2020: Start - EnTAP
Fri Feb 14 23:35:37 2020: No inputted config file, using default: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_config.txt
Fri Feb 14 23:35:37 2020: Parsing configuration file...
Fri Feb 14 23:35:37 2020: Config file found at: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_config.txt
Fri Feb 14 23:35:37 2020: Success!
Fri Feb 14 23:35:37 2020: Assigning execution paths. Note they are not checked for validity yet...
Fri Feb 14 23:35:37 2020: Success! All exe paths set
Fri Feb 14 23:35:37 2020: ------------------------------------------------------
EnTAP Run Information - Execution
------------------------------------------------------
Current EnTAP Version: 0.9.0
Start time: Fri Feb 14 23:35:37 2020

Working directory has been set to: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles

Execution Paths/Commands:

RSEM Directory: /labs/Wegrzyn/EnTAP/libs/RSEM-1.3.0
GeneMarkS-T: perl /labs/Wegrzyn/EnTAP/libs/gmst_linux_64/gmst.pl
DIAMOND: diamond
InterPro: interproscan.sh
EggNOG SQL Database: /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP/databases/databases/eggnog.db
EggNOG DIAMOND Database: /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP/databases/bin/eggnog_proteins.dmnd
EnTAP Database (binary): /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP/databases/bin/entap_database.bin
EnTAP Database (SQL): /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//databases/entap_database.db
EnTAP Graphing Script: /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py

User Inputs:

contam: null
data-type: 0 
database: /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd /labs/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein_v2_150_headers.dmnd /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd 
e: 1.00e-05
fpkm: 0.50
input: ../1_gfacs/gfacs/genes_without_introns.fasta.faa
level: 0 3 4 
ontology: 0 
out-dir: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles
output-format: 1 4 3 
protein: pfam 
qcoverage: 70.00
runP: null
state: +
taxon: pinus
tcoverage: 70.00
threads: 12


Fri Feb 14 23:35:37 2020: User has input a database at: /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd
Fri Feb 14 23:35:37 2020: User has input a database at: /labs/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein_v2_150_headers.dmnd
Fri Feb 14 23:35:37 2020: User has input a database at: /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd
Fri Feb 14 23:35:37 2020: Verifying EnTAP database...
Fri Feb 14 23:35:37 2020: Reading serialized database from: /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP/databases/bin/entap_database.bin
 Of type: 2
Fri Feb 14 23:36:05 2020: Success!
Fri Feb 14 23:36:05 2020: Taxonomic species verified
Fri Feb 14 23:36:05 2020: Verifying software...
Fri Feb 14 23:36:05 2020: Executing command: 
diamond --version
Fri Feb 14 23:36:05 2020: 
Std Err:

Fri Feb 14 23:36:05 2020: Executing command: 
diamond --version
Fri Feb 14 23:36:06 2020: 
Std Err:

Fri Feb 14 23:36:06 2020: Success!
Fri Feb 14 23:36:06 2020: Success! Input verified
Fri Feb 14 23:36:06 2020: Killing Object - EntapDatabase
Fri Feb 14 23:36:12 2020: EnTAP Executing...
Fri Feb 14 23:36:12 2020: verifying state...
Fri Feb 14 23:36:12 2020: verifying state...
Fri Feb 14 23:36:12 2020: Success!
Fri Feb 14 23:36:12 2020: Processing transcriptome...
Fri Feb 14 23:36:12 2020: Transcriptome Lines - START
Fri Feb 14 23:36:12 2020: >g77857
Fri Feb 14 23:36:12 2020: MQALITPTHARVLSHLPRLETPRMSIDISFKPFNPTFSIPYSQIQFTHVRSSAGKRINLESRSIDRLHPIQASASQSSGGTDKWVFQFVGDGNSSHIGQPVSPPKAFELTSDVATVGRLPEKADIVIPIATVSGIHARLERKEGILFVTDLNSTNGTYIDNARLSPGAVTVLSVGSSITFGDINLATFRFSKEEGNATPIQGASLDEATNLASQG*
Fri Feb 14 23:36:12 2020: >gb|GEUZ01003485.1|
Fri Feb 14 23:36:12 2020: MTCTMDLNKLEGGNWMSSFLATLSEWRKLVSPVKGRKNSMFAIGFVLAVLVCSLFLGFNYPNKLVANPMDHRQSWFPSVFRDSSRSNSFFKFIFPDSTAEETDESVFDNAGQTPDRGANGIQAIPNNVSAPLVDNAGKMHDLRTNRTQGIVNNGSVPVINNAGPMSDPRAKSAHGIHKNGSKPVNSPVIDGLDKATISGNSTKRTAPGGFNNKPPAVVSNSGSSQNSTQGTVPVPPGVVSSSYSSKNSTQGTVPVPPGVVSSSDSSQSPKSIADSSVATPDLAPRSTELKPHGVVNGSTKSGEKKCDIFFGRWVPDDTGTLYPPGSCPHIDESFNCFLNRRPDNGYERWRWQPHDCNIPRLNATDMLEQLRGKRLAFVGDSLNRNMWESLVCILRHGAKNQSRVHEISGRSEFRTEGFYAFLYEDYNCSVEFVRAPFLVQEWEVDVGNGTKKETLRLDLIESSAPKFKEADVIVFNTGHWWTHEKTSKGKDYYQEGDHVYPELNVLEAFRKALTTWGKWVDKNINPNKSLVFFRGYSATHFSGGQWNSGGQCHKESEPIYNETFLTKYPPKMEVLESVLNEMKTPVMYLNITRITDYRKDAHPSIYRRQYPSQEERSERYQDCSHWCLPGVPDTWNELLYASMLIRGKGLRT*
Fri Feb 14 23:36:12 2020: >g207175
Fri Feb 14 23:36:12 2020: MPITGELSQQNIKDRYYGVNDPVAARLLNKAGEMPSLVPLEDESIKALYVGGLDSRVTEVDLKDNFYAYGEIESIILVFERACAFVTYTTTKGAEKALENFSNKLVIKGLRLKMLWGKPQDPKPEVEHQGDEAAKIISGERENTSHAAALVCAYLRTAANIKEMKEKKLNQFCFTGVLDCRPHFEP*
Fri Feb 14 23:36:12 2020: >g217806.1
Fri Feb 14 23:36:12 2020: MEDPCKRATQWLQSLQNTTPPSPILSSLDHSNITNIYDGFVLRGIQFQQITPGRLLCTFTVPHRLVDESGHWRAAALMTLVDILCAAAVVSCGLAVKVSVDYNVSYISQVKVHDEIEIDARVVGHQGGLSTVVVKLRNKGTAELVVCARQTMHNSTVFNRQTPFSKI*
Fri Feb 14 23:36:12 2020: >gb|GEUZ01000051.1|
Fri Feb 14 23:36:12 2020: Transcriptome Lines - END
Fri Feb 14 23:36:13 2020: Success!
Fri Feb 14 23:36:13 2020: Spawn Object - GraphingManager
Fri Feb 14 23:36:13 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -s -1 -g -1 -i /temp -t temp
Fri Feb 14 23:36:19 2020: 
Std Err:

Fri Feb 14 23:36:19 2020: Graphing is supported
Fri Feb 14 23:36:19 2020: Reading serialized database from: /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP/databases/bin/entap_database.bin
 Of type: 2
Fri Feb 14 23:36:44 2020: STATE - EXPRESSION FILTERING
Fri Feb 14 23:36:44 2020: No alignment file specified, skipping expression analysis
Fri Feb 14 23:36:44 2020: verifying state...
Fri Feb 14 23:36:44 2020: STATE - FRAME SELECTION
Fri Feb 14 23:36:44 2020: Protein sequences input, skipping frame selection
Fri Feb 14 23:36:44 2020: verifying state...
Fri Feb 14 23:36:44 2020: Beginning to copy final transcriptome to be used...
Fri Feb 14 23:36:45 2020: Success! Copied to: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/transcriptomes//genes_without_introns_final.fasta
Fri Feb 14 23:36:45 2020: verifying state...
Fri Feb 14 23:36:45 2020: STATE - SIMILARITY SEARCH
Fri Feb 14 23:36:45 2020: Spawn Object - SimilaritySearch
Fri Feb 14 23:36:45 2020: opendir: Path could not be read: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/figures/
Fri Feb 14 23:36:45 2020: opendir: Path could not be read: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed/
Fri Feb 14 23:36:45 2020: opendir: Path could not be read: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/overall_results
Fri Feb 14 23:36:45 2020: Spawn Object - ModDiamond
Fri Feb 14 23:36:45 2020: Verifying previous execution of database: /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd...
Fri Feb 14 23:36:45 2020: File for database uniprot_sprot does not exist.
/labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_uniprot_sprot.out
Fri Feb 14 23:36:45 2020: Verifying previous execution of database: /labs/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein_v2_150_headers.dmnd...
Fri Feb 14 23:36:45 2020: File for database conifer_geneSet_protein_v2_150_headers does not exist.
/labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_conifer_geneSet_protein_v2_150_headers.out
Fri Feb 14 23:36:45 2020: Verifying previous execution of database: /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd...
Fri Feb 14 23:36:45 2020: File for database plant does not exist.
/labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_plant.out
Fri Feb 14 23:36:45 2020: Success! Verified files for DIAMOND, continuing...
Fri Feb 14 23:36:45 2020: Executing DIAMOND for necessary files....
Fri Feb 14 23:36:45 2020: File not found, executing against database at: /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd
Fri Feb 14 23:36:45 2020: Executing command: 
diamond blastp -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd --query-cover 70.000000 --subject-cover 70.000000 --evalue 0.000010 --more-sensitive --top 3 -q /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/transcriptomes//genes_without_introns_final.fasta -o /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_uniprot_sprot.out -p 12 -f 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovhsp stitle
Fri Feb 14 23:41:44 2020: 
Std Err:

Fri Feb 14 23:41:44 2020: 
Printing to files:
Std Out: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_uniprot_sprot.out_std.out
Std Err: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_uniprot_sprot.out_std.err
Fri Feb 14 23:41:44 2020: Success! Results written to: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_uniprot_sprot.out
Fri Feb 14 23:41:44 2020: File not found, executing against database at: /labs/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein_v2_150_headers.dmnd
Fri Feb 14 23:41:44 2020: Executing command: 
diamond blastp -d /labs/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein_v2_150_headers.dmnd --query-cover 70.000000 --subject-cover 70.000000 --evalue 0.000010 --more-sensitive --top 3 -q /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/transcriptomes//genes_without_introns_final.fasta -o /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_conifer_geneSet_protein_v2_150_headers.out -p 12 -f 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovhsp stitle
Fri Feb 14 23:50:55 2020: 
Std Err:

Fri Feb 14 23:50:55 2020: 
Printing to files:
Std Out: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_conifer_geneSet_protein_v2_150_headers.out_std.out
Std Err: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_conifer_geneSet_protein_v2_150_headers.out_std.err
Fri Feb 14 23:50:55 2020: Success! Results written to: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_conifer_geneSet_protein_v2_150_headers.out
Fri Feb 14 23:50:55 2020: File not found, executing against database at: /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd
Fri Feb 14 23:50:55 2020: Executing command: 
diamond blastp -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.87.dmnd --query-cover 70.000000 --subject-cover 70.000000 --evalue 0.000010 --more-sensitive --top 3 -q /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/transcriptomes//genes_without_introns_final.fasta -o /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_plant.out -p 12 -f 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovhsp stitle
Sat Feb 15 01:29:39 2020: 
Std Err:

Sat Feb 15 01:29:39 2020: 
Printing to files:
Std Out: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_plant.out_std.out
Std Err: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_plant.out_std.err
Sat Feb 15 01:29:39 2020: Success! Results written to: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_plant.out
Sat Feb 15 01:29:39 2020: Beginning to filter individual DIAMOND files...
Sat Feb 15 01:29:39 2020: DIAMOND file located at /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_uniprot_sprot.out being parsed
Sat Feb 15 01:29:39 2020: Database file at /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_uniprot_sprot.out
Determined to be UniProt
Sat Feb 15 01:29:46 2020: File parsed, calculating statistics and writing output...
Sat Feb 15 01:29:48 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//uniprot_sprot/figures//_species_bar.png -t uniprot_sprot_Top_10_Species_Distribution -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//uniprot_sprot/figures//_species_bar.txt 
Sat Feb 15 01:29:52 2020: 
Std Err:

Sat Feb 15 01:29:52 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 2 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//uniprot_sprot/figures//_summary_bar.png -t uniprot_sprot_Summary -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//uniprot_sprot/figures//_summary_bar.txt 
Sat Feb 15 01:29:53 2020: 
Std Err:

Sat Feb 15 01:29:53 2020: Success!
Sat Feb 15 01:29:53 2020: DIAMOND file located at /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_conifer_geneSet_protein_v2_150_headers.out being parsed
Sat Feb 15 01:30:04 2020: File parsed, calculating statistics and writing output...
Sat Feb 15 01:30:07 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//conifer_geneSet_protein_v2_150_headers/figures//_species_bar.png -t conifer_geneSet_protein_v2_150_headers_Top_10_Species_Distribution -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//conifer_geneSet_protein_v2_150_headers/figures//_species_bar.txt 
Sat Feb 15 01:30:09 2020: 
Std Err:
Traceback (most recent call last):
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 282, in <module>
    main()
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 279, in main
    create_graphs(_software_flag)
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 139, in create_graphs
    sim_search_graphs()
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 112, in sim_search_graphs
    InputValues = parse_input(_stats_path)
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 236, in parse_input
    UserVals.values.append(int(values[1]))
IndexError: list index out of range

Sat Feb 15 01:30:09 2020: 
Error generating graph from:
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//conifer_geneSet_protein_v2_150_headers/figures//_species_bar.png -t conifer_geneSet_protein_v2_150_headers_Top_10_Species_Distribution -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//conifer_geneSet_protein_v2_150_headers/figures//_species_bar.txt 
Sat Feb 15 01:30:09 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 2 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//conifer_geneSet_protein_v2_150_headers/figures//_summary_bar.png -t conifer_geneSet_protein_v2_150_headers_Summary -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//conifer_geneSet_protein_v2_150_headers/figures//_summary_bar.txt 
Sat Feb 15 01:30:10 2020: 
Std Err:

Sat Feb 15 01:30:10 2020: Success!
Sat Feb 15 01:30:10 2020: DIAMOND file located at /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/blastp_genes_without_introns_final_plant.out being parsed
Sat Feb 15 02:11:42 2020: File parsed, calculating statistics and writing output...
Sat Feb 15 02:11:53 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//plant/figures//_species_bar.png -t plant_Top_10_Species_Distribution -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//plant/figures//_species_bar.txt 
Sat Feb 15 02:11:56 2020: 
Std Err:

Sat Feb 15 02:11:56 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 2 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//plant/figures//_summary_bar.png -t plant_Summary -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/processed//plant/figures//_summary_bar.txt 
Sat Feb 15 02:11:57 2020: 
Std Err:

Sat Feb 15 02:11:57 2020: Success!
Sat Feb 15 02:11:57 2020: Calculating overall Similarity Searching statistics...
Sat Feb 15 02:11:59 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/overall_results/figures//_species_bar.png -t _Top_10_Species_Distribution -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/overall_results/figures//_species_bar.txt 
Sat Feb 15 02:12:01 2020: 
Std Err:
Traceback (most recent call last):
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 282, in <module>
    main()
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 279, in main
    create_graphs(_software_flag)
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 139, in create_graphs
    sim_search_graphs()
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 112, in sim_search_graphs
    InputValues = parse_input(_stats_path)
  File "/labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py", line 236, in parse_input
    UserVals.values.append(int(values[1]))
IndexError: list index out of range

Sat Feb 15 02:12:01 2020: 
Error generating graph from:
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/overall_results/figures//_species_bar.png -t _Top_10_Species_Distribution -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/overall_results/figures//_species_bar.txt 
Sat Feb 15 02:12:01 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 2 -s 3 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/overall_results/figures//_summary_bar.png -t _Summary -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/similarity_search/DIAMOND/overall_results/figures//_summary_bar.txt 
Sat Feb 15 02:12:02 2020: 
Std Err:

Sat Feb 15 02:12:02 2020: Success!
Sat Feb 15 02:12:02 2020: verifying state...
Sat Feb 15 02:12:02 2020: STATE - GENE ONTOLOGY
Sat Feb 15 02:12:02 2020: Spawn Object - Ontology
Sat Feb 15 02:12:02 2020: opendir: Path could not be read: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures/
Sat Feb 15 02:12:02 2020: opendir: Path could not be read: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/processed/
Sat Feb 15 02:12:02 2020: Spawn Object - ModEggnogDMND
Sat Feb 15 02:12:02 2020: Overwrite was unselected, verifying output files...
Sat Feb 15 02:12:02 2020: 
File Status at: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/blastp_genes_without_introns_eggnog_proteins.out
    Error: File empty
    Error: File could not be found
    Error: File could not be opened
Sat Feb 15 02:12:02 2020: Errors in opening file, continuing with execution...
Sat Feb 15 02:12:02 2020: Running EggNOG against Diamond database...
Sat Feb 15 02:12:02 2020: Executing command: 
diamond blastp -d /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP/databases/bin/eggnog_proteins.dmnd --top 1 --more-sensitive -q /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/transcriptomes//genes_without_introns_final.fasta -o /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/blastp_genes_without_introns_eggnog_proteins.out -p 12 -f 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovhsp stitle
Sat Feb 15 03:26:10 2020: 
Std Err:

Sat Feb 15 03:26:10 2020: 
Printing to files:
Std Out: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/blastp_genes_without_introns_eggnog_proteins__std.out
Std Err: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/blastp_genes_without_introns_eggnog_proteins__std.err
Sat Feb 15 03:26:10 2020: Parsing EggNOG DMND file located at: /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/blastp_genes_without_introns_eggnog_proteins.out
Sat Feb 15 03:26:10 2020: Beginning to parse EggNOG results...
Sat Feb 15 03:26:11 2020: Alignments parsed: 5000
Sat Feb 15 03:26:11 2020: Alignments parsed: 10000
Sat Feb 15 03:26:18 2020: Alignments parsed: 15000
Sat Feb 15 03:26:25 2020: Alignments parsed: 20000
Sat Feb 15 03:26:25 2020: Alignments parsed: 25000
Sat Feb 15 03:26:38 2020: Alignments parsed: 30000
Sat Feb 15 03:26:39 2020: Alignments parsed: 35000
Sat Feb 15 03:26:46 2020: Alignments parsed: 40000
Sat Feb 15 03:26:47 2020: Alignments parsed: 45000
Sat Feb 15 03:26:47 2020: Alignments parsed: 50000
Sat Feb 15 03:26:54 2020: Alignments parsed: 55000
Sat Feb 15 03:26:54 2020: Alignments parsed: 60000
Sat Feb 15 03:26:55 2020: Alignments parsed: 65000
Sat Feb 15 03:26:56 2020: Alignments parsed: 70000
Sat Feb 15 03:27:02 2020: Alignments parsed: 75000
Sat Feb 15 03:27:03 2020: Alignments parsed: 80000
Sat Feb 15 03:27:04 2020: Alignments parsed: 85000
Sat Feb 15 03:27:05 2020: Alignments parsed: 90000
Sat Feb 15 03:27:05 2020: Success!
Sat Feb 15 03:27:05 2020: Success! Calculating statistics and accessing database...
Sat Feb 15 03:27:05 2020: Opening SQL database...
Sat Feb 15 03:27:05 2020: Opening SQL database at: /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP/databases/databases/eggnog.db
Sat Feb 15 03:27:05 2020: Success!
Sat Feb 15 03:27:05 2020: Getting EggNOG database SQL version...
Sat Feb 15 03:27:05 2020: Executing sql cmd: SELECT version FROM version
Sat Feb 15 03:27:05 2020: WARNING: couldn't get SQL version.Setting to earlier version by default
Sat Feb 15 05:15:43 2020: Killing object - EggNOG Database
Sat Feb 15 05:15:43 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//eggnog_tax_scope.png -t Top_Tax_Levels -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//eggnog_tax_scope.txt 
Sat Feb 15 05:15:46 2020: 
Std Err:

Sat Feb 15 05:15:46 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//molecular_function0_go_bar_graph.png -t Top_GO_Molecular_Terms_Level:_0 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//molecular_function0_go_bar_graph.txt 
Sat Feb 15 05:15:48 2020: 
Std Err:

Sat Feb 15 05:15:48 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//cellular_component0_go_bar_graph.png -t Top_GO_Cellular_Terms_Level:_0 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//cellular_component0_go_bar_graph.txt 
Sat Feb 15 05:15:50 2020: 
Std Err:

Sat Feb 15 05:15:50 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//overall0_go_bar_graph.png -t Top_GO_Terms_Level:_0 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//overall0_go_bar_graph.txt 
Sat Feb 15 05:15:52 2020: 
Std Err:

Sat Feb 15 05:15:52 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//biological_process0_go_bar_graph.png -t Top_GO_Biological_Terms_Level:_0 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//biological_process0_go_bar_graph.txt 
Sat Feb 15 05:15:54 2020: 
Std Err:

Sat Feb 15 05:15:54 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//molecular_function3_go_bar_graph.png -t Top_GO_Molecular_Terms_Level:_3 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//molecular_function3_go_bar_graph.txt 
Sat Feb 15 05:15:56 2020: 
Std Err:

Sat Feb 15 05:15:56 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//cellular_component3_go_bar_graph.png -t Top_GO_Cellular_Terms_Level:_3 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//cellular_component3_go_bar_graph.txt 
Sat Feb 15 05:15:58 2020: 
Std Err:

Sat Feb 15 05:15:58 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//overall3_go_bar_graph.png -t Top_GO_Terms_Level:_3 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//overall3_go_bar_graph.txt 
Sat Feb 15 05:16:00 2020: 
Std Err:

Sat Feb 15 05:16:00 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//biological_process3_go_bar_graph.png -t Top_GO_Biological_Terms_Level:_3 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//biological_process3_go_bar_graph.txt 
Sat Feb 15 05:16:02 2020: 
Std Err:

Sat Feb 15 05:16:02 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//molecular_function4_go_bar_graph.png -t Top_GO_Molecular_Terms_Level:_4 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//molecular_function4_go_bar_graph.txt 
Sat Feb 15 05:16:04 2020: 
Std Err:

Sat Feb 15 05:16:04 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//cellular_component4_go_bar_graph.png -t Top_GO_Cellular_Terms_Level:_4 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//cellular_component4_go_bar_graph.txt 
Sat Feb 15 05:16:06 2020: 
Std Err:

Sat Feb 15 05:16:06 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//overall4_go_bar_graph.png -t Top_GO_Terms_Level:_4 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//overall4_go_bar_graph.txt 
Sat Feb 15 05:16:08 2020: 
Std Err:

Sat Feb 15 05:16:08 2020: Executing command: 
python /labs/Wegrzyn/EnTAP/EnTAP_v0.9.0/EnTAP//src/entap_graphing.py -g 1 -s 4 -p /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//biological_process4_go_bar_graph.png -t Top_GO_Biological_Terms_Level:_4 -i /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/2_entap/entap_outfiles/ontology//EggNOG_DMND/figures//biological_process4_go_bar_graph.txt 
Sat Feb 15 05:16:10 2020: 
Std Err:

Sat Feb 15 05:16:10 2020: Success! EggNOG results parsed
Sat Feb 15 05:16:10 2020: Killing Object - ModEggnogDMND
Sat Feb 15 05:16:10 2020: Beginning to print final results...
Sat Feb 15 05:16:34 2020: Success!
Sat Feb 15 05:16:34 2020: verifying state...
Sat Feb 15 05:16:34 2020: Pipeline finished! Calculating final statistics...
Sat Feb 15 05:16:34 2020: Killing Object - QueryData
Sat Feb 15 05:16:39 2020: QuerySequence data freed
Sat Feb 15 05:16:39 2020: Killing Object - EntapDatabase
Sat Feb 15 05:16:50 2020: End - EnTAP
Sat Feb 15 05:16:50 2020: Killing object - FileSystem
Sat Feb 15 05:16:50 2020: Killing object - UserInput
