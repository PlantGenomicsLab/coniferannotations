#!/bin/bash
# Submission script for Xanadu 
####SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --job-name=mergeBam
#SBATCH -o mergeBam-%j.output
#SBATCH -e mergeBam-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1
#SBATCH --nodes=1 
#SBATCH --cpus-per-task=8
#SBATCH --partition=general

module load samtools/1.3.1

#these sorted files are sorted by read name (for BRAKER) 
samtools merge ./Pila_AllBamMerged.bam ../../hiseq/sortByReadName/sorted_*.bam ../../miseq/sortByReadName/sorted_*.bam 
