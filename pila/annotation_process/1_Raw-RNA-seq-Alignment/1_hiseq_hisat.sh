#!/bin/bash
# Submission script for Xanadu 
####SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --job-name=sickle_Pila
#SBATCH -o sickle-%j.output
#SBATCH -e sickle-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=16
#SBATCH --partition=general
#####SBATCH --mem-per-cpu=256000 ###(256G)

module load hisat2/2.0.5

myarr=()
for f in ../../sickle/hiseq/trimmed*.fastq
#for f in ../../raw_reads/hiseq/*.fastq
do 
	myarr+=($(echo "$f" | awk -F'[/_]' '{ print $6 }'))
done

#for i in ${myarr[@]}; do echo $i; done
uniq_file=($(echo "${myarr[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))

for i in ${uniq_file[@]}
do
	echo $i
	f1="../../sickle/hiseq/trimmed_"$i"_1.fastq"
	f2="../../sickle/hiseq/trimmed_"$i"_2.fastq"
	echo $f1
	echo $f2
	hisat2 -q --max-intronlen 2000000 -x /isg/shared/databases/alignerIndex/plant/Pila/hisat/v1.5_masked/pila_v1.5.5000.masked -1 $f1 -2 $f2 -S $i.sam
done
