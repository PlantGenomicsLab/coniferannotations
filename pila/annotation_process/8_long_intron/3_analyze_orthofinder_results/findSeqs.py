import linecache
import argparse
import os
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq

parser = argparse.ArgumentParser(
     prog='findSeqs.py',
     usage='''python findSeqs.py --og [gff file given by braker] --path [Path of gff file] --nameList [name of list file] --pathList [path of the list file]''',
     description='''This program pulls out the gff of specific genes, given the gff file and a list of sequences saved in a text file''',
     epilog='''It requires numpy and biopython libraries''')
parser.add_argument('--og', type=str, help='The name of the orthogroup file', required=True)
parser.add_argument('--path', type=str, help='The path of the orthogroup file', required=False)
parser.add_argument('--nameList', type=str, help='name of the list file that contains name of sequences', required=True)
parser.add_argument('--pathList', type=str, help='path of list file', required=False)

args=parser.parse_args()
path=args.path
og=args.og
listName=args.nameList
listPath=args.pathList


if path==None:
    ogFile=og
else:
    ogFile=os.path.join(path, og)

if listPath==None:
    listFile=listName
else:
    listFile=os.path.join(listPath, listName)

keepOGs=[line.rstrip('\n').split("\t")[0] for line in open(listFile)]
print(keepOGs[:5])

keep=[]

for row in open(ogFile,'r'):
    if "Orthogroup" in row:
        pass
    else:
        #print(row)
        long_intron_genes=[]
        cols=row.split(",")
        orthogroup_name=cols[0]
        #print(cols[1])
        if orthogroup_name in keepOGs:
            fastafile="../2_orthofinder/species/OrthoFinder/Results_Mar11/Orthogroup_Sequences/"+orthogroup_name+".fa"
            id_dict=SeqIO.to_dict(SeqIO.parse(fastafile, "fasta"))
            if len(id_dict)>=7:
                f=open(fastafile, "r")    
                contents=f.read()
                if contents.count(">PILA") > 0 and contents.count(">PITA") > 0 and contents.count(">SEGI") > 0 and contents.count(">Gb_") > 0 and contents.count(">gnl") > 0 and contents.count(">Pz") > 0 and contents.count(">ERN") > 0:
                    keep.append(orthogroup_name)
                else:
                    pass
                    #print(orthogroup_name)

print(len(keep))

cds_fastafile="/labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/8_long_intron/2_orthofinder/1_clean-up/cds/all_species_coding_genes.fna"

cds_fasta=SeqIO.to_dict(SeqIO.parse(cds_fastafile, "fasta"))

for k in keep:
    with open(k+".cds.fa", 'w') as out:
        fastafile="../2_orthofinder/species/OrthoFinder/Results_Mar11/Orthogroup_Sequences/"+k+".fa"
        id_dict=SeqIO.to_dict(SeqIO.parse(fastafile, "fasta"))
        for record in id_dict:
            header=id_dict[record].id
            sequence=cds_fasta[record].seq
            out.write(">%s\n%s\n" %(header,sequence)) 
