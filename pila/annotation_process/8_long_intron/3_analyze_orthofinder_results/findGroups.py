import linecache
import argparse
import os

parser = argparse.ArgumentParser(
     prog='createGFF.py',
     usage='''python createGFF.py --og [gff file given by braker] --path [Path of gff file] --nameList [name of list file] --pathList [path of the list file] --out [name of output file]''',
     description='''This program pulls out the gff of specific genes, given the gff file and a list of sequences saved in a text file''',
     epilog='''It requires numpy and biopython libraries''')
parser.add_argument('--og', type=str, help='The name of the orthogroup file', required=True)
parser.add_argument('--path', type=str, help='The path of the orthogroup file', required=False)
parser.add_argument('--nameList', type=str, help='name of the list file that contains name of sequences', required=True)
parser.add_argument('--pathList', type=str, help='path of list file', required=False)
parser.add_argument('--out', type=str, help='name of output fasta file', required=True)

args=parser.parse_args()
path=args.path
og=args.og
listName=args.nameList
listPath=args.pathList
output=args.out

if path==None:
    ogFile=og
else:
    ogFile=os.path.join(path, og)

if listPath==None:
    listFile=listName
else:
    listFile=os.path.join(listPath, listName)

singleCopyFile=os.path.join("../2_orthofinder/species/OrthoFinder/Results_Mar11/Orthogroups/Orthogroups_SingleCopyOrthologues.txt")

keep=[line.rstrip('\n') for line in open(listFile)]
#print(keep[:5])
singleCopy=[line.rstrip('\n') for line in open(singleCopyFile)]

count=0
shared=0

with open(output, 'w') as out:
    for row in open(ogFile,'r'):
        if "Orthogroup" in row:
            pass
        else:
            #print(row)
            long_intron_genes=[]
            cols=row.split(",")
            orthogroup_name=cols[0]
            #print(cols[1])
            genes=cols[1:]
            #print(genes)
            for gene in genes:
                if "PILA_" in gene:
                    #print(gene)
                    if gene.strip() in keep:
                        long_intron_genes.append(gene.strip())
                        count=count+1
                        if orthogroup_name in singleCopy:
                            print(orthogroup_name)
            if long_intron_genes!=[]:
                shared=shared+1
                str_list=','.join(map(str, long_intron_genes))
                out.write("%s\t%s\n" %(orthogroup_name,str_list)) 

print ('number of long intron genes that belong in an orthgroup:', count)
print ('number of orthogroups across which long intron genes are shared:', shared)
