#!/bin/bash
#SBATCH --job-name=fasta
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mem=4G
#SBATCH -o fasta_%j.out
#SBATCH -e fasta_%j.err

python /labs/Wegrzyn/ConiferGenomes/coniferannotations/scripts/misc/createFasta.py --fasta PILA.1_5.peptides.fa --path /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/4_rename/ --nameList 50_99kbps_genes.txt --out 50_99kbps_genes.faa

python /labs/Wegrzyn/ConiferGenomes/coniferannotations/scripts/misc/createFasta.py --fasta PILA.1_5.peptides.fa --path /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/4_rename/ --nameList 100_149kbps_genes.txt --out 100_149kbps_genes.faa

python /labs/Wegrzyn/ConiferGenomes/coniferannotations/scripts/misc/createFasta.py --fasta PILA.1_5.peptides.fa --path /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/4_rename/ --nameList 150_199kbps_genes.txt --out 150_199kbps_genes.faa

python /labs/Wegrzyn/ConiferGenomes/coniferannotations/scripts/misc/createFasta.py --fasta PILA.1_5.peptides.fa --path /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/4_rename/ --nameList 200_249kbps_genes.txt --out 299_249kbps_genes.faa

python /labs/Wegrzyn/ConiferGenomes/coniferannotations/scripts/misc/createFasta.py --fasta PILA.1_5.peptides.fa --path /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/4_rename/ --nameList 250_299kbps_genes.txt --out 250_299kbps_genes.faa

python /labs/Wegrzyn/ConiferGenomes/coniferannotations/scripts/misc/createFasta.py --fasta PILA.1_5.peptides.fa --path /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/7_Combine/4_rename/ --nameList 300kbps_genes.txt --out 300kbps_genes.faa
