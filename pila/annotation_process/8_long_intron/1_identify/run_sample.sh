#!/bin/bash
#SBATCH --job-name=gfacs
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mem=4G
#SBATCH -o gfacs_%j.out
#SBATCH -e gfacs_%j.err

module load perl/5.24.0

perl /labs/Wegrzyn/gFACs/gFACs.pl -f gFACs_gtf -p pila --statistics --distributions intron_lengths 10000 --intron_position --intron_position_data -O ./ ../7_Combine/4_rename/PILA.1_5.gtf



