import linecache
import argparse
import os

parser = argparse.ArgumentParser(
     prog='createGFF.py',
     usage='''python createGFF.py --gff [gff file given by braker] --path [Path of gff file]''')
parser.add_argument('--gff', type=str, help='The name of the gff file', required=True)
parser.add_argument('--path', type=str, help='The path of the gff file', required=False)

args=parser.parse_args()
path=args.path
gff=args.gff

if path==None:
    gffFile=gff
else:
    gffFile=os.path.join(path, gff)

len_dict={}


for row in open(gffFile,'r'):
    cols=row.split(" ")
    if cols[8] not in len_dict.keys():
        len_dict[cols[8]]=int(cols[9].strip())
    else:
        if  len_dict.get(cols[8]) < int(cols[9]):
            len_dict[cols[8]]=int(cols[9].strip())

for l in len_dict:
    print (l+"\t"+str(len_dict[l]))
