import linecache
import argparse
import os

parser = argparse.ArgumentParser(
     prog='gene_enrch.py',
     usage='''python gene_enrch.py --annot [gff file given by braker] --path [Path of gff file] --nameList [name of list file] --pathList [path of the list file] --out [name of output fasta file]''',
     description='''This program pulls out the gff of specific genes, given the gff file and a list of sequences saved in a text file''',
     epilog='''It requires numpy and biopython libraries''')
parser.add_argument('--annot', type=str, help='The name of the gff file', required=True)
parser.add_argument('--path', type=str, help='The path of the gff file', required=False)
parser.add_argument('--nameList', type=str, help='name of the list file that contains name of sequences', required=False)
parser.add_argument('--pathList', type=str, help='path of list file', required=False)
parser.add_argument('--out', type=str, help='name of output fasta file', required=True)

args=parser.parse_args()
path=args.path
gff=args.annot
listName=args.nameList
listPath=args.pathList
output=args.out

if path==None:
    gffFile=gff
else:
    gffFile=os.path.join(path, gff)

rows=[line.rstrip('\n') for line in open(gffFile)]

go={}

for row in rows[1:]:
    cols=row.split("\t")
    if cols[33]!="":
        GO_bio=cols[33].split(",")
        for g in GO_bio:
            if g in go and g!="":
                freq=go[g]+1
                go[g]=freq 
            elif g not in go and g!="":
                go[g]=1

sorted_go=sorted(go.items(), key=lambda x: x[1], reverse=True)

for g in sorted_go:
    print(g[0]+"\t"+str(g[1]))
