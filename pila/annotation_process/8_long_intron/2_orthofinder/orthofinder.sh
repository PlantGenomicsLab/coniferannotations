#!/bin/bash
#SBATCH --job-name=orthofinder
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=16G
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o orthofinder_%j.out
#SBATCH -e orthofinder_%j.err
 
module load OrthoFinder/2.3.3
module load muscle
module load DLCpar/1.0
module load FastME
module load diamond/0.9.25
module load mcl
module load scipy/1.0.0
orthofinder -t 12 -f /labs/Wegrzyn/ConiferGenomes/coniferannotations/pila/annotation_process/8_long_intron/2_orthofinder/species/
