#Annotation Location  
/isg/treegenes/treegenes_store/FTP/Genomes/Pila/v1.5/annotation/

#Annotation Process for Sugar Pine 
### Processing transcripts & pre-existing gene models:
####    Data Location:  
&nbsp;&nbsp;&nbsp;1. Pacbio --> $PWD/Genes/Pacbio/pacbio_pre_frame_selection.fasta  
&nbsp;&nbsp;&nbsp;2. Maker --> $PWD/Genes/Maker/maker_transcripts.fasta 
####    Processing Genes  
&nbsp;&nbsp;&nbsp;Processing these genes includes:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. Concatenating all sequences  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. Frame selecting for complete genes (using Genemarks-t)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. Aligning complete genes to the genome  (with gmap)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4. Alignments are filtered for completeness & canonical splice sites  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5. Genes are translated into protein (.faa files) then filtered for Met start, STOP codon at the end, only 1 permitted in-frame stop codon, and 150aa. The sequences are trimmed to the first in-frame stop codon that is seen.   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6. Proteins are run against Diamond using a set of conifer genes.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(conifer database located here: /UCHC/LABS/Wegrzyn/ConiferGenomes/ConiferDB/conifer_geneSet_protein150aa.fasta)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7. Diamond hits are filtered into a new gff3, fasta.faa, and fasta.fnn.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;README for each source is located in it's respective directory.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Processed Files are located here:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. $PWD/Genes/Pacbio/Diamond_filtered_Protein_filtered_CompAlignedCanonicalPacbioGenes_Pila.(faa,fnn,gff3)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. $PWD/Genes/Maker/Diamond_filtered_Protein_filtered_CompAlignedCanonicalMakerGenes_Pila.fasta.(faa,fnn, gff3)  
#### Hisat2 Alignment:  
&nbsp;&nbsp;&nbsp;/labs/Wegrzyn/ConiferGenomes/Pila/analysis/hisat2/    
#### Protein Alignment using GenomeThreader:  
&nbsp;&nbsp;&nbsp;*Note: Protein Alignments were done by using the processed the pacbio & maker described above:  
&nbsp;&nbsp;&nbsp;$PWD/analysis/braker2.05/protein_alignments/  
#### Braker Run:  
&nbsp;&nbsp;&nbsp;More details on how to execute braker is located in the scripts folder.  
&nbsp;&nbsp;&nbsp;$PWD/UCHC/LABS/Wegrzyn/ConiferGenomes/Pila/analysis/braker2.05/braker2.05NoSkipModBamHome.sh  
&nbsp;&nbsp;&nbsp;Braker results are located here: $PWD/analysis/braker2.05/braker/Pila1.5_Braker2.05_gth2train  
#### Post-processing braker: 
&nbsp;&nbsp;&nbsp;The resulting genes from braker were processed  by gFACS for complete, multiexonic genes with exons at least 20 bps long  
&nbsp;&nbsp;&nbsp;and introns at least 9 bps long. The resulting genes were also processed by EnTAP for having functional protein domains.  
&nbsp;&nbsp;&nbsp;The remaining genes were processed by InterPro scan and removed if they had a similarity hit to reterotransposon elements.    
&nbsp;&nbsp;&nbsp;$PWD/analysis/braker2.05/braker/Pila1.5_Braker2.05_gth2train/analysis/
#### Overlap:  
&nbsp;&nbsp;&nbsp;To resolve gene fragmentation, we look for nested gene models between various methods i.e _ab initio_ (maker and braker) and _de novo_ assembled transcriptome (Pacbio):  
&nbsp;&nbsp;&nbsp;$PWD/UCHC/LABS/Wegrzyn/ConiferGenomes/Pila/analysis/braker/Pila1.5_Braker2.05_gth2train/bedtools/  
#### Gstats Post-overlap & removing genes < 450 bps:  
&nbsp;&nbsp;&nbsp;$PWD/UCHC/LABS/Wegrzyn/ConiferGenomes/Pila/analysis/gstats_post_overlap/all/gstats_out/filter150aa/
#### Renaming Gene Models:
&nbsp;&nbsp;&nbsp;$PWD/UCHC/LABS/Wegrzyn/ConiferGenomes/Pila/analysis/
#### Removing duplicate gene models (i.e. genes on same scaffold with same start and stop site)
&nbsp;&nbsp;&nbsp;$PWD/analysis/braker2.05/braker/Pila1.5_Braker2.05_gth2train/analysis/gstats_post_overlap/all/gstats_out/filter150aa/gstats_out_150aa/rename/remDup
#### Functional Annotation of final set of genes (using EnTAP)
&nbsp;&nbsp;&nbsp;$PWD/analysis/braker2.05/braker/Pila1.5_Braker2.05_gth2train/analysis/gstats_post_overlap/all/gstats_out/filter150aa/gstats_out_150aa/rename/remDup/filterEntap