#!/bin/bash
# Submission script for Xanadu 
####SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --job-name=brakerPila
#SBATCH -o brakerPila-%j.output
#SBATCH -e brakerPila-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=48
#SBATCH --partition=general
#SBATCH --mem=250G

#$SLURM_TMPDIR="/UCHC/LABS/Wegrzyn/ConiferGenomes/Pila/analysis/braker/"
TMPDIR=/home/CAM/szaman/testBraker2.0/augustus/pitaBraker2.0TempAugustus
mkdir -p "$TMPDIR"
echo $TMPDIR
module load perl/5.24.0
export PERL5LIB=/home/CAM/szaman/perl5/lib/perl5/
export PATH=/home/CAM/szaman/augustus-3.2.3/bin:/home/CAM/szaman/augustus-3.2.3/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus-3.2.3/config
module load samtools/1.7 
module load genomethreader/1.6.6
module load bamtools/2.5.1

~/2.0.5/braker.pl --cores 32 --genome=/isg/shared/databases/alignerIndex/plant/Pila/genome/v1.5/Pila.v.1.5.masked.fasta --species=Pila1.5_Braker2.05_gth2train --hints=/UCHC/LABS/Wegrzyn/ConiferGenomes/Pila/analysis/braker2.05/protein_alignments/prep/hintsfile.gff --prot_aln=/UCHC/LABS/Wegrzyn/ConiferGenomes/Pila/analysis/braker2.05/protein_alignments/prep/all.multiexonics.protAligned.pilav1.05.gth.gff3 --GENEMARK_PATH=/home/CAM/szaman/gm_et_linux_64/gmes_petap/ --BAMTOOLS_PATH=/isg/shared/apps/bamtools/2.5.1/bin -prg=gth --gth2traingenes --softmasking 1 --gff3


