#!/bin/bash
#SBATCH --job-name=gmap
#SBATCH -n 20
#SBATCH --mem=60G
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o gmap_%j.out
#SBATCH -e gmap_%j.err
#SBATCH --qos=general
# Run the program 

module load gmap/2019-06-10 
#for CONIFERS ONLY
#gmapl -K 1000000 -L 10000000 --cross-species -D /isg/shared/databases/alignerIndex/plant/Pila/gmap/v1.5/pila_5000bps_masked/ -d pila_5000bps_masked -f gff3_gene GEUZ01.1.fsa_nt --nthreads=12 --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 > illumina_pacbio_genes_pila_noFullLength.gff3

gmapl -K 1000000 -L 10000000 --cross-species -T -F -D  /isg/shared/databases/alignerIndex/plant/Pila/gmap/v1.5/gmap-2019-06-10/Pila.1_5.gmap/ -d Pila.1_5.gmap -f gff3_gene GEUZ01.1.fsa_nt --nthreads=20 --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 > illumina_pacbio_genes_pila_FullLength.gff3
