#!/bin/bash
#SBATCH --job-name=gfacs_pila
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=himem
#SBATCH --mail-type=ALL
#SBATCH --mem=30G
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o pita_stats_%j.o
#SBATCH -e pita_stats_%j.e
#SBATCH --qos=himem

module load perl/5.24.0
perl /labs/Wegrzyn/gFACs/gFACs.pl  \
-f gmap_2017_03_17_gff3 \
--statistics \
--min-CDS-size 300 \
--min-exon-size 9 \
--min-intron-size 9 \
--allowed-inframe-stop-codons 0 \
--get-fasta-without-introns \
--get-protein-fasta \
--create-gtf \
--fasta /isg/shared/databases/alignerIndex/plant/Pila/genome/v1.5/Pila.v.1.5.masked.fasta \
-O ./gfacs_noInframeStop \
../1_gmap/maker_genes_pila_FullLength.gff3
