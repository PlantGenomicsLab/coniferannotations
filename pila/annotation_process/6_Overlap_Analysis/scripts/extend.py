import os
import linecache
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq

bed = [line.rstrip('\n') for line in open('./resolve_merged.bed')]
bed2 = [line.rstrip('\n') for line in open('./resolve_merged_temp.bed')]
id_dict = SeqIO.to_dict(SeqIO.parse("../../../PitaGeneScaffs.fasta", "fasta"))
genes = []
scaff = []
names = []
for b in bed:
    cols=b.split('\t')
    scaff.append(cols[0])
    genes.append(b)
 
print len(genes) 
print len(scaff)

for b in bed2:
    cols = b.split('\t')
    names.append(cols[3])

with open ("./FinalBrakerGeneSet_merged.fasta", "w") as xyz:
    for i in range(len(scaff)):
        my_seq=""  
        line = genes[i] 
        cols=line.split('\t')
        if "-" in cols[3]:
            seq = id_dict[cols[0]].seq[int(cols[1]):int(cols[2])].reverse_complement() 
            my_seq = seq + my_seq
        else:
            seq = id_dict[cols[0]].seq[int(cols[1]):int(cols[2])]
            my_seq = my_seq + seq
        my_seq=my_seq.upper()
        #print "actual length of the gene: ", len(my_seq)
        #print "predicted length of the gene: ", int(cols[2])-int(cols[1])
        if my_seq[0:3]=="ATG" and (my_seq[-3:] == "TAA" or my_seq[-3:]=="TGA" or my_seq[-3:]=="TAG"):
            print "at least it's complete"
            my_prot = my_seq.translate()
            print my_prot+"\n"
            if my_prot.count("*") > 1:
                print my_prot.count("*")
            elif my_prot.count("*")==1:
                #print names[i]
                xyz.write("%s%s\n" %(">",names[i]))
                xyz.write("%s\n" %(my_prot))  
