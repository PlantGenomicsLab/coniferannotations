import linecache
import argparse
import os

parser = argparse.ArgumentParser(
     prog='createGFF.py',
     usage='''python createGFF.py --gff [gff file given by braker] --path [Path of gff file] --nameList [name of list file] --pathList [path of the list file] --out [name of output fasta file]''',
     description='''This program pulls out the gff of specific genes, given the gff file and a list of sequences saved in a text file''',
     epilog='''It requires numpy and biopython libraries''')
parser.add_argument('--gff', type=str, help='The name of the gff file', required=True)
parser.add_argument('--path', type=str, help='The path of the gff file', required=False)
parser.add_argument('--nameList', type=str, help='name of the list file that contains name of sequences', required=True)
parser.add_argument('--pathList', type=str, help='path of list file', required=False)
parser.add_argument('--out', type=str, help='name of output fasta file', required=True)

args=parser.parse_args()
path=args.path
gff=args.gff
listName=args.nameList
listPath=args.pathList
output=args.out

if path==None:
    gffFile=gff
else:
    gffFile=os.path.join(path, gff)

if listPath==None:
    listFile=listName
else:
    listFile=os.path.join(listPath, listName)

mrna=[]
mrnalines=[]
genes=[]
i=1
for row in open(gffFile,'r'):
    if "\tmRNA\t" in row or "\tgene\t" in row:
        cols=row.split("\t")
        id=cols[8].strip()
        mrna.append(id[3:])
        mrnalines.append(i)
    i+=1    
mrnalines.append(i)
print mrna[:5]
keep=[line.rstrip('\n') for line in open(listFile)]
print keep[:5]

with open(output,'w') as seqs:
   for name in mrna:
       if name in keep:
           k=mrna.index(name)
           entry1=mrnalines[k]
           entry2=mrnalines[k+1]
           geneGFF=""
           for m in range(entry1,entry2):
	       geneGFF+=linecache.getline(gffFile,m)
           seqs.write("%s" %(geneGFF))

        
