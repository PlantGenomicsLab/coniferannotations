import sys

bed= [line.rstrip('\n') for line in open(sys.argv[1])]
text = [line.rstrip('\n') for line in open(sys.argv[2])]

genes = []
exon_gene = []
exon_start = []
exon_end = []
exon_strand = []
exon_scaff = []

for t in text:
    c = t.split('\t')
    if ";" in c[3]:
        cols = c[3].split(".")
        genes.append(cols[0])
    else:
        genes.append(c[3])
    if ";" in c[10]:
        cols = c[10].split(".")
        genes.append(cols[0])
    else:
        genes.append(c[10])


for b in bed:
    c = b.split('\t')
    if "transcript_id" not in c[3]:
        for g in genes:
            if g in c[3] and "exon" in c[3]:
                print b
                break
    elif "transcript_id" in c[3]:
        cols = c[3].split("\"")
        for g in genes:
            if cols[1] in genes:
                print b
                break

