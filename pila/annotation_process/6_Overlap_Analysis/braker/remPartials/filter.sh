#!/bin/bash
module load bedtools
bedtools intersect -a ../braker_remInternal.bed -b ../../maker/gmap_maker_pita_remInternal.bed ../../pacbio_illumina/gmap_pacbio_ill_remInternal.bed -wa -wb > AnyOverlap.txt

bedtools intersect -f 1.0 -a ../braker_remInternal.bed -b ../../maker/gmap_maker_pita_remInternal.bed ../../pacbio_illumina/gmap_pacbio_ill_remInternal.bed -wa -wb > CompleteOverlap.txt

diff -U $(wc -l < AnyOverlap.txt) AnyOverlap.txt CompleteOverlap.txt | sed -n 's/^-//p' > PartialOverlap.txt 

##remove the first line in PartialOverlap.txt before proceeding
sed -i '1d' PartialOverlap.txt 
python ../../scripts/ReportOverlap.py PartialOverlap.txt PartialOverlap_ReportOverlap.txt

###resolve genes that have little coverage (less than 85%) and are on the same strand
awk -F '\t' '(NR>=1)&&(($6==$13)&&($16<.85))' PartialOverlap_ReportOverlap.txt > resolve.txt 
awk -F '\t' '(NR>=1)&&(($3-$2)<($10-$9))' CompleteOverlap.txt > LongerGeneModel.txt
awk -F '\t' '(NR>=1)&&(($3-$2)==($10-$9))' CompleteOverlap.txt > EqualGeneModel.txt 

###discard genes where coverage is less than 0.85 but on different strand, check if braker is the short gene model, if so append that gene model to discardBraker 
awk -F '\t' '(NR>=1)&&(($6!=$13)&&($16<.85))' PartialOverlap_ReportOverlap.txt | awk '(NR>=1)&&(($3-$2)<($10-$9))' - | cut -f4 - | sort - | uniq - > discardBraker
###discard genes where coverage is more than 0.85 and genes are on the same strand, check if braker is the short gene model, if so append that gene model to discardBraker   
awk -F '\t' '(NR>=1)&&(($6==$13)&&($16>.85))' PartialOverlap_ReportOverlap.txt | awk '(NR>=1)&&(($3-$2)<($10-$9))' - | cut -f4 - | sort - | uniq - >> discardBraker
###discard genes where coverage is more than 0.85 and gene are on different strnads, check if braker is the short gene model, if so append that gene model to discardBraker
awk -F '\t' '(NR>=1)&&(($6!=$13)&&($16>.85))' PartialOverlap_ReportOverlap.txt | awk '(NR>=1)&&(($3-$2)<($10-$9))' - | cut -f4 - | sort - | uniq - >> discardBraker
###use the discardBraker file to remove braker genes that are partially overlapping with other gene models and are shorter
python ../../scripts/filterBed.py discardBraker ../braker_remInternal.bed braker_remInternal_remPartial.bed
