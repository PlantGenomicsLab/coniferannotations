# Sugar pine --> $PWD=/labs/Wegrzyn/ConiferGenomes/Pila/
## Data Location:
### Genome(s): 
#### Original Genome:
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/Pila/genome/v1.5/pila.v1.5.provisional.fasta  
&nbsp;&nbsp;&nbsp;[TreeGenes FTP](https://treegenesdb.org/FTP/Genomes/Pila/genome/Pila.1_5.fa.gz)
#### Softmasked Genome:
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/Pila/genome/v1.5/Pila.v.1.5.masked.fasta 
&nbsp;&nbsp;&nbsp;[TreeGenes FTP](https://treegenesdb.org/FTP/Genomes/Pila/genome/Pila.1_5.softmasked.fa) 
### Raw RNA-seq Reads:
&nbsp;&nbsp;&nbsp;$PWD/raw_reads/ 
&nbsp;&nbsp;&nbsp;[NCBI Bioproject](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA174450)
&nbsp;&nbsp;&nbsp;[NCBI Download](https://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GEUZ01)
### Transcriptome: 
Combination of Illumina & Pacbio reads  
&nbsp;&nbsp;&nbsp;$PWD/Genes/pacbio_illumina/GEUZ01.1.fsa_nt 
&nbsp;&nbsp;&nbsp;[NCBI Download](https://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GEUZ01)
## Repeats: 
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/Pila/repeats/Pila_completelib.classified
&nbsp;&nbsp;&nbsp;[TreeGenes FTP](https://treegenesdb.org/FTP/Genomes/Pila/v1.0/repeats/Pila_completelib.classified)
### Quast:
&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/Pila/genome/v1.5/quast   
&nbsp;&nbsp;&nbsp;Note: The "broken" notation is default behavior of quast, meant for the summary of contigs (rather than the scaffolds).  

        Assembly                    Pila.v.1.5.masked   Pila.v.1.5.masked_broken
        contigs (>= 0 bp)               16468              -                       
        contigs (>= 1000 bp)            16468              3175448                 
        contigs (>= 5000 bp)            16468              1066189                 
        contigs (>= 10000 bp)           16468              515611                  
        contigs (>= 25000 bp)           15473              83203                   
        contigs (>= 50000 bp)           14311              5128                    
        Total length (>= 0 bp)          21122470373        -                       
        Total length (>= 1000 bp)       21122470373        18058912312             
        Total length (>= 5000 bp)       21122470373        13298799352             
        Total length (>= 10000 bp)      21122470373        9387573825              
        Total length (>= 25000 bp)      21105090171        2818727843              
        Total length (>= 50000 bp)      21063126346        304571885               
        contigs                         16468              4466016                 
        Largest contig                  23976851           122640                  
        Total length                    21122470373        18987373459             
        GC (%)                          37.07              37.07                   
        N50                             2916077            9844                    
        N75                             1453914            3999                    
        L50                             2015               526307                  
        L75                             4597               1276587                 
        N's per 100 kbp                  6944.60            21.50        
                   
### Genome Busco:

&nbsp;&nbsp;&nbsp;/isg/shared/databases/alignerIndex/plant/Pila/genome/v1.5/run_Busco_PILA_wholeGenome_5000bps  

        C:27.5%[S:24.2%,D:3.3%],F:11.9%,M:60.6%,n:1440
        396     Complete BUSCOs (C)
        349     Complete and single-copy BUSCOs (S)
        47      Complete and duplicated BUSCOs (D)
        172     Fragmented BUSCOs (F)
        872     Missing BUSCOs (M)
        1440    Total BUSCO groups searched


### Transcriptome Busco:

&nbsp;&nbsp;&nbsp; /labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/transcriptomes/sugarpine/run_Busco_pila_all_transcripts/  
&nbsp;&nbsp;&nbsp; Note: This transcriptome reflects the transcriptome published on NCBI and includes both pacbio and illumina transcripts.  
&nbsp;&nbsp;&nbsp; The transcriptome contained 33,112 sequences.  

        C:71.8%[S:59.3%,D:12.5%],F:3.7%,M:24.5%,n:1440

        1034    Complete BUSCOs (C)
        854     Complete and single-copy BUSCOs (S)
        180     Complete and duplicated BUSCOs (D)
        53      Fragmented BUSCOs (F)
        353     Missing BUSCOs (M)
        1440    Total BUSCO groups searched

### Genome Annotation Busco:

&nbsp;&nbsp;&nbsp; /labs/Wegrzyn/ConiferGenomes/Pita/paper/busco/genes/sugarpine/run_Busco_pila_annotation/
&nbsp;&nbsp;&nbsp; Note: The annotation contains 38,517 sequences. 

        C:69.8%[S:61.0%,D:8.8%],F:6.3%,M:23.9%,n:1440

        1005    Complete BUSCOs (C)
        878     Complete and single-copy BUSCOs (S)
        127     Complete and duplicated BUSCOs (D)
        91      Fragmented BUSCOs (F)
        344     Missing BUSCOs (M)
        1440    Total BUSCO groups searched

### Genome Annotation Structural

&nbsp;&nbsp;&nbsp; Detailed process is outlined in the annotationProcess.md  
&nbsp;&nbsp;&nbsp; [TreeGenes FTP Protein Sequence](https://treegenesdb.org/FTP/Genomes/Pila/v1.5/annotation/PILA.1_5.peptides.fa)  
&nbsp;&nbsp;&nbsp; [TreeGenes FTP Nucleotide Sequence](https://treegenesdb.org/FTP/Genomes/Pila/v1.5/annotation/PILA.1_5.cds.fa)  
&nbsp;&nbsp;&nbsp; [TreeGenes FTP GTF](https://treegenesdb.org/FTP/Genomes/Pila/v1.5/annotation/PILA.1_5.gtf)  

### Genome Annotation Functional

&nbsp;&nbsp;&nbsp; [TreeGenes FTP](https://treegenesdb.org/FTP/Genomes/Pila/v1.5/annotation/Pila.1_5.functionalAnnotation.tsv)

### Google SpreadSheet
&nbsp;&nbsp;&nbsp; [SpreadSheet for Sugar Pine](https://docs.google.com/spreadsheets/d/1xMcGvoVfS4Jk9NuRHpH-IFLc7--d4zBM-ld0LQT_0nw/edit#gid=0)
  






