# Location:
Output file is located here: /UCHC/LABS/Wegrzyn/ConiferGenomes/Pila/analysis/braker2.05/protein_alignments/all.protAligned.pilav1.05.gth.gff3  

# Stats: 
Number of genes:        3523  
Number of monoexonic genes:     79  
Number of multiexonic genes:    3444  

Number of positive strand genes:        1758  
Monoexonic:     43  
Multiexonic:    1715  

Number of negative strand genes:        1765  
Monoexonic:     36  
Multiexonic:    1729  

Average overall gene size:      72056.157  
Median overall gene size:       5221  
Average overall CDS size:       1209.138  
Median overall CDS size:        1083  
Average overall exon size:      230.858  
Median overall exon size:       149  

Average size of monoexonic genes:       6711.532  
Median size of monoexonic genes:        732  
Largest monoexonic gene:        376652  
Smallest monoexonic gene:       372  
  
Average size of multiexonic genes:      73555.061  
Median size of multiexonic genes:       5622  
Largest multiexonic gene:       1604060  
Smallest multiexonic gene:      532  

Average size of multiexonic CDS:        1217.440  
Median size of multiexonic CDS: 1086  
Largest multiexonic CDS:        6027  
Smallest multiexonic CDS:       446  

Average size of multiexonic exons:      228.208  
Median size of multiexonic exons:       148  
Average size of multiexonic introns:    16080.181  
Median size of multiexonic introns:     275  

Average number of exons per multiexonic gene:   5.335  
Median number of exons per multiexonic gene:    4  
Largest multiexonic exon:       4410  
Smallest multiexonic exon:      1  
Most exons in one gene: 25  

Average number of introns per multiexonic gene: 4.335  
Median number of introns per multiexonic gene:  3  
Largest intron: 1097574  
Smallest intron:        50  

# Command: 

```module load genomethreader/1.6.6
echo genome_scaffolds_blathits.fasta"$SLURM_ARRAY_TASK_ID".fa

gth -genomic /UCHC/LABS/Wegrzyn/ConiferGenomes/Pila/splitfasta/Pila.v.1.5.masked.fasta"$SLURM_ARRAY_TASK_ID".fa -protein /UCHC/LABS/Wegrzyn/ConiferGenomes/Pila/Genes/proteinSet/5prime_centroids_HQ_Maker_Pacbio_150aaproteins.fasta.faa150aaFixed.faa -gff3out -startcodon -gcmincoverage 80 -finalstopcodon -introncutout -dpminexonlen 20 -skipalignmentout -o comp_combined_protein_filtered.fasta150aa.faa"$SLURM_ARRAY_TASK_ID".gth.gff3 -force -gcmaxgapwidth 1000000```

