Library: SRR3689473  
```147701443 reads; of these:
  147701443 (100.00%) were paired; of these:
    50614639 (34.27%) aligned concordantly 0 times
    74249873 (50.27%) aligned concordantly exactly 1 time
    22836931 (15.46%) aligned concordantly >1 times
    ----
    50614639 pairs aligned concordantly 0 times; of these:
      5095804 (10.07%) aligned discordantly 1 time
    ----
    45518835 pairs aligned 0 times concordantly or discordantly; of these:
      91037670 mates make up the pairs; of these:
        56337462 (61.88%) aligned 0 times
        19368221 (21.27%) aligned exactly 1 time
        15331987 (16.84%) aligned >1 times
80.93% overall alignment rate 
```

Library: SRR3723920  
```118513879 reads; of these:
  118513879 (100.00%) were paired; of these:
    52427241 (44.24%) aligned concordantly 0 times
    51618190 (43.55%) aligned concordantly exactly 1 time
    14468448 (12.21%) aligned concordantly >1 times
    ----
    52427241 pairs aligned concordantly 0 times; of these:
      3544703 (6.76%) aligned discordantly 1 time
    ----
    48882538 pairs aligned 0 times concordantly or discordantly; of these:
      97765076 mates make up the pairs; of these:
        75313312 (77.03%) aligned 0 times
        12706849 (13.00%) aligned exactly 1 time
        9744915 (9.97%) aligned >1 times
68.23% overall alignment rate  
```
Library: SRR3723921  
```155232805 reads; of these:
  155232805 (100.00%) were paired; of these:
    56003149 (36.08%) aligned concordantly 0 times
    77717986 (50.07%) aligned concordantly exactly 1 time
    21511670 (13.86%) aligned concordantly >1 times
    ----
    56003149 pairs aligned concordantly 0 times; of these:
      5043850 (9.01%) aligned discordantly 1 time
    ----
    50959299 pairs aligned 0 times concordantly or discordantly; of these:
      101918598 mates make up the pairs; of these:
        66599381 (65.35%) aligned 0 times
        20722702 (20.33%) aligned exactly 1 time
        14596515 (14.32%) aligned >1 times
78.55% overall alignment rate  
```
Library: SRR3723922  
```117592023 reads; of these:
  117592023 (100.00%) were paired; of these:
    41131179 (34.98%) aligned concordantly 0 times
    60705057 (51.62%) aligned concordantly exactly 1 time
    15755787 (13.40%) aligned concordantly >1 times
    ----
    41131179 pairs aligned concordantly 0 times; of these:
      4431525 (10.77%) aligned discordantly 1 time
    ----
    36699654 pairs aligned 0 times concordantly or discordantly; of these:
      73399308 mates make up the pairs; of these:
        46454562 (63.29%) aligned 0 times
        15963006 (21.75%) aligned exactly 1 time
        10981740 (14.96%) aligned >1 times
80.25% overall alignment rate  
```
Library: SRR3723923  
```141718961 reads; of these:  
  141718961 (100.00%) were paired; of these:  
    53921072 (38.05%) aligned concordantly 0 times  
    66941890 (47.24%) aligned concordantly exactly 1 time  
    20855999 (14.72%) aligned concordantly >1 times  
    ----  
    53921072 pairs aligned concordantly 0 times; of these:  
      5272126 (9.78%) aligned discordantly 1 time  
    ----  
    48648946 pairs aligned 0 times concordantly or discordantly; of these:  
      97297892 mates make up the pairs; of these:  
        62988555 (64.74%) aligned 0 times  
        19941971 (20.50%) aligned exactly 1 time  
        14367366 (14.77%) aligned >1 times  
77.78% overall alignment rate 
``` 
Library: SRR3723924  
```139601430 reads; of these:
  139601430 (100.00%) were paired; of these:
    55555257 (39.80%) aligned concordantly 0 times
    64139893 (45.95%) aligned concordantly exactly 1 time
    19906280 (14.26%) aligned concordantly >1 times
    ----
    55555257 pairs aligned concordantly 0 times; of these:
      4492497 (8.09%) aligned discordantly 1 time
    ----
    51062760 pairs aligned 0 times concordantly or discordantly; of these:
      102125520 mates make up the pairs; of these:
        69736896 (68.29%) aligned 0 times
        18366623 (17.98%) aligned exactly 1 time
        14022001 (13.73%) aligned >1 times
75.02% overall alignment rate
```

Library: SRR3723925     
```140517031 reads; of these:
  140517031 (100.00%) were paired; of these:
    45765555 (32.57%) aligned concordantly 0 times
    67400405 (47.97%) aligned concordantly exactly 1 time
    27351071 (19.46%) aligned concordantly >1 times
    ----
    45765555 pairs aligned concordantly 0 times; of these:
      4079647 (8.91%) aligned discordantly 1 time
    ----
    41685908 pairs aligned 0 times concordantly or discordantly; of these:
      83371816 mates make up the pairs; of these:
        53166378 (63.77%) aligned 0 times
        16950461 (20.33%) aligned exactly 1 time
        13254977 (15.90%) aligned >1 times
81.08% overall alignment rate
```

Library: SRR3723926  
```155613699 reads; of these:
  155613699 (100.00%) were paired; of these:
    57973165 (37.25%) aligned concordantly 0 times
    78108954 (50.19%) aligned concordantly exactly 1 time
    19531580 (12.55%) aligned concordantly >1 times
    ----
    57973165 pairs aligned concordantly 0 times; of these:
      4371131 (7.54%) aligned discordantly 1 time
    ----
    53602034 pairs aligned 0 times concordantly or discordantly; of these:
      107204068 mates make up the pairs; of these:
        78955444 (73.65%) aligned 0 times
        16161448 (15.08%) aligned exactly 1 time
        12087176 (11.27%) aligned >1 times
74.63% overall alignment rate
```

Library: SRR3723927  
```152584689 (100.00%) were paired; of these:
    59616342 (39.07%) aligned concordantly 0 times
    71084103 (46.59%) aligned concordantly exactly 1 time
    21884244 (14.34%) aligned concordantly >1 times
    ----
    59616342 pairs aligned concordantly 0 times; of these:
      5849583 (9.81%) aligned discordantly 1 time
    ----
    53766759 pairs aligned 0 times concordantly or discordantly; of these:
      107533518 mates make up the pairs; of these:
        69955533 (65.05%) aligned 0 times
        21742912 (20.22%) aligned exactly 1 time
        15835073 (14.73%) aligned >1 times
77.08% overall alignment rate
```   